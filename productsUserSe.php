<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017")){
	if ($user->can(IS_SECURITY,$_SESSION['sysUser'])) {
		
		$tpl->load_file("pg/security/products.html", "bodyContent");
		$tpl->load_file("pg/security/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);				
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		
		
		$sAction 			= $_POST['sAction'];
		
		$permitidos = array("image/jpg", "image/jpeg", "image/gif", "image/png");
		$limite_kb = 100000000;
	
		
		switch ($sAction){
			case "editInformacion":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['editName'];
					$oData->id	 		= (int)($_POST['iEditID']);
					$oData->brand	 		= (int)($_POST['editMarca']);
					$oData->description = htmlentities($_POST['editDescription']);
					
					
					
					if ($_FILES["fileUploadEdit"]["tmp_name"]<>""){
						if(in_array($_FILES['fileUploadEdit']['type'], $permitidos) && $_FILES['fileUploadEdit']['size'] <= $limite_kb * 1024){
							$oName=$oArchivo->savePro($_FILES["fileUploadEdit"]["tmp_name"],'img_prod/'.$_FILES["fileUploadEdit"]["name"],1);
							$oData->file = $oName;
							if ($_POST['editFile']<>'sinImagen.jpg'){
								//unlink("img_prod/".$_POST['editFile']);
							}
							
						}else{
							$oData->file = 'sinImagen.jpg';
						}
					} else {
						$oData->file = htmlentities($_POST['editFile']);
					}
					//uno
					if ($_FILES['fileUploadEdit1']["tmp_name"]<>""){
						if(in_array($_FILES['fileUploadEdit1']['type'], $permitidos) && $_FILES['fileUploadEdit1']['size'] <= $limite_kb * 1024){
							$oName1=$oArchivo->savePro($_FILES["fileUploadEdit1"]["tmp_name"],'img_prod/'.$_FILES["fileUploadEdit1"]["name"],2);
							$oData->file1 = $oName1;
							if ($_POST['editFile1']<>'sinImagen.jpg'){
								//unlink("img_prod/".$_POST['editFile1']);
							}
							
						}else{
							$oData->file1 = 'sinImagen.jpg';
						}
					} else {
						$oData->file1 = htmlentities($_POST['editFile1']);
					}
					
					if ($_FILES['fileUploadEdit2']["tmp_name"]<>""){
						if(in_array($_FILES['fileUploadEdit2']['type'], $permitidos) && $_FILES['fileUploadEdit2']['size'] <= $limite_kb * 1024){
							$oName2=$oArchivo->savePro($_FILES["fileUploadEdit2"]["tmp_name"],'img_prod/'.$_FILES["fileUploadEdit2"]["name"],3);
							$oData->file2 = $oName2;
							if ($_POST['editFile2']<>'sinImagen.jpg'){
								//unlink("img_prod/".$_POST['editFile2']);
							}
							
						}else{
							$oData->file2= 'sinImagen.jpg';
						}
					} else {
						$oData->file2 = htmlentities($_POST['editFile2']);
					}
					
					//tres
					if ($_FILES['fileUploadEdit3']["tmp_name"]<>""){
						if(in_array($_FILES['fileUploadEdit3']['type'], $permitidos) && $_FILES['fileUploadEdit3']['size'] <= $limite_kb * 1024){
							$oName3=$oArchivo->savePro($_FILES["fileUploadEdit3"]["tmp_name"],'img_prod/'.$_FILES["fileUploadEdit3"]["name"],4);
							$oData->file3 = $oName3;
							if ($_POST['editFile3']<>'sinImagen.jpg'){
								//unlink("img_prod/".$_POST['editFile3']);
							}
							
						}else{
							$oData->file3 = 'sinImagen.jpg';
						}
					} else {
						$oData->file3 = htmlentities($_POST['editFile3']);
					}
					
					
					
					
					
					
					
					
					
					
					$oData->value = htmlentities($_POST['editValue']);
					$oData->cuotas = htmlentities($_POST['editCuotas']);
					$oData->category = (int)($_POST['editCategory']);
					$oData->subcategory = (int)($_POST['editSubCategory']);
					$oData->cuotas = htmlentities($_POST['editCuotas']);
					$oData->status = (int)($_POST['editStatus']);
					$oData->offer = (int)($_POST['editOferta']);
					
					$oData->valorFinanciado = htmlentities($_POST['editPtf']);
					$oData->valorCuota = htmlentities($_POST['editValorCuota']);
					$oData->cantidadCuotas = htmlentities($_POST['editCantidadCuotas']);

					
					
					
					$resultEdit = $oAdv->editProducts($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado la Información.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "newInformacionUno":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['newName'];
					$oData->description = htmlentities($_POST['newDescription']);
					
					$oData->brand	= (int)$_POST['newMarca'];
					$oData->valorFinanciado = htmlentities($_POST['newPtf']);
					$oData->valorCuota = htmlentities($_POST['newValorCuota']);
					$oData->cantidadCuotas = htmlentities($_POST['newCantidadCuotas']);
					
					$oData->value = htmlentities($_POST['newValue']);
					$oData->cuotas = htmlentities($_POST['newCuotas']);
					$oData->category = (int)($_POST['newCategory']);
					$oData->subcategory = (int)($_POST['newSubCategory']);
					$oData->cuotas = htmlentities($_POST['newCuotas']);
					$oData->offer = (int)($_POST['newOferta']);
					$oData->status = 1;
					
	
				if(in_array($_FILES['fileUpload']['type'], $permitidos) && $_FILES['fileUpload']['size'] <= $limite_kb * 1024){
					$oName=$oArchivo->savePro($_FILES["fileUpload"]["tmp_name"],'img_prod/'.$_FILES["fileUpload"]["name"],1);					
					
					if($oName){
						$oData->file = $oName;
						
						
						
						if(in_array($_FILES['fileUpload1']['type'], $permitidos) && $_FILES['fileUpload1']['size'] <= $limite_kb * 1024){
							$oName1=$oArchivo->savePro($_FILES["fileUpload1"]["tmp_name"],'img_prod/'.$_FILES["fileUpload1"]["name"],2);
							$oData->file1 = $oName1;
							
						}else{
							$oData->file1 = 'sinImagen.jpg';
						}
						
						
						if(in_array($_FILES['fileUpload2']['type'], $permitidos) && $_FILES['fileUpload2']['size'] <= $limite_kb * 1024){
							$oName2=$oArchivo->savePro($_FILES["fileUpload2"]["tmp_name"],'img_prod/'.$_FILES["fileUpload2"]["name"],3);
							$oData->file2 = $oName2;
							
						}else{
							$oData->file2 = 'sinImagen.jpg';
						}
						
						
						if(in_array($_FILES['fileUpload3']['type'], $permitidos) && $_FILES['fileUpload3']['size'] <= $limite_kb * 1024){
							$oName3=$oArchivo->savePro($_FILES["fileUpload3"]["tmp_name"],'img_prod/'.$_FILES["fileUpload3"]["name"],4);
							$oData->file3 = $oName3;
							
						}else{
							$oData->file3 = 'sinImagen.jpg';
						}						
									
//var_dump($oData);exit;

					$resultEdit = $oAdv->register($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado la Información.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
				} else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "searchInformacionById":
	
		$oData = $oAdv->getProductsById($_POST['iID']);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	case "newInformacion":

					$oName=$oArchivo->saveA($_FILES["fileUpload"]["tmp_name"],'temp/'.$_FILES["fileUpload"]["name"]);
					$result = $oAdv->newAdvUp($oName);
					
					if ($result->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado Nuevos Productos.");
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}
					 
					
	break;
	case "searchByCategorias":
	
		$oData = $oAdv->getSubCategorias((int)$newCate);
		echo json_encode($oData);
		exit;
	break;
	case "deleteInformacion":
		
			$oData = $oAdv->deleteProductById($_POST['iID']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	default:
	break;
}
		
if(count($oAdv->getProducts())>0){
		foreach ($oAdv->getProducts() as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sName",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sDescription",mb_convert_encoding($Item['description'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("iValue",mb_convert_encoding($Item['value'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sCategory",mb_convert_encoding($Item['category'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sSubCategory",mb_convert_encoding($Item['subcategory'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sMarca",mb_convert_encoding($Item['brand'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sPlan",mb_convert_encoding($Item['cuotas'], 'UTF-8', 'ISO-8859-1'));
				
				$tpl->parse("ResultsBlock",true);
			}
	} 
	

if(count($oAdv->getMarcas())>0){
		foreach ($oAdv->getMarcas() as $Item){
				$tpl->set_var("iMarca",$Item['id']);
				$tpl->set_var("sMarca",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("MarcasBlock",true);
			}
	} 	
	
	if(count($oAdv->getCategorias())>0){
		foreach ($oAdv->getCategorias() as $Item){
				$tpl->set_var("iCategory",$Item['id']);
				$tpl->set_var("sCategory",mb_convert_encoding($Item['description'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("CategoriasBlock",true);
			}
	} 	
	
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>