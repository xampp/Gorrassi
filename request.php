<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser'])) ||  ($user->can(IS_USER,$_SESSION['sysUser']))) {
		
		$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);	
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		
		
		if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
			
			$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
		
	} 
	
		if($user->can(IS_USER,$_SESSION['sysUser'])){
			
			$tpl->load_file("pg/user/menu.html","menu");
		
	} else {
		$tpl->load_file("pg/admin/menu.html","menu");
	}
		
		$tpl->load_file("pg/admin/request.html", "bodyContent");
		
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		//$tpl->set_var("sSysUserName","Administrador");
		$sAction 	= isset($_POST['sAction'])?$_POST['sAction']:null;
		$bId 	= isset($_GET['iDCustomer'])?true: false;
		
		if($bId){
			$oData = $custo->searchCustomerById($_GET['iDCustomer']);
			//var_dump($oData);
			$tpl->set_var("first",$oData["first"]);
			$tpl->set_var("last",$oData["last"]);
			$tpl->set_var("dni",$oData["dni"]);
			$tpl->set_var("birthdate",$oData["birthdate"]);
			$tpl->set_var("local",$oData["local"]);
			$tpl->set_var("email",$oData["email"]);
			$tpl->set_var("estado",$oData["estado"]);
			$tpl->set_var("domicilio",$oData["domicilio"]);
			$tpl->set_var("cpa",$oData["cpa"]);
			$tpl->set_var("tel",$oData["tel"]);
			$tpl->set_var("cel",$oData["cel"]);
			//var_dump($oData["busi"]);
			$tpl->set_var("empresa",$oData["busi"]["0"]["empresa"]);
			$tpl->set_var("dom",$oData["busi"]["0"]["empresa"]);
			$tpl->set_var("tel_empresa",$oData["busi"]["0"]["tel_empresa"]);
			$tpl->set_var("rubro",$oData["busi"]["0"]["rubro"]);
			$tpl->set_var("cargo",$oData["busi"]["0"]["cargo"]);
			$tpl->set_var("sueldo",$oData["busi"]["0"]["sueldo"]);
			$tpl->set_var("antiguedad",$oData["busi"]["0"]["antiguedad"]);
			$tpl->set_var("first_cont",$oData["busi"]["0"]["first_cont"]);
			$tpl->set_var("last_cont",$oData["busi"]["0"]["last_cont"]);
			$tpl->set_var("domicilio_cont",$oData["busi"]["0"]["domicilio_cont"]);
			$tpl->set_var("tel_cont",$oData["busi"]["0"]["tel_cont"]);
			$tpl->set_var("rel_cont",$oData["busi"]["0"]["rel_cont"]);
			$tpl->set_var("sActionValue","editCustomer");
			//datos garante
			$tpl->set_var("first_g",$oData["charges"]["0"]["first"]);
			$tpl->set_var("last_g",$oData["charges"]["0"]["last"]);
			$tpl->set_var("dni_g",$oData["charges"]["0"]["dni"]);
			$tpl->set_var("birthdate_g",$oData["charges"]["0"]["birthdate"]);
			$tpl->set_var("email_g",$oData["charges"]["0"]["email"]);
			$tpl->set_var("estado_g",$oData["charges"]["0"]["estado"]);
			$tpl->set_var("domicilio_g",$oData["charges"]["0"]["domicilio"]);
			$tpl->set_var("local_g",$oData["local"]);
			$tpl->set_var("cpa_g",$oData["charges"]["0"]["cpa"]);
			$tpl->set_var("tel_g",$oData["charges"]["0"]["tel"]);
			$tpl->set_var("cel_g",$oData["charges"]["0"]["cel"]);
			$tpl->set_var("garante",$oData["charges"]["0"]["garante"]);
			$tpl->set_var("cliente",$oData["charges"]["0"]["cliente"]);
			//trabajo garante
			$tpl->set_var("empresa_g",$oData["busiGarante"]["0"]["empresa"]);
			$tpl->set_var("dom_g",$oData["busiGarante"]["0"]["empresa"]);
			$tpl->set_var("tel_empresa_g",$oData["busiGarante"]["0"]["tel_empresa"]);
			$tpl->set_var("rubro_g",$oData["busiGarante"]["0"]["rubro"]);
			$tpl->set_var("cargo_g",$oData["busiGarante"]["0"]["cargo"]);
			$tpl->set_var("sueldo_g",$oData["busiGarante"]["0"]["sueldo"]);
			$tpl->set_var("antiguedad_g",$oData["busiGarante"]["0"]["antiguedad"]);
			$tpl->set_var("first_cont_g",$oData["busiGarante"]["0"]["first_cont"]);
			$tpl->set_var("last_cont_g",$oData["busiGarante"]["0"]["last_cont"]);
			$tpl->set_var("domicilio_cont_g",$oData["busiGarante"]["0"]["domicilio_cont"]);
			$tpl->set_var("tel_cont_g",$oData["busiGarante"]["0"]["tel_cont"]);
			$tpl->set_var("rel_cont_g",$oData["busiGarante"]["0"]["rel_cont"]);
			//garantia propiedad
			$tpl->set_var("propietario",$oData["pro"]["0"]["propietario"]);
			$tpl->set_var("ubica",$oData["pro"]["0"]["ubica"]);
			$tpl->set_var("matricula",$oData["pro"]["0"]["matricula"]);
			$tpl->set_var("partida",$oData["pro"]["0"]["partida"]);
			$tpl->set_var("circun",$oData["pro"]["0"]["circun"]);
			$tpl->set_var("seccion",$oData["pro"]["0"]["seccion"]);
			$tpl->set_var("manzana",$oData["pro"]["0"]["manzana"]);
			$tpl->set_var("parcela",$oData["pro"]["0"]["parcela"]);
			$tpl->set_var("subparcela",$oData["pro"]["0"]["subparcela"]);
			
			
			
		} else {
			$tpl->set_var("first",'');
			$tpl->set_var("last",'');
			$tpl->set_var("dni",'');
			$tpl->set_var("birthdate",isset($oData["birthdate"])?$oData["birthdate"]:'');
			$tpl->set_var("email",isset($oData["email"])?$oData["email"]:'');
			$tpl->set_var("estado",isset($oData["estado"])?$oData["estado"]:'');
			$tpl->set_var("domicilio",isset($oData["domicilio"])?$oData["domicilio"]:'');
			$tpl->set_var("cpa",isset($oData["cpa"])?$oData["cpa"]:'');
			$tpl->set_var("tel",isset($oData["tel"])?$oData["tel"]:'');
			$tpl->set_var("cel",isset($oData["cel"])?$oData["cel"]:'');
			$tpl->set_var("garante",isset($oData["garante"])?$oData["garante"]:'');
			$tpl->set_var("cliente",isset($oData["cliente"])?$oData["cliente"]:'');
			$tpl->set_var("sActionValue","newCustomer");
		}
	
		
		switch ($sAction){
			case "editServices":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['editNombre'];
					$oData->phone = ($_POST['editTelefono']);
					$oData->cellPhone = ($_POST['editCelular']);
					$oData->email = ($_POST['editEmail']);
					$oData->obser = ($_POST['editObs']);
					$oData->brand = (int)($_POST['editMarca']);
					$oData->type = (int)($_POST['editTipo']);
					$oData->address = ($_POST['editDomicilio']);
					$oData->id	 		= (int)($_POST['iEditID']);
					
					$resultEdit = $oAdv->editServices($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado el service.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "searchCustomerById":
		$oData = $custo->searchCustomerById($_POST['iID']);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	case "searchPersonalByName":
	//var_dump("hola");exit;
		$vData = $custo->searchPersonalByName($_POST['userName']);
		if($vData && @substr($vData,0,3) == "ERR"){
			$vData['status'] = "ERROR";
			$vData['message'] = $db->getLabel("lbl_".$vData,"SPA");
		}
		echo json_encode($vData);
		exit;
	break;
	case "newCustomer":

					$oDataC = new stdClass();
					$oDataE = new stdClass();
					$oDataG = new stdClass();
					$oDataGE = new stdClass();
					$oDataGP = new stdClass();
					//cliente solicitante
					$oDataC->first	= $_POST['first'];
					$oDataC->last =$_POST['last'];
					$oDataC->dni = $_POST['dni'];
					$oDataC->birthdate = $_POST['birthdate'];
					$oDataC->email = $_POST['email'];
					$oDataC->estado = $_POST['estado'];
					$oDataC->domicilio = $_POST['domicilio'];
					$oDataC->cpa = $_POST['cpa'];
					$oDataC->tel = $_POST['tel'];
					$oDataC->cel = $_POST['cel'];
					$oDataC->garante = 0;
					$oDataC->cliente = 1;
					//datos empresa
					$oDataE->empresa	= $_POST['empresa'];
					$oDataE->dom =$_POST['dom'];
					$oDataE->tel_empresa = $_POST['tel_empresa'];
					$oDataE->rubro = $_POST['rubro'];
					$oDataE->cargo = $_POST['cargo'];
					$oDataE->sueldo = $_POST['sueldo'];
					$oDataE->antiguedad = $_POST['antiguedad'];
					//datos relacion empresa
					$oDataE->first_cont	= $_POST['first_cont'];
					$oDataE->last_cont	= $_POST['last_cont'];
					$oDataE->domicilio_cont =$_POST['domicilio_cont'];
					$oDataE->tel_cont = $_POST['tel_cont'];
					$oDataE->rel_cont = $_POST['rel_cont'];
					//garante group1 si es conyugue o lo otro
					$oDataG->first	= $_POST['first_g'];
					$oDataG->last =$_POST['last_g'];
					$oDataG->dni = $_POST['dni_g'];
					$oDataG->birthdate = $_POST['birthdate_g'];
					$oDataG->email = $_POST['email_g'];
					$oDataG->estado = $_POST['estado_g'];
					$oDataG->domicilio = $_POST['domicilio_g'];
					$oDataG->cpa = $_POST['cpa_g'];
					$oDataG->tel = $_POST['tel_g'];
					$oDataG->cel = $_POST['cel_g'];
					$oDataG->garante = 0;
					$oDataG->cliente = 0;
					//datos empresa
					$oDataGE->empresa	= $_POST['empresa_g'];
					$oDataGE->dom =$_POST['dom_g'];
					$oDataGE->tel_empresa = $_POST['tel_empresa_g'];
					$oDataGE->rubro = $_POST['rubro_g'];
					$oDataGE->cargo = $_POST['cargo_g'];
					$oDataGE->sueldo = $_POST['sueldo_g'];
					$oDataGE->antiguedad = $_POST['antiguedad_g'];
					//datos relacion empresa
					$oDataGE->first_cont	= $_POST['first_cont_g'];
					$oDataGE->last_cont	= $_POST['last_cont_g'];
					$oDataGE->domicilio_cont =$_POST['domicilio_cont_g'];
					$oDataGE->tel_cont = $_POST['tel_cont_g'];
					$oDataGE->rel_cont = $_POST['rel_cont_g'];
					
					//hasta aca lo del garante
					//garantía propietario
					$oDataGP->propietario	= $_POST['propietario'];
					$oDataGP->ubica	= $_POST['ubica'];
					$oDataGP->matricula	= $_POST['matricula'];
					$oDataGP->partida	= $_POST['partida'];
					$oDataGP->circun	= $_POST['circun'];
					$oDataGP->seccion	= $_POST['seccion'];
					$oDataGP->manzana	= $_POST['manzana'];
					$oDataGP->parcela	= $_POST['parcela'];
					$oDataGP->subparcela	= $_POST['subparcela'];
					//finde garantia propietario
					//monto solicitud
					/*monto
					operador
					autoriza*/
					
					$resultC = $custo->newCustomer($oDataC);

					if ($resultC->status=="OK"){
						
						$oDataE->idCus = $resultC->lastId;
						$resultE = $custo->newBusiness($oDataE);
						$oDataGP->idCus = $resultC->lastId;
						$resultGP = $custo->newProperty($oDataGP);
						$resultG = $custo->newCustomer($oDataG);
						$oDataGE->idCus = $resultG->lastId;
						$resultGE = $custo->newBusiness($oDataGE);
						$Update = $custo->updateGarante($resultC->lastId,$resultG->lastId);

						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado una Nueva Solicitud de Crédito.");
						$tpl->set_var("sResultId",$resultC->lastId);
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}
					 
					
	break;
	case "deleteServices":
		
			$oData = $oAdv->deleteServicesById($_POST['iID']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	default:
	break;
}
		
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>