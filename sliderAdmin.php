<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM == "PASCUAL2017")) {
  if (($user->can(IS_ADMIN, $_SESSION['sysUser'])) || ($user->can(IS_EDIT, $_SESSION['sysUser']))) {
    
    $oData = $user->getName();
    $tpl->set_var("sSysUserName", $oData['name'] . '  ' . $oData['lastName']);
    $tpl->set_var("sSysUserLetter", $oData['name'][0]);
    $tpl->set_var("sSysUserEmail", $oData['userName']);
    $tpl->set_var("sSysUserNameRol", $oData['rol']);
    
    
    if ($user->can(IS_ADMIN, $_SESSION['sysUser'])) {
      
      $tpl->set_var("sUsuarioAdmin", '<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
      
    }
    $tpl->load_file("pg/admin/slider.html", "bodyContent");
    $tpl->load_file("pg/admin/menu.html", "menu");
    $tpl->set_var("sDisplayError", "display:none;");
    $tpl->set_var("sDisplayOK", "display:none;");
    $tpl->set_var("sResult", "");
    //$tpl->set_var("sSysUserName","Administrador");
    $sAction = isset($_POST['sAction']) ? $_POST['sAction'] : null;
    
    switch ($sAction) {
      case "editSlider":
        
        $oData = new stdClass();
        $oData->name = $_POST['editName'];
        $oData->id = (int)($_POST['iEditID']);
        
        //$oData->description = htmlentities($_POST['editDescription']);
        $oData->description = $_POST['editName'];
        
        $oData->img = htmlentities($_POST['editImg']);
        
        
        $resultEdit = $oAdv->editSlider($oData);
        
        if ($resultEdit->status == "OK") {
          $tpl->set_var("sDisplayError", "display:none;");
          $tpl->set_var("sDisplayOK", "");
          $tpl->set_var("sResult", "A modificado el slider.");
          
        } else {
          $tpl->set_var("sDisplayError", "");
          $tpl->set_var("sDisplayOK", "display:none;");
          $tpl->set_var("sResult", "");
          
        }
        
        break;
      case "searchSliderById":
        
        $oData = $oAdv->getSliderById($_POST['iID']);
        //var_dump($oData);exit;
        if ($oData['queryStatus'] != "OK") {
          $oData['queryStatus'] = $db->getLabel("lbl_" . $oData['queryStatus'], "SPA");
        }
        echo json_encode($oData);
        exit;
        break;
      case "newSlider":
        
        $oData = new stdClass();
        $oData->name = $_POST['newName'];
        //$oData->description = htmlentities($_POST['newDescription']);
        $oData->description = $_POST['newName'];
        
        
        $permitidos = array("image/jpg", "image/jpeg", "image/gif", "image/png");
        $limite_kb = 100000000;
        
        if (in_array($_FILES['fileUpload']['type'], $permitidos) && $_FILES['fileUpload']['size'] <= $limite_kb * 1024) {
          $oName = $oArchivo->save($_FILES["fileUpload"]["tmp_name"], 'img_prod/slider/' . $_FILES["fileUpload"]["name"]);
          if ($oName) {
            $oData->img = $oName;
            
            $result = $oAdv->newSlider($oData);
            
            if ($result->status == "OK") {
              $tpl->set_var("sDisplayError", "display:none;");
              $tpl->set_var("sDisplayOK", "");
              $tpl->set_var("sResult", "A agregado un nuevo slider.");
            } else {
              $tpl->set_var("sDisplayError", "");
              $tpl->set_var("sDisplayOK", "display:none;");
              $tpl->set_var("sResult", "error");
            }
          } else {
            $tpl->set_var("sDisplayError", "");
            $tpl->set_var("sDisplayOK", "display:none;");
            $tpl->set_var("sResult", "error");
          }
        } else {
          $tpl->set_var("sDisplayError", "");
          $tpl->set_var("sDisplayOK", "display:none;");
          $tpl->set_var("sResult", "error");
        }
        
        break;
      case "deleteSlider":
        
        $oData = $oAdv->deleteSliderById($_POST['iID']);
        if ($oData->status != "OK") {
          $oData->status = $db->getLabel("lbl_" . $oData->status, "SPA");
        }
        
        echo json_encode($oData);
        exit;
        break;
      default:
        break;
    }
    
    if (count($oAdv->getSliders()) > 0) {
      foreach ($oAdv->getSliders() as $Item) {
        $tpl->set_var("iId", $Item['id']);
        $tpl->set_var("sName", mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
        $tpl->set_var("sDescription", mb_convert_encoding($Item['description'], 'UTF-8', 'ISO-8859-1'));
        $tpl->set_var("sImg", mb_convert_encoding($Item['img'], 'UTF-8', 'ISO-8859-1'));
        $tpl->parse("ResultsBlock", true);
      }
    }
    
  } else {
    header('location: login.php');
  }
} else {
  header('location: login.php');
}
$tpl->pparse("main");
?>