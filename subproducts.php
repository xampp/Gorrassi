<?php
include_once 'config.php';
$tpl->load_file("/subproducts/sub.html", "bodyContent");
$sAction 			= $_GET['art'];

if ($sAction){

	$name = (int)$utils->decrypt($sAction,"sapopep16+3/");
	
	if(count($oAdv->getSubCategorys($name))>0){
	
		foreach ($oAdv->getSubCategorys($name) as $ItemSub){
			
			$psub= "<li><a href='javascript:void(0);' class='filter' data-filter='.category-".($ItemSub['id'])."' data-sort='myorder:asc'>".($ItemSub['description'])."</a></li>";
			$tpl->set_var("sSubM",$psub);
			$tpl->parse("Sub",true);				
		}
	}		
	//var_dump($name);	
	$parametros=$para->getParametros();
	
	if(count($oAdv->getProductsByArtSub($name))>0){
		
		$names=$oAdv->getNameProductsByArt($name);
		$tpl->set_var("sCate",($names[0]["categoria"]));
		$tpl->set_var("sSubC","");

		foreach ($oAdv->getProductsByArtSub($name) as $Item){		
			
			$tpl->set_var("iId",$utils->encrypt($Item['id'],"sapopep16+3/"));
			$tpl->set_var("sBrand",($Item['brand']));
			$tpl->set_var("sName",ucwords(mb_strtolower(($Item['name']))));
			$tpl->set_var("sDescription",($Item['description']));
			$tpl->set_var("sDescription1",($oAdv->wordlimit($Item['model'],4)));
			
			if ($parametros['precios']=='S') {
			$tpl->set_var("iValue",(round($Item['value'])));
			}
			$tpl->set_var("sFile",($Item['file']));
			$tpl->set_var("sFile1",($Item['file1']));
			$tpl->set_var("sFile2",($Item['file2']));
			$tpl->set_var("sFile3",($Item['file3']));
			
			$tpl->set_var("sPlan",($Item['cuotas']));
			
			if ($Item['cantidadCuotas']>0){
					
				$tpl->set_var("sCartel",'Con Crédito Personal');
				$tpl->set_var("sCuotasNew",$Item['cantidadCuotas'].' cuotas de $ '.$Item['valorCuota']);
				$tpl->set_var("cft",'CFT: '.$parametros['cft'].'% ');
				$tpl->set_var("tea",'TEA: '.$parametros['tea'].'%');
				$tpl->set_var("iFinanciado",($Item['valorFinanciado']));
			} else {
				$tpl->set_var("sCuotasNew",'');
				$tpl->set_var("cft",'');
				$tpl->set_var("tea",'');
				$tpl->set_var("iFinanciado",($Item['value']));
			}
			
			$tpl->set_var("sCategory",($Item['category']));
			$tpl->parse("ProductosXArt",true);
		}
	} 
	else {
		
		$tpl->set_var("sDisplayResul","display:none!important;");
		$tpl->set_var("sResultado","<h2>Página vacía.</h2><br><p>No se han encontrado resultados...</p><br><img src='img/logoL.png'>");
		}			
}	else {
		$tpl->set_var("sDisplayResul","display:none!important;");
		$tpl->set_var("sResultado","<h2>Página vacía.</h2><br><p>No se han encontrado resultados...</p><br><img src='img/logoL.png'>");
}	
$tpl->pparse("main");
?> 