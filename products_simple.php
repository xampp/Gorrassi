<?php
include_once 'config.php';
$tpl->load_file("/products_single_page/prod_single_page.html", "bodyContent");
$sAction 	= isset($_POST['sAction'])?$_POST['sAction']:null;
$parametros=$para->getParametros();
$bArt      = isset($_GET['art'])?true:false;
if($bArt){

	$name = $utils->decrypt($_GET['art'],"sapopep16+3/");
	//$name = $_GET['art'];//$utils->decrypt($_GET['art'],"sapopep16+3/");  
 // var_dump(($oAdv->searchAdvById($name)));
	//if(is_countable($oAdv->searchAdvById($name))){
	if($oAdv->searchAdvById($name)['queryStatus'] == "OK"){

		$Item =	$oAdv->getProductsSearchId($name);
		if($Item['queryStatus'] == "OK"){
			if ($parametros['precios']=='S'){
				$tpl->set_var("iValue",(round($Item['value'])));
				$tpl->set_var("sCondicion",mb_convert_encoding($Item['condicion'], 'UTF-8'));
				$i=0;
				$oCond = $oAdv->getCondiciones(1);
				if(count($oCond)>0){
					foreach ($oCond as $ItemDos){

						if ($i==1){
							//CHART
							$tpl->set_var("sNameMain",mb_convert_encoding($ItemDos['name'], 'UTF-8'));
							$tpl->set_var("sCuotaMain",($ItemDos['cuota']));							
							$tpl->set_var("sValorMain", round(( round($Item['value']) + (round($Item['value']) * ($ItemDos['rec']/100)))/($ItemDos['cuota']=='0'?1:$ItemDos['cuota'])));
							$tpl->set_var("sValorSinFinMain", round(( round($Item['value']) + (round($Item['value']) * ($ItemDos['rec']/100)))) );
						}
						$i++;
						//MODAL
						$tpl->set_var("sNameCon",mb_convert_encoding($ItemDos['name'], 'UTF-8'));
						$tpl->set_var("sCuotaCon",($ItemDos['cuota']));
						$tpl->set_var("sValorCon",
													round( (round($Item['value']) + (round($Item['value']) * ($ItemDos['rec']/100)))/($ItemDos['cuota']=='0'?1:$ItemDos['cuota']) )
												);
						$tpl->set_var("sValorSinFinCon",
													round( (round($Item['value']) + (round($Item['value']) * ($ItemDos['rec']/100))) ) 
												);
						$tpl->parse("ResultsBlock",true);
		  		}
	    	}
	    }
			$tpl->set_var("iId",$Item['id']);
			$tpl->set_var("sBrand",mb_convert_encoding($Item['brand'], 'UTF-8'));
			$tpl->set_var("sName",ucwords(mb_strtolower(mb_convert_encoding($Item['name'],'UTF-8'))));
			$tpl->set_var("sDescription",html_entity_decode(mb_convert_encoding($Item['description'], 'UTF-8')));
			$tpl->set_var("sDescription1",substr(mb_convert_encoding($Item['description'], 'UTF-8'),0,24).'...');
			$tpl->set_var("sFile",mb_convert_encoding($Item['file'], 'UTF-8'));
			$tpl->set_var("sFile1",mb_convert_encoding($Item['file1'], 'UTF-8'));
			$tpl->set_var("sFile2",mb_convert_encoding($Item['file2'], 'UTF-8'));
			$tpl->set_var("sFile3",mb_convert_encoding($Item['file3'], 'UTF-8'));
			$tpl->set_var("sModel",mb_convert_encoding($Item['model'], 'UTF-8'));
			$tpl->set_var("sCodBar",mb_convert_encoding($Item['codBar'], 'UTF-8'));
			$tpl->set_var("quiero",$_GET['art']);
						
			if ($Item['quantity'] > 0){
				$options = '';
				for ($w=1; $w<=$Item['quantity'] ;  $w++) {
					$options.='<option value="'.$w.'">'.$w.'</option>';
				}
				$tpl->set_var("options",$options);
			} else {
				$tpl->set_var("options",'<option value="0">0</option>');
			}
			
			if ($Item['cantidadCuotas']>0){
				$tpl->set_var("sCuotasNew",$Item['cantidadCuotas'].' cuotas de $ '.$Item['valorCuota']);
				$tpl->set_var("cft",'CFT: '.$parametros['cft'].'% ');
				$tpl->set_var("tea",'TEA: '.$parametros['tea'].'%');
				$tpl->set_var("iFinanciado",mb_convert_encoding($Item['valorFinanciado'], 'UTF-8'));
			} else {
				$tpl->set_var("sCuotasNew",'');
				$tpl->set_var("cft",'CFT: 0%');
				$tpl->set_var("tea",'TEA: 0%');
				$tpl->set_var("iFinanciado",mb_convert_encoding($Item['value'], 'UTF-8'));
			}
			if ($Item['envio']==0){
				$tpl->set_var("sEnvio",'ENVIO SIN CARGO');
			} else {
				$tpl->set_var("sEnvio",'CONSULTAR ENVIO');
			}
			
			$tpl->set_var("sPlan",mb_convert_encoding($Item['cuotas'], 'UTF-8'));
			//$tpl->parse("ProductosXArt",true);
			
		
  	}else {
			$tpl->set_var("sDisplayResul","display:none!important;");
			$tpl->set_var("sResultado","<h2>P�gina vac�a.</h2><br><p>No se han encontrado resultados...</p><br><img src='img/logo_inv.svg'>"); 
		}	
	}else {
		$tpl->set_var("sDisplayResul","display:none!important;");
		$tpl->set_var("sResultado","<h2>P�gina vac�a.</h2><br><p>No se han encontrado resultados...</p><br><img src='img/logo_inv.svg'>");
	}
}else{
	$tpl->set_var("sDisplayResul","display:none!important;");
	$tpl->set_var("sResultado","<h2>P�gina vac�a.</h2><br><p>No se han encontrado resultados...</p><br><img src='img/logo_inv.svg'>");
}
	
$tpl->pparse("main");
?> 