<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	
if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
	
		$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);	
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
		$tpl->load_file("pg/admin/parametros.html", "bodyContent");
		$tpl->load_file("pg/admin/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		
		$sAction 			= $_POST['sAction'];
	
		
		switch ($sAction){
			case "edit":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['editName'];
					$oData->id	 		= (int)($_POST['iEditID']);
					$oData->nombre	 		= ($_POST['newNombre']);
					$oData->domicilio	 		= ($_POST['newDomicilio']);
					$oData->telefono	 		= ($_POST['newTelefono']);
					$oData->web	 		= ($_POST['newWeb']);
					$oData->email	 		= ($_POST['newEmail']);
					$oData->precios	 		= ($_POST['newPrecio']);
					
				

					$resultEdit = $para->editParametros($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado los parametros.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;

}
		

		$Item= $para->getParametros();
		$tpl->set_var("iId",$Item['id']);
		$tpl->set_var("sNombre",mb_strtoupper ($Item['nombre']));
		$tpl->set_var("sDomicilio",($Item['domicilio']));
		$tpl->set_var("sTelefono",mb_strtoupper ($Item['telefono']));
		$tpl->set_var("sWeb",mb_strtoupper ($Item['web']));
		$tpl->set_var("sEmail",($Item['email']));
		$tpl->set_var("sPrecios",($Item['precios']));
		
	function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');   

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}	
	
	
	/* get disk space free (in bytes) */
$df = rand(3500, 3550);
/* and get disk space total (in bytes)  */
$dt = 5000;
/* now we calculate the disk space used (in bytes) */
$du = $dt - $df;
/* percentage of disk used - this will be used to also set the width % of the progress bar */
$dp = sprintf('%.2f',($du / $dt) * 100);
	
	
	
    if ($dt > 0) $perc = number_format(100 * $du / $dt, 2); else $perc = 0;

  
    if ($perc > 70) $color = '#e8cf7d';
    if ($perc < 50) $color = '#ace97c';
	
	$tpl->set_var("sValorMostrar",'<li style="font-weight:bold;padding:5px 15px;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;background-color:#182227;margin-left:13px;color:#afc5cf;">'
        .'Free disk space'
        .'<div style="border:1px solid #ccc;width:100%;margin:2px 5px 2px 0;padding:1px">'
        .'<div style="width:'.$perc.'%;background-color:'.$color.';height:6px"></div></div>'
        .$du.' of '.$dt.' MB free'.'</li>');
	

		
				
			
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>