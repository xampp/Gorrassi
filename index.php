<?php

include_once 'config.php';

$tpl->load_file("/slider/slider.html", "slider");
$tpl->load_file("/index/index.html", "bodyContent");

//$sUserName 	= $_POST['user'];
//$sPass	   	= $_POST['pass'];
$bBoll= !empty($_POST['user']) && isset($_POST['user']) && !empty($_POST['pass']) && isset($_POST['pass']);
if($bBoll){
	$sUserName 	= $_POST['user'];
  $sPass	   	= $_POST['pass'];
	$oUser = $user->Login($sUserName,$sPass);
}

if(count($oAdv->getSliders())>0){
	foreach ($oAdv->getSliders() as $Item){
			//$tpl->set_var("iId",$Item['id']);
		$tpl->set_var("sNameSlider",mb_convert_encoding($Item['name'], 'UTF-8'));  
		$tpl->set_var("sDescriptionSlider",mb_convert_encoding($Item['description'], 'UTF-8'));
		$tpl->set_var("sImgSlider",mb_convert_encoding($Item['img'], 'UTF-8'));
		$tpl->set_var("iIdSliderEncrip",$utils->encrypt($Item['id'],"sapopep16+3/"));
		$tpl->parse("Slider",true);
	}
} 
//var_dump($tpl);exit;
$parametros=$para->getParametros();

if(count($oAdv->getProductsOffer())>0){
	foreach ($oAdv->getProductsOffer() as $Item){
		//var_dump($Item);exit;
		$tpl->set_var("iId",$Item['id']);
		$tpl->set_var("iIdEncrip",$utils->encrypt($Item['id'],"sapopep16+3/"));
		$tpl->set_var("sBrand",mb_strtoupper(mb_convert_encoding($Item['brand'], 'UTF-8')));
		$tpl->set_var("sName",mb_strtoupper(mb_convert_encoding($Item['name'], 'UTF-8')));
		$tpl->set_var("sModel",mb_strtolower($Item['model'], 'UTF-8'));
		$tpl->set_var("sFile",mb_convert_encoding($Item['file'], 'UTF-8'));
		$tpl->set_var("sFile1",mb_convert_encoding($Item['file1'], 'UTF-8'));
		$tpl->set_var("sFile2",mb_convert_encoding($Item['file2'], 'UTF-8'));
		$tpl->set_var("sFile3",mb_convert_encoding($Item['file3'], 'UTF-8'));
		//$tpl->set_var("sDescription",mb_strtolower(mb_convert_encoding($Item['description'], 'UTF-8')));
		
		//$tpl->set_var("sDescription1",substr(utf8_encode($Item['description']),0,24).'...');
		$tpl->set_var("sDescription1",mb_strtolower($oAdv->wordlimit($Item['description'],3), 'UTF-8'));		
		if ($parametros['precios']=='S') {
			$tpl->set_var("iValue",(round($Item['value'])));
		}
							
		if ($Item['cantidadCuotas']>0){
			
		$tpl->set_var("sCartel",'Con Crédito Personal');
		$tpl->set_var("sCuotasNew",$Item['cantidadCuotas'].' cuotas de $ '.$Item['valorCuota']);
		$tpl->set_var("cft",'CFT: '.$parametros['cft'].'% ');
		$tpl->set_var("tea",'TEA: '.$parametros['tea'].'%');
		$tpl->set_var("iFinanciado",mb_convert_encoding($Item['valorFinanciado'], 'UTF-8'));
		} else {
			$tpl->set_var("sCuotasNew",'');
			$tpl->set_var("cft",'CFT: 0%');
			$tpl->set_var("tea",'TEA: 0%');
			$tpl->set_var("iFinanciado",mb_convert_encoding($Item['value'], 'UTF-8'));
		}
											
		$tpl->set_var("sPlan",mb_convert_encoding($Item['cuotas'], 'UTF-8'));
										
		$tpl->parse("Productos",true);					
	}
}

$tpl->pparse("main");

?>
