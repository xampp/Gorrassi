<?php
include_once 'config.php';

//var_dump("hola");exit;
	
if (isset($_POST['user'])&&isset($_POST['pass'])){

	$sUserName 	= $_POST['user'];
	$sPass	   	= $_POST['pass'];

	if($sUserName && $sPass){
		$oUser = $user->Login($sUserName,$sPass);
	}
}	
	
if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"])){
	
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser'])) ) {
		//Si es administrador ingresa
		header('location: cpanel_admin.php');
	} else {
		if ($user->can(IS_SECURITY,$_SESSION['sysUser'])) {
		//Si es seguridad ingresa
		header('location: cpanel_security.php');
	  } else {
		//Si es usuario normal
		  if ($user->can(IS_USER,$_SESSION['sysUser'])) {
		    header('location: cpanel_user.php');
		  }
	  }
	}

	$login=htmlspecialchars(session_id());
	$oData=$user->getName();
	$tpl->set_var("sSysUserName",$oData['lastName'].' , '.$oData['name']);
	
}else{
	if (isset($oUser)){
	   if(!is_object($oUser)&&$oUser){
		  $tpl->set_var("sDisplay","");
		  $tpl->set_var("Erros",$oUser);
	  }
	}else{
		$tpl->set_var("sDisplay","none");
	}
	
		$tpl->load_file("pg/login/login.html", "bodyContent");
	
	$login="";
	$tpl->set_var("ver",$login);
	$sPath = $_SERVER['PHP_SELF'];
	$sFile = basename($sPath);
	if($sFile != "index.php"  && $sFile != "login.php"){
		header("location:".URL_BASE);
	} 
}

$tpl->pparse("main");

?>