<?php

/* Establecemos que las paginas no pueden ser cacheadas */
header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include_once ("includes/cnx.inc");
include_once ("includes/class.Templates.php");
include_once ("includes/class.Database.php");
include_once ("includes/class.Utils.php");
include_once ("includes/class.Adv.php");
include_once ("includes/class.Archivos.php");
include_once ("includes/constants.php");
include_once ("includes/class.User.php");
include_once ("includes/class.Parametros.php");
include_once ("includes/class.Informacion.php");
include_once ("includes/class.Customer.php");

session_start();

$inactividad = 600;
// Comprobar si $_SESSION["timeout"] está establecida
if(isset($_SESSION["timeout"])){
  // Calcular el tiempo de vida de la sesión (TTL = Time To Live)
  $sessionTTL = time() - $_SESSION["timeout"];
  if($sessionTTL > $inactividad){
      //session_destroy();
      header("Location: /logout.php");
  }
}
// El siguiente key se crea cuando se inicia sesión
$_SESSION["timeout"] = time();

$tpl			= new Templates("templates");
$db 			= new DB($cnx);
$utils 		= new Utils($cnx);
$oArchivo	= new Archivo();
$oAdv     = new Adv($cnx);
$para			= new Parametros($cnx);
$info			= new Informacion($cnx);
$user 		= new user($cnx);
$custo 		= new Customer($cnx);

date_default_timezone_set("America/Argentina/Buenos_Aires");

$sPath = $_SERVER['PHP_SELF'];
$sFile = basename($sPath);
	
//	if($sFile != "cpanel_admin.php"  && $sFile != "cpanel_user.php" && $sFile != "users.php" && $sFile != "advertising.php"  && $sFile != "profile.php" && $sFile != "bill.php" && $sFile != "login.php" && $sFile != "userRegister.php" && $sFile != "viewMail.php"){
if (isset($_POST['search'])){
	$name = $_POST['search'];
	$tpl->set_var("sValueSearchBus",$name);
}
	
if(!$utils->pageWhitOutBar($cnx,$sFile)){
	
	if($sFile<>"orders.php"){
		$tpl->load_file("frame.html","main");
		$tpl->load_file("/header/header.html","header");
		$tpl->load_file("/menu/menu.html","menu");
		$tpl->load_file("/footer/footer.html","footer");
		
		$i=0;	
	  foreach ($oAdv->getCategorys() as $Item){
			$tpl->set_var("sSub",$utils->encrypt($Item['id'],"sapopep16+3/"));
			$tpl->set_var("iIdMenu",$Item['id']);
			$tpl->set_var("sDescriptionMenu",($Item['description']));
			
			
			$i=($Item['prueba']);
			
			if ($i){
				$array = explode(",", $i);
				$sub = "<ul>";

				foreach ($array as $ItemSub){
					$IdSub = explode("-", $ItemSub);
					$sub.= "<li><a href='products.php?art=".$utils->encrypt($Item['id'].'/'.$IdSub['0'],"sapopep16+3/")."'>".$IdSub['1']."</a></li>";
				}
				$sub.= "</ul>";
				$tpl->set_var("sSubmenu",$sub);
			} else {
				$tpl->set_var("sSubmenu","");
			}
		
			//$tpl->parse("MenuF",true);
			$tpl->parse("Menu",true);
				
		}
	}	
			
} else {
	//pagina privada
	$tpl->load_file("frame.html","main");
	$tpl->load_file("/pg/header/header.html","header");
	//$tpl->load_file("/pg/menu/menu.html","menu");
	$tpl->load_file("/pg/footer/footer.html","footer");
}
 


/*
$tpl->load_file("frame.html","main");
$tpl->load_file("/header/header.html","header");
$tpl->load_file("/menu/menu.html","menu");
$tpl->load_file("/footer/footer.html","footer");
*/


?>