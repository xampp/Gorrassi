<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser']))) {
		
		$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);					
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		
		if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
			
			$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
		
	} 
		
		$tpl->load_file("pg/admin/services.html", "bodyContent");
		$tpl->load_file("pg/admin/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		//$tpl->set_var("sSysUserName","Administrador");
		$sAction 			= isset($_POST['sAction'])? $_POST['sAction']:null;
		
		switch ($sAction){
			case "editServices":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['editNombre'];
					$oData->phone = ($_POST['editTelefono']);
					$oData->cellPhone = ($_POST['editCelular']);
					$oData->email = ($_POST['editEmail']);
					$oData->obser = ($_POST['editObs']);
					$oData->brand = (int)($_POST['editMarca']);
					$oData->type = (int)($_POST['editTipo']);
					$oData->address = ($_POST['editDomicilio']);
					$oData->id	 		= (int)($_POST['iEditID']);
					
					$resultEdit = $oAdv->editServices($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado el service.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "searchServicesById":
	
		$oData = $oAdv->getServicesById($_POST['iID']);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	case "newServices":

					$oData = new stdClass();
					$oData->name	= $_POST['newNombre'];
					$oData->address = htmlentities($_POST['newDomicilio']);
					$oData->phone = htmlentities($_POST['newTelefono']);
					$oData->cellPhone = ($_POST['newCelular']);
					$oData->email = ($_POST['newEmail']);
					$oData->observaciones = htmlentities($_POST['newObs']);
					$oData->brand = htmlentities($_POST['newMarca']);
					$oData->type = (int)($_POST['newTipo']);
					$result = $oAdv->newServices($oData);

					if ($result->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado un Nuevo Servicio.");
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}
					 
					
	break;
	case "deleteServices":
		
			$oData = $oAdv->deleteServicesById($_POST['iID']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	default:
	break;
}
		
if(count($oAdv->getServicesOn())>0){
		foreach ($oAdv->getServicesOn() as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sNombre",mb_convert_encoding($Item['name'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sMarca",mb_convert_encoding($Item['brand'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sDomicilio",mb_convert_encoding($Item['address'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sTelefono",mb_convert_encoding($Item['phone'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sCelular",mb_convert_encoding($Item['cellPhone'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sObservaciones",mb_convert_encoding($Item['observaciones'],'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ResultsBlock",true);
			}
	} 
	
	
if(count($oAdv->getMarcas())>0){
		foreach ($oAdv->getMarcas() as $Item){
				$tpl->set_var("iMarca",$Item['id']);
				$tpl->set_var("sMarca",mb_convert_encoding($Item['name'],'UTF-8', 'ISO-8859-1'));
				$tpl->parse("MarcasBlock",true);
			}
	} 
			

			
			
			
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>