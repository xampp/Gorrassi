<?php
include_once 'config.php';


if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser']))) {
	
			$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);	
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
			$tpl->set_var("iId",$oData['id']);	
		
		if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
			$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
		
	} 
		
		$tpl->load_file("pg/admin/main.html", "bodyContent");
		$tpl->load_file("pg/admin/menu.html","menu");
		
		
		
		//$tpl->set_var("iReservas",count($oAdv->getService()));
		//$tpl->set_var("iReclamos",count($oAdv->getProducts()));
	
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>