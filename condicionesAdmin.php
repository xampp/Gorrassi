<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser']))) {
				
		$oData=$user->getName();
		$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
		$tpl->set_var("sSysUserLetter",$oData['name'][0]);			
		$tpl->set_var("sSysUserEmail",$oData['userName']);	
		$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		
		
		if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
			
			$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
	  } 
		
		$tpl->load_file("pg/admin/condicion.html", "bodyContent");
		$tpl->load_file("pg/admin/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		//$tpl->set_var("sSysUserName","Administrador");
		$sAction = isset($_POST['sAction']) ? $_POST['sAction'] : null;		
		
		switch ($sAction){
			case "editCondicion":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['editName'];
					$oData->id	 		= (int)($_POST['iEditID']);
					$oData->cuota	 		= (int)($_POST['editCuota']);
					$oData->rec	= $_POST['editRec'];
					$oData->visible	= (int)$_POST['editVisible'];
					

					$resultEdit = $oAdv->editCondicion($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado la condición.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}					
						
		  break;
			case "searchCondicionById":
			
				$oData = $oAdv->getCondicionById($_POST['ediID']);
				//var_dump($oData);exit;
				if($oData['queryStatus'] != "OK"){
					$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
				}
				echo json_encode($oData);
				exit;
			break;
			case "newCondicion":

				$oData = new stdClass();
				$oData->name	= $_POST['newName'];
				$oData->rec	= $_POST['newRec'];
				$oData->cuota	 		= (int)($_POST['newCuota']);
				$oData->visible	= (int)$_POST['newVisible'];
				
						
				$result = $oAdv->newCondicion($oData);
				
				if ($result->status=="OK"){
					$tpl->set_var("sDisplayError","display:none;");
					$tpl->set_var("sDisplayOK","");
					$tpl->set_var("sResult","A agregado una nueva condición.");
				}else {
					$tpl->set_var("sDisplayError","");
					$tpl->set_var("sDisplayOK","display:none;");
					$tpl->set_var("sResult","");
				}
							
			break;
			case "deleteCondicion":
								
					$oData = $oAdv->deleteCondicionById($_POST['iID']);
					if($oData->status != "OK"){
						$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
					}
					
				echo json_encode($oData);
				exit;
			break;
			default:
			break;
		}
		
		if(count($oAdv->getCondiciones())>0){
			foreach ($oAdv->getCondiciones() as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sName",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sRec",($Item['rec']));
				$tpl->set_var("sCuota",($Item['cuota']));
				$tpl->set_var("sVisible",($Item['visible']));
				$tpl->parse("ResultsBlock",true);
			}
		}
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>