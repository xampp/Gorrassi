<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
	
		$oData=$user->getName();
		$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
		$tpl->set_var("sSysUserLetter",$oData['name'][0]);					
		$tpl->set_var("sSysUserEmail",$oData['userName']);	
		$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
		$tpl->load_file("pg/admin/usuarios.html", "bodyContent");
		$tpl->load_file("pg/admin/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		
		$sAction 	= isset($_POST['sAction'])?$_POST['sAction']:null;
		switch ($sAction){
			case "newUser":
					$oData					= new stdClass();
					$oData->name 			= htmlentities($_POST['newNombre']);
					$oData->lastName	 	= htmlentities($_POST['newApellido']);
					$oData->address 		= htmlentities($_POST['newDomicilio']);
					$oData->userName		= $_POST['newEmail'];
					$oData->password  		= $_POST['newPassword'];
					$oData->phone	 		= $_POST['newTelefono'];
					$oData->status	 		= 1;
					//$oData->last_ip	 		= $_SERVER['REMOTE_ADDR'];
					$oData->sha	 			= sha1($oData->password . $oData->userName);
					$idUser = $user->getName();
					$oData->sysUsersType	= (int)$_POST['newTipo'];
					$oData->idAdmin	 		= (int)$idUser['id'];
					
					$result = $user->newUser($oData);
					if ($result->status=="OK"){
						$newEmail=$utils->sendEmail($oData->userName, 'Bienvenido al sistema', 'Sus datos para acceder al sistema son:<br><br>Usuario : '.$oData->userName.'<br>Password : '.$oData->password.'<br><br>Para acceder ingrese a <a href=https://pascualgorrassi.com.ar>https://pascualgorrassi.com.ar</a><br><br>');
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado un Nuevo Usuario.");
						//SI TIENE UNIDADES ASIGNADAS no va mas para aca
						/* $i=1;
						while ($_POST['newIdUnidad-'.$i]) {
							$oDataU				= new stdClass();
							$oDataU->idUser 	= (int)$result->lastId;
							$oDataU->id	 		= (int)($_POST['newIdUnidad-'.$i]);
						    $resultUnidad = $edi->editUnidadUser($oDataU);
						$i++;
						}*/
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}

			break;
			case "searchUserById":
			
				$oData = $user->searchUserById($_POST['userID']);
				if($oData['queryStatus'] != "OK"){
					$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
				}
				echo json_encode($oData);
				exit;
			break;
			case "searchLogById":
			
				$oData = $user->searchLogById($_POST['ID']);
				if($oData['queryStatus'] != "OK"){
					$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
				}
				echo json_encode($oData);
				exit;
			break;
			case "editUser":
							$oData					= new stdClass();
							$oData->name 			= htmlentities($_POST['editNombre']);
							$oData->lastName	 	= htmlentities($_POST['editApellido']);
							$oData->address 		= htmlentities($_POST['editDomicilio']);
							$oData->phone	 		= $_POST['editTelefono'];
							$oData->id	 		= (int)($_POST['iEditID']);
							$oData->userName		= $_POST['editEmail'];
							$oData->password  		= $_POST['editPassword'];
							$oData->sysUsersType	= (int)$_POST['editTipo'];
							$oData->sha	 			= sha1($oData->password . $oData->userName);
							
							
							$result = $user->editUser($oData);
							if ($result->status=="OK"){
								//$newEmail=$utils->sendEmail($oData->userName, 'MODIFICACION EN BETA', 'Su nombre de Usuario para el ingreso es: '.$oData->userName.'<br> Su Password: '.$oData->password.'<br><br>Un cordial saludo.', null, 'sistema@vspace.com.ar',null,null);
								$tpl->set_var("sDisplayError","display:none;");
								$tpl->set_var("sDisplayOK","");
								$tpl->set_var("sResult","A modificado el Usuario.");
								
							}else {
								$tpl->set_var("sDisplayError","");
								$tpl->set_var("sDisplayOK","display:none;");
								$tpl->set_var("sResult","");
							}
			break;
			case "deleteUser":
					$oData = $user->deleteUserById($_POST['iID']);
					if($oData->status != "OK"){
						$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
					}

				echo json_encode($oData);
				exit;
			break;
			default:
			break;
		}
    $aRes= $user->getUsers($_SESSION['sysUser']);
	  $bBoll= !is_null($aRes);
    if ($bBoll){
			if (count($aRes)>0){
			 foreach ($aRes as $Item){
					$tpl->set_var("iId",$Item['id']);
					$tpl->set_var("sNombre",mb_convert_encoding($Item['nombre'],'UTF-8', 'ISO-8859-1'));
					$tpl->set_var("sDomicilio",mb_convert_encoding($Item['address'],'UTF-8', 'ISO-8859-1'));
					$tpl->set_var("sTelefono",mb_convert_encoding($Item['phone'],'UTF-8', 'ISO-8859-1'));
					$tpl->set_var("sRol",mb_convert_encoding($Item['rol'],'UTF-8', 'ISO-8859-1'));
					$tpl->parse("USUARIOS",true);
				}	
			}
    }
			
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>