<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017")){
	if ($user->can(IS_USER,$_SESSION['sysUser'])) {
		
		$tpl->load_file("pg/user/history.html", "bodyContent");
		$tpl->load_file("pg/user/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		$oData=$user->getName();
		$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
		$tpl->set_var("sSysUserLetter",$oData['name'][0]);	
		$tpl->set_var("sSysUserEmail",$oData['userName']);	
		$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		$sAction 			= $_POST['sAction'];
		
		switch ($sAction){
			case "editService":
			  
					$oData = new stdClass();
		
					
					$oData->id	 		= (int)($_POST['iEditID']);
				
					$oData->comentario	 		= $_POST['editComentarios'];
					
					
					$resultEdit = $oAdv->editServiceMode($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado el service.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "searchServiceById":
	
		$oData = $oAdv->getServiceById($_POST['iID']);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	
	case "newService":
	
		$oData = new stdClass();
		$oData->name	= $_POST['newNombre'];
		$oData->lastName	= $_POST['newApellido'];
		$oData->dni	= $_POST['newDNI'];
		$oData->email = htmlentities($_POST['newEmail']);
		$oData->phone = htmlentities($_POST['newTelefono']);
		$oData->descrip = ($_POST['newAcc']);
		$oData->problem = ($_POST['newProblema']);
		$oData->obser = htmlentities($_POST['newObservaciones']);
		$oData->status = 1;
		$oData->credito = htmlentities($_POST['newCredito']);
		$oData->marca = (int)($_POST['newMarca']);
		$oData->modelo = htmlentities($_POST['newModelo']);
		$oData->serie = htmlentities($_POST['newSerie']);
		$oData->fecha = htmlentities($_POST['newFecha']);
		$oData->recibe = htmlentities($_POST['newRecibe']);
		$oData->articulo = htmlentities($_POST['newArticulo']);
		$oData->bill = htmlentities($_POST['newFactura']);
		

		$result = $oAdv->newService($oData);
		
		
		if($result->status != "OK"){
			$result->status = $db->getLabel("lbl_".$oData->status,"SPA");
		}
		
		echo json_encode($result);
		exit;
				
	break;
	
	/*case "newService":

					$oData = new stdClass();
					$oData->name	= $_POST['newNombre'];
					$oData->lastName	= $_POST['newApellido'];
					$oData->dni	= $_POST['newDNI'];
					$oData->email = htmlentities($_POST['newEmail']);
					$oData->phone = htmlentities($_POST['newTelefono']);
					$oData->descrip = ($_POST['newAcc']);
					$oData->problem = ($_POST['newProblema']);
					$oData->obser = htmlentities($_POST['newObservaciones']);
					$oData->status = 1;
					$oData->credito = htmlentities($_POST['newCredito']);
					$oData->marca = (int)($_POST['newMarca']);
					$oData->modelo = htmlentities($_POST['newModelo']);
					$oData->serie = htmlentities($_POST['newSerie']);
					$oData->fecha = htmlentities($_POST['newFecha']);
					$oData->recibe = htmlentities($_POST['newRecibe']);
					$oData->articulo = htmlentities($_POST['newArticulo']);
					$oData->bill = htmlentities($_POST['newFactura']);
					

					$result = $oAdv->newService($oData);
	

					if ($result->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado un Nuevo Servicio.");
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}
					 
					
	break;*/
	case "deleteService":
		
			$oData = $oAdv->deleteServiceById($_POST['iID']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	default:
	break;
}
		
/*
if(count($oAdv->getServiceNews())>0){
		foreach ($oAdv->getServiceNews() as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sApellido",utf8_encode($Item['lastName']));
				$tpl->set_var("sNombre",utf8_encode($Item['name']));
				$tpl->set_var("sDNI",utf8_encode($Item['dni']));
				$tpl->set_var("sCredito",utf8_encode($Item['credito']));
				
				$tpl->set_var("sFactura",utf8_encode($Item['credito']));
				$tpl->set_var("sFechaCompra",utf8_encode($Item['fecha']));
				$tpl->set_var("sArticulo",utf8_encode($Item['modelo']));
				
				$tpl->set_var("sFecha",utf8_encode($Item['beginDate']));
				$tpl->set_var("sFechaB",utf8_encode($Item['beginDate']));
				$tpl->set_var("sMarca",utf8_encode($Item['marca']));
				$tpl->set_var("sSerie",utf8_encode($Item['serie']));
				$tpl->set_var("sTelefono",utf8_encode($Item['phone']));
				$tpl->set_var("sEstado",utf8_encode($Item['status']));
				$tpl->parse("ResultsBlock",true);
			}
	} */
	
if(count($oAdv->getService(0))>0){
		foreach ($oAdv->getService(0) as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sNombre",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sNumero",mb_convert_encoding($Item['numero'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sModelo",mb_convert_encoding($Item['modelo'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sApellido",mb_convert_encoding($Item['lastName'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFecha",mb_convert_encoding($Item['fecha'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaB",mb_convert_encoding($Item['beginDate'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sMarca",mb_convert_encoding($Item['marca'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sSerie",mb_convert_encoding($Item['serie'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sTelefono",mb_convert_encoding($Item['phone'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sEstado",mb_convert_encoding($Item['status'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ResultsBlock",true);
			}
	}
	
if(count($oAdv->getMarcas())>0){
		foreach ($oAdv->getMarcas() as $Item){
				$tpl->set_var("iMarca",$Item['id']);
				$tpl->set_var("sMarca",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("MarcasBlock",true);
			}
	} 
	
	/*if(count($oAdv->getStatus())>0){
		foreach ($oAdv->getStatus() as $Item){
				$tpl->set_var("iEstado",$Item['id']);
				$tpl->set_var("sEstado",utf8_encode($Item['name']));
				$tpl->parse("EstadoBlock",true);
			}
	} */
	
	
	/*if(count($oAdv->getServicesOn())>0){
		foreach ($oAdv->getServicesOn() as $Item){
				$tpl->set_var("iServicio",$Item['id']);
				$tpl->set_var("sServicio",utf8_encode($Item['name'].' - '.$Item['address'].' / '.$Item['phone']));
				$tpl->parse("ServiciosOnBlock",true);
			}
	} */

if(count($oAdv->getStatus())>0){
		foreach ($oAdv->getStatus() as $Item){
				$tpl->set_var("iEstado",$Item['id']);
				$tpl->set_var("sEstado",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("EstadoBlock",true);
			}
	} 
		
	if(count($oAdv->getServicesOnIn())>0){
		foreach ($oAdv->getServicesOnIn() as $Item){
				$tpl->set_var("iServicio",$Item['id']);
				$tpl->set_var("sServicio",mb_convert_encoding($Item['name'].' - '.$Item['address'].' / '.$Item['phone'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ServiciosOnBlock",true);
			}
	} 	
					
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>