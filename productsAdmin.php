<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser']))) {
		
		$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);	
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
			
		
		
		if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
			
			$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
		
	} 
		
		$tpl->load_file("pg/admin/products.html", "bodyContent");
		$tpl->load_file("pg/admin/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		//$tpl->set_var("sSysUserName","Administrador");
		$sAction 	= isset($_POST['sAction'])?$_POST['sAction']:null;	
		$permitidos = array("image/jpg", "image/jpeg", "image/gif", "image/png");
		$limite_kb = 100000000;
		switch ($sAction){
			case "editInformacion":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['editName'];
					$oData->id	 		= (int)($_POST['iEditID']);
					$oData->brand	 		= (int)($_POST['editMarca']);
					$oData->description = ($_POST['editDescription']);
					$oData->quantity	 		= (int)($_POST['editQuantity']);
					
					$c = count($_FILES);					
					if ($c<=4 && $c){
						for ($i=0; $i < $c ; $i++) {
							$l= $i>0?$i:"";							
							if ($_FILES["fileUploadEdit".$l]["tmp_name"]<>""){
								if(in_array($_FILES['fileUploadEdit'.$l]['type'], $permitidos) && $_FILES['fileUploadEdit'.$l]['size'] <= $limite_kb * 1024){
									$oName=$oArchivo->savePro($_FILES["fileUploadEdit".$l]["tmp_name"],'img_prod/'.$_FILES["fileUploadEdit".$l]["name"],$i+1);
									$oData->{'file'.$l} = $oName;
									if ($_POST['editFile'.$l]<>'sinImagen.jpg'){
										//unlink("img_prod/".$_POST['editFile']);
									}
									
								}else{
									$oData->{"file".$l} = 'sinImagen.jpg';
								}
							} else {
								$oData->{"file".$l} = htmlentities($_POST['editFile'.$l]);
							}
						}
					}
					
					$oData->value = htmlentities($_POST['editValue']);
					$oData->cuotas = htmlentities($_POST['editCuotas']);
					$oData->category = (int)($_POST['editCategory']);
					$oData->subcategory = (int)($_POST['editSubCategory']);
					$oData->cuotas = htmlentities($_POST['editCuotas']);
					$oData->status = (int)($_POST['editStatus']);
					$oData->offer = (int)($_POST['editOferta']);
					$oData->envio = (int)($_POST['editEnvio']);
					$oData->conditionPay = (int)($_POST['editCondiciones']);
					$oData->codBar = $_POST['editCodigoBarras'];
					
					/*$oData->valorFinanciado = htmlentities($_POST['editPtf']);
					$oData->valorCuota = htmlentities($_POST['editValorCuota']);
					$oData->cantidadCuotas = htmlentities($_POST['editCantidadCuotas']);*/
					
					$oData->valorFinanciado = 1;
					$oData->valorCuota = 1;
					$oData->cantidadCuotas = 1;
										
					$oData->model = htmlentities($_POST['editModel']);
					if ($_POST['editValue']<>$_POST['iCopiaValor']){
						$oData->dateUpdate = date("Y-m-d"); 
					} else {
						$oData->dateUpdate = $_POST['iCopiaDia']; 
					}

					$resultEdit = $oAdv->editProducts($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado la Información.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "newInformacionUno":
			  
			$oData = new stdClass();
			$oData->name	= $_POST['newName'];
			$oData->description = ($_POST['newDescription']);
			
			$oData->brand	= (int)$_POST['newMarca'];
			$oData->quantity	 		= (int)($_POST['newQuantity']);
			/*$oData->valorFinanciado = htmlentities($_POST['newPtf']);
			$oData->valorCuota = htmlentities($_POST['newValorCuota']);
			$oData->cantidadCuotas = htmlentities($_POST['newCantidadCuotas']);*/
			
			$oData->valorFinanciado = 1;
			$oData->valorCuota = 1;
			$oData->cantidadCuotas = 1;
			
						
			$oData->conditionPay = (int)($_POST['newCondiciones']);
			$oData->value = htmlentities($_POST['newValue']);
			$oData->cuotas = htmlentities($_POST['newCuotas']);
			$oData->category = (int)($_POST['newCategory']);
			$oData->subcategory = (int)($_POST['newSubCategory']);
			$oData->cuotas = htmlentities($_POST['newCuotas']);
			$oData->offer = (int)($_POST['newOferta']);
			$oData->status = 1;
			$oData->model = htmlentities($_POST['newModel']);
			$oData->envio = (int)($_POST['newEnvio']);
			$oData->codBar = ($_POST['newCodigoBarras']);


			if(in_array($_FILES['fileUpload']['type'], $permitidos) && $_FILES['fileUpload']['size'] <= $limite_kb * 1024){

			$oName=$oArchivo->savePro($_FILES["fileUpload"]["tmp_name"],'img_prod/'.$_FILES["fileUpload"]["name"],1);				
				
			if($oName){
				$oData->file = $oName;
				$c = count($_FILES);
				if ($c<=4 && $c){
					for ($i=1; $i < $c ; $i++) {
						if(in_array($_FILES['fileUpload'.$i]['type'], $permitidos) && $_FILES['fileUpload'.$i]['size'] <= $limite_kb * 1024){
							$oName=$oArchivo->savePro($_FILES["fileUpload".$i]["tmp_name"],'img_prod/'.$_FILES["fileUpload".$i]["name"],$i+1);
							if($oName){
								$oData->{"file".$i} = $oName;
							}else{
								$oData->{"file".$i} = 'sinImagen.jpg';
							}
		    		}
		    	}
		    }
							
				$resultEdit = $oAdv->register($oData);
				
				if ($resultEdit->status=="OK"){
					$tpl->set_var("sDisplayError","display:none;");
					$tpl->set_var("sDisplayOK","");
					$tpl->set_var("sResult","A agregado la Información.");					
				}else {
					$tpl->set_var("sDisplayError","");
					$tpl->set_var("sDisplayOK","display:none;");
					$tpl->set_var("sResult","");					
				}
			}else {
				$tpl->set_var("sDisplayError","");
				$tpl->set_var("sDisplayOK","display:none;");
				$tpl->set_var("sResult","");					
			}
		} else {
			$tpl->set_var("sDisplayError","");
			$tpl->set_var("sDisplayOK","display:none;");
			$tpl->set_var("sResult","");			
		}			
					
	break;
	case "searchInformacionById":
	
		$oData = $oAdv->getProductsById($_POST['iID']);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	
		case "editConceptById":
	
		$oData = $oAdv->editConceptById($_POST['ID'],$_POST['val']);
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
		break;
		
	case "newInformacion":

					$oName=$oArchivo->saveA($_FILES["fileUpload"]["tmp_name"],'temp/'.$_FILES["fileUpload"]["name"]);
					$result = $oAdv->newAdvUp($oName);
					
					if ($result->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado Nuevos Productos.");
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}
								 
					
	break;
	case "searchByCategorias":
	
		$oData = $oAdv->getSubCategorias((int)$newCate);
		echo json_encode($oData);
		exit;
	break;
	case "deleteInformacion":
		
			$oData = $oAdv->deleteProductById($_POST['iID']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	
	case "sendByDelete":
		
			$oData = $oAdv->deleteProductByIds($_POST['productosDelete']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	
	
	
	default:
	break;
}
		
if(count($oAdv->getProducts())>0){
		foreach ($oAdv->getProducts() as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sName",mb_strtoupper ($Item['name']));
				//$tpl->set_var("sDescription",(utf8_encode($Item['description'])));
				$tpl->set_var("iValue",($Item['value']));
				$tpl->set_var("sQuantity",($Item['quantity']));
				//$tpl->set_var("sCategory",utf8_encode($Item['category']));
				//$tpl->set_var("sSubCategory",utf8_encode($Item['subcategory']));
				$tpl->set_var("sMarca",mb_strtoupper ($Item['brand']));
				//$tpl->set_var("sPlan",utf8_encode($Item['cuotas']));
				$tpl->set_var("sModel",mb_strtoupper ($Item['model']));
				$tpl->set_var("sEnvio",($Item['envio']));
				$tpl->set_var("sVisible",($Item['visible']));
				$tpl->set_var("sUpdate",($Item['sUpdate']));
				$tpl->set_var("sCuotas",($Item['cuotas']));
				
				$tpl->parse("ResultsBlock",true);
			}
	} 
	

if(count($oAdv->getMarcas())>0){
		foreach ($oAdv->getMarcas() as $Item){
				$tpl->set_var("iMarca",$Item['id']);
				$tpl->set_var("sMarca",($Item['name']));
				$tpl->parse("MarcasBlock",true);
			}
	} 	
	
	if(count($oAdv->getCategorias())>0){
		foreach ($oAdv->getCategorias() as $Item){
				$tpl->set_var("iCategory",$Item['id']);
				$tpl->set_var("sCategory",($Item['description']));
				$tpl->parse("CategoriasBlock",true);
			}
	} 	
	

	if(count($oAdv->getCondiciones(1))>0){
				foreach ($oAdv->getCondiciones(1) as $ItemDos){
				$tpl->set_var("sIdCon",$ItemDos['id']);
				$tpl->set_var("sNameCon",$ItemDos['cuota'].' - '.$ItemDos['name']);
				//$tpl->set_var("sCuotaCon",($ItemDos['cuota']));
				//$tpl->set_var("sValorCon", round(( round($Item['value']) + (round($Item['value']) * ($ItemDos['rec']/100)))/$ItemDos['cuota'])           );
				//$tpl->set_var("sValorSinFinCon", round(( round($Item['value']) + (round($Item['value']) * ($ItemDos['rec']/100))))           );
				$tpl->parse("ResultsBlockCon",true);
			}
	} 
		
			
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>