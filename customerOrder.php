<?php
include_once("config.php");


if ( !empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017")){
	
set_time_limit(0);
$idCus =(int)$_REQUEST['idCus'];


if ((isset($idCus)) && ($idCus!=0)) {

	$oConfig = $para->getConfigSystem();
	$oBillData=$custo->searchCustomerById($idCus);
	//echo "<Pre>";
    //var_dump($oConfig->web);
    //var_dump($oBillData);exit;
		
	require_once("includes/pdf/fpdf.php");

	class PDF extends FPDF{
		//Cabecera de página
		function __construct()
       {
          parent::FPDF();
       }
		function Header(){
		    
		   $this->SetLineWidth(1);
			$this->SetFillColor(93,181,240);
		    $this->Rect(0,0,210,20,'F');
			global $vDataHeader;
			global $oConfig;
			//Logo
			$this->Image($oConfig->logo,8,5,13);
			
			$this->SetFont('Arial','B',10);
			//Movernos a la derecha
			$this->Cell(40,1);
			//Título
			$this->Cell(80,5,$oConfig->name,0,1,'L');
			
			
			
			//Arial bold 15
			$this->SetFont('Arial','',6);
			//Movernos a la derecha
			$this->Cell(60);
			//Título
			$this->Cell(2,3,$oConfig->address,0,1,'C');
			$this->Cell(60);
			$this->Cell(2,3,$oConfig->phone,0,1,'C');
			$this->Cell(60);
			$this->Cell(2,3,'(7600) Mar del Plata - Pcia. Buenos Aires',0,1,'C');
			
			
			$this->SetFont('Arial','B',11);
			$this->Ln(2);
			$this->Cell(140);
			$this->Cell(20,6,mb_strtoupper('Cliente: '),0,1,'R');
			//$this->Cell(4);
			//$this->Cell(10,6,$vDataHeader[0],0,1,'L');
			$this->SetFont('Arial','',7);
			$this->Cell(140);
			$this->Cell(20,6,mb_strtoupper('DNI: '),0,1,'R');
			//$this->SetFont('Arial','B',11);
			//$this->Cell(150);
			//$this->Cell(20,6,$vDataHeader[2],0,1,'L');
			//Salto de línea
			//$this->Ln(7);
			$this->Cell(140);
			$this->SetFont('Arial','',7);
			$this->Cell(20,6,mb_strtoupper('Fecha Nacimiento: '),0,1,'R');
			$this->Cell(140);
			$this->SetFont('Arial','',6);
			$this->Cell(20,6,mb_strtoupper('Fecha Alta: '),0,1,'R');	
			//$this->Cell(20,4,$vDataHeader[1],0,1,'L');
					
		}		

			//Pie de página
		function Footer(){
			global $oConfig;
			//Posición: a 1,5 cm del final
			$this->SetY(-15);
			//Arial italic 8
			$this->SetFont('Arial','I',8);
			//Número de página
			$this->SetFillColor(240);
			$this->Cell(0,10,'Web: '. $oConfig->web .' // Email: '.$oConfig->email .' // Horario de Atención: L a V de 8 a 13hrs y Sábados de 9 a 13 hrs.',0,0,'C',true);
		}
	}	
	
	
	if ($oBillData) {
	

		if(!is_object($mpdf)){
				$mpdf=new PDF();
			}
			
			$mpdf->SetTopMargin(3);
			$mpdf->addPage();
			
				
			$mpdf->setXY(170,20);
			$mpdf->SetFont('Arial','B',11);
			$mpdf->Cell(20,4,str_pad($oBillData['id'] ,8, "0", STR_PAD_LEFT),0,1);
			$mpdf->setXY(170,26);
			$mpdf->SetFont('Arial','B',9);
			$mpdf->Cell(20,4,$oBillData['dni'],0,1);
			$mpdf->setXY(170,32);
			$mpdf->SetFont('Arial','B',9);
			$mpdf->Cell(20,4,$oBillData['birthdate'],0,1); 
			$mpdf->setXY(170,38);
			$mpdf->SetFont('Arial','',6);
			$mpdf->Cell(20,4,$oBillData['beginDate'],0,1);
			
			$mpdf->TextWithDirection(3,40,'Datos Cliente','U');
			//$mpdf->TextWithDirection(75,40,'Domicillio Postal','U');
			
			$mpdf->setXY(70,20);
			$mpdf->SetFont('Arial','',6);
			$mpdf->Cell(20,4,mb_strtoupper('Datos Extras'),0,1);
			$mpdf->setXY(80,26);
			$mpdf->SetFont('Arial','B',7);
			$mpdf->Cell(20,4,'CPA: '.mb_strtoupper($oBillData['cpa']),0,1);
			//$mpdf->setXY(80,29);
			//$mpdf->SetFont('Arial','B',7);
			//$mpdf->Cell(20,4,mb_strtoupper($factura['typeDocumentUser'].':'.$factura['documentUser'].' Cuit/Cuil: '.$factura['cuitUser']),0,1);
			$mpdf->setXY(80,29);
			$mpdf->SetFont('Arial','B',7);
			$mpdf->Cell(170,4,'CEL: '.mb_strtoupper($oBillData['cel']),0,1);
			$mpdf->setXY(80,32);
			$mpdf->SetFont('Arial','B',7);
			$mpdf->Cell(20,4,'Otro',0,1);
			$mpdf->setXY(80,35);
			//$mpdf->SetFont('Arial','B',7);
			//$mpdf->Cell(20,4,mb_strtoupper($factura['typeUserUser'].' - '.$factura['typeUser']),0,1);
			//$mpdf->setXY(80,38);

			
			$mpdf->setXY(5,21);
			//$mpdf->Cell(70,5,'Socio/Usuario',0,1);
			//$mpdf->SetLineWidth(1);
			//$mpdf->Rect(18,47,170,30);
			//$mpdf->SetLineWidth(0);
			//********************************************************
			//** Esto ponerlo en funcion porque se repite al pedo   **
			//** parece codigo de alfredito!!!                      **
			//** Lo mismo con todo lo que veas que se repita        **
			//********************************************************
			$mpdf->SetFont('Arial','B',8);
			//$mpdf->Cell(15);
			//$mpdf->Cell(30,8,'Apellido y Nombre: ',0,0);
			//$mpdf->SetFont('Arial','',9);
			$mpdf->Cell(170,4,mb_convert_encoding(mb_strtoupper($oBillData['last'].' , '.$oBillData['first']), 'UTF-8', 'ISO-8859-1'),0,1);
			$mpdf->setX(5);
			
			$mpdf->setX(5);
			$mpdf->Cell(170,4,mb_strtoupper($oBillData['domicilio']),0,1);
			$mpdf->setX(5);
			$mpdf->Cell(170,4,mb_strtoupper($oBillData['tel']),0,1);
			$mpdf->setX(5);
			$mpdf->Cell(170,4,mb_strtoupper($oBillData['email']),0,1);
			//////////////////////////////////////////////////////////////////////Datos Empleador Fin//////////////////
	
	
	
	
	$mpdf->SetFont('Arial','B',11);
			$mpdf->Ln(4);
			//$mpdf->Cell(8);
			$mpdf->SetFillColor(240);
			//$mpdf->SetFillColor(128);
			//$mpdf->Rect(18,83,170,5);
			

			$mpdf->setX(0);
			
			
			//$mpdf->Cell(210,5,'Vencimiento '.str_pad($factura['firstDate'] ,2, "0", STR_PAD_LEFT)."/".str_pad($factura['monthGenerated'],2, "0", STR_PAD_LEFT)."/".$factura['yearGenerated'],0,0,'R',true);
			//$mpdf->Cell(80,5,'Domicilio Consumo '.substr($factura['meterAddress'],0,20),0,1,'L',true);
			
			
			$mpdf->Cell(80,5,' ',0,0,'L',true);
			$mpdf->Cell(130,5,' ',0,1,'R',true);
			
			
			

			//Datos Medidor y Información/////////////////////////////////////////////////////////////////////////////
			$mpdf->Rect(4,51,200,8,'F');
			$mpdf->Ln(3);
			$mpdf->setXY(6,52);
			$mpdf->SetFont('Arial','',9);
			$mpdf->Cell(2);
			$mpdf->Cell(30,6,'DATOS GARANTE',0,1);
			/*$mpdf->Cell(50,6,'EMPRESA ',0,0);
			$mpdf->Cell(40,6,'DOMICILIO',0,0);
			$mpdf->Cell(40,6,'TEL',0,0);
			$mpdf->Cell(40,6,'SUELDO',0,1);*/
			$mpdf->setXY(6,56);
			$mpdf->SetFont('Arial','B',9);
			$mpdf->Cell(2);

			$posy=59;
			
		
			foreach($oBillData['charges'] as $item){
						
						$mpdf->setXY(6,$posy);
				        $mpdf->Cell(50,6,mb_convert_encoding(mb_strtoupper($item['last'].' , '.$item['first']), 'UTF-8', 'ISO-8859-1'),0,0,'L');
						$mpdf->Cell(50,6,mb_strtoupper($item['dni']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['email']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['sueldo']),0,1,'L');
						$posy=$posy + 7;	
				
				$mpdf->setXY(6,$posy);
				        $mpdf->Cell(60,6,'CONTACTO: '.mb_strtoupper($item['tel'].'/'.$item['cel']),0,0,'L');
						$mpdf->Cell(50,6,mb_strtoupper($item['domicilio']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['tel_cont']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['rel_cont']),0,0,'L');
						
				
				
			
			}
			
			

			/*$mpdf->SetFont('Arial','B',11);
			$mpdf->Ln(4);
			$mpdf->SetFillColor(240);
			$mpdf->setX(0);
			$mpdf->Cell(80,5,' ',0,0,'L',true);
			$mpdf->Cell(130,5,' ',0,1,'R',true);*/
	
	
	$posy=$posy +10;
	
			$mpdf->Rect(4,$posy,200,8,'F');
			$mpdf->Ln(3);
			$mpdf->setXY(6,$posy);
			$mpdf->SetFont('Arial','',9);
			$mpdf->Cell(2);
			$mpdf->Cell(30,6,'CARGO',0,0);
			$mpdf->Cell(50,6,'EMPRESA ',0,0);
			$mpdf->Cell(40,6,'DOMICILIO',0,0);
			$mpdf->Cell(40,6,'TEL',0,0);
			$mpdf->Cell(40,6,'SUELDO',0,1);
			$mpdf->setXY(6,56);
			$mpdf->SetFont('Arial','B',9);
			$mpdf->Cell(2);
			
			$posy=$posy +10;
		
			foreach($oBillData['busi'] as $item){
						
						$mpdf->setXY(6,$posy);
				        $mpdf->Cell(30,6,mb_strtoupper($item['cargo']),0,0,'L');
						$mpdf->Cell(50,6,mb_strtoupper($item['empresa']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['dom']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['tel_empresa']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['sueldo']),0,1,'L');
						$posy=$posy + 7;	
				
				$mpdf->setXY(6,$posy);
				        $mpdf->Cell(60,6,'CONTACTO: '.mb_strtoupper($item['first_cont'].','.$item['last_cont']),0,0,'L');
						$mpdf->Cell(50,6,mb_strtoupper($item['domicilio_cont']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['tel_cont']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['rel_cont']),0,0,'L');
						
				
				
			
			}
	
		$posy=$posy +10;
	
			$mpdf->Rect(4,$posy,200,8,'F');
			$mpdf->Ln(3);
			$mpdf->setXY(6,$posy);
			$mpdf->SetFont('Arial','',9);
			$mpdf->Cell(2);
			/*$mpdf->Cell(30,6,'CARGO',0,0);
			$mpdf->Cell(50,6,'EMPRESA ',0,0);
			$mpdf->Cell(40,6,'DOMICILIO',0,0);
			$mpdf->Cell(40,6,'TEL',0,0);*/
			$mpdf->Cell(40,6,'DATOS PROPIEDAD',0,1);
			$mpdf->setXY(6,56);
			$mpdf->SetFont('Arial','B',9);
			$mpdf->Cell(2);
			
			$posy=$posy +10;
		
			foreach($oBillData['pro'] as $item){
						
						$mpdf->setXY(6,$posy);
				        $mpdf->Cell(50,6,mb_strtoupper($item['propietario']),0,0,'L');
						$mpdf->Cell(50,6,mb_strtoupper($item['ubica']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['matricula']),0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['partida']),0,1,'L');
						
						$posy=$posy + 7;	
				
				$mpdf->setXY(6,$posy);
				        $mpdf->Cell(60,6,'CIR / MANZANA / SECCION: '.mb_strtoupper($item['circun'].' / '.$item['manzana'].' / '.$item['seccion']),0,0,'L');
						$mpdf->Cell(50,6,'',0,0,'L');
						$mpdf->Cell(40,6,mb_strtoupper($item['parcela'].'/'.$item['subparcela']),0,0,'L');
						
						
				
				
			
			}
	
	
	
	


		$mpdf->Output();exit;
} else {
	var_dump("Error");
}

} else {
	var_dump("Error");
}



}else{
	header('location: login.php');
}

?>