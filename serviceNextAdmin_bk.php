<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser']))) {
		
		$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);		
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		
		
		if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
			
			$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
		
	}
		
		$tpl->load_file("pg/admin/serviceNext.html", "bodyContent");
		$tpl->load_file("pg/admin/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		//$tpl->set_var("sSysUserName","Administrador");
		$sAction 			= $_POST['sAction'];
		
		
	
		
		switch ($sAction){
			case "editServiceNext":
			  
					$oData = new stdClass();
		
					$oData->status = (int)($_POST['editStatus']);
					$oData->id	 		= (int)($_POST['iEditID']);
					$oData->servicio	 		= (int)($_POST['editServicio']);
					$oData->seguimiento	 		= $_POST['editSeguimiento'];
					$oData->comentario	 		= $_POST['editComentarios'];
					$oData->orden	 		= $_POST['editOrden'];
					$oData->endDate	 		= $_POST['editFechaPromesa'];
					$oData->fechaSeguimiento	 		= $_POST['editFechaServicio'];
					
					$resultEdit = $oAdv->editServiceNext($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado el service.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "searchServiceById":
	
		$oData = $oAdv->getServiceById($_POST['iID']);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	
	case "newService":
	
	  
		$oData = new stdClass();
					$oData->name	= $_POST['newNombre'];
					$oData->lastName	= $_POST['newApellido'];
					$oData->dni	= $_POST['newDNI'];
					$oData->email = htmlentities($_POST['newEmail']);
					$oData->phone = htmlentities($_POST['newTelefono']);
					$oData->descrip = ($_POST['newAcc']);
					$oData->problem = ($_POST['newProblema']);
					$oData->obser = htmlentities($_POST['newObservaciones']);
					$oData->status = 1;
					$oData->credito = htmlentities($_POST['newCredito']);
					$oData->marca = (int)($_POST['newMarca']);
					$oData->modelo = htmlentities($_POST['newModelo']);
					$oData->serie = htmlentities($_POST['newSerie']);
					$oData->fecha = htmlentities($_POST['newFecha']);
					$oData->recibe = htmlentities($_POST['newRecibe']);
					$oData->articulo = htmlentities($_POST['newArticulo']);
					$oData->bill = htmlentities($_POST['newFactura']);
					

					$result = $oAdv->newService($oData);
					
					
					if($result->status != "OK"){
			$result->status = $db->getLabel("lbl_".$oData->status,"SPA");
		}
		
		echo json_encode($result);
		exit;
				
	break;
	
	
	
	
	
	
	
	
	/*case "newService":

					$oData = new stdClass();
					$oData->name	= $_POST['newNombre'];
					$oData->lastName	= $_POST['newApellido'];
					$oData->dni	= $_POST['newDNI'];
					$oData->email = htmlentities($_POST['newEmail']);
					$oData->phone = htmlentities($_POST['newTelefono']);
					$oData->descrip = ($_POST['newAcc']);
					$oData->problem = ($_POST['newProblema']);
					$oData->obser = htmlentities($_POST['newObservaciones']);
					$oData->status = 1;
					$oData->credito = htmlentities($_POST['newCredito']);
					$oData->marca = (int)($_POST['newMarca']);
					$oData->modelo = htmlentities($_POST['newModelo']);
					$oData->serie = htmlentities($_POST['newSerie']);
					$oData->fecha = htmlentities($_POST['newFecha']);
					$oData->recibe = htmlentities($_POST['newRecibe']);
					$oData->articulo = htmlentities($_POST['newArticulo']);
					$oData->bill = htmlentities($_POST['newFactura']);
					

					$result = $oAdv->newService($oData);
	

					if ($result->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado un Nuevo Servicio.");
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}
					 
					
	break;*/
	case "deleteService":
		
			$oData = $oAdv->deleteServiceById($_POST['iID']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	default:
	break;
}
		
if(count($oAdv->getServiceNews())>0){
		foreach ($oAdv->getServiceNews() as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sNumero",$Item['numero']);
				$tpl->set_var("sApellido",mb_convert_encoding($Item['lastName'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sNombre",mb_convert_encoding($Item['name'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sDNI",mb_convert_encoding($Item['dni'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sCredito",mb_convert_encoding($Item['credito'],'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFactura",mb_convert_encoding($Item['bill'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaCompra",mb_convert_encoding($Item['fecha'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sArticulo",mb_convert_encoding($Item['modelo'],'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFecha",mb_convert_encoding($Item['beginDate'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaB",mb_convert_encoding($Item['beginDate'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sMarcaB",mb_convert_encoding($Item['marca'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sSerie",mb_convert_encoding($Item['serie'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sTelefono",mb_convert_encoding($Item['phone'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sEstado",mb_convert_encoding($Item['status'],'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ResultsBlock",true);
			}
	} 
	
	
	if(count($oAdv->getServiceRepa())>0){
		foreach ($oAdv->getServiceRepa() as $Item){
				$tpl->set_var("iIdR",$Item['id']);
				$tpl->set_var("sNumeroR",$Item['numero']);
				$tpl->set_var("sApellidoR",mb_convert_encoding($Item['lastName'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sNombreR",mb_convert_encoding($Item['name'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sDNIR",mb_convert_encoding($Item['dni'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sCreditoR",mb_convert_encoding($Item['credito'],'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFacturaR",mb_convert_encoding($Item['bill'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaCompraR",mb_convert_encoding($Item['fecha'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sArticuloR",mb_convert_encoding($Item['modelo'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sServicioR",mb_convert_encoding($Item['servicio'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sPhoneR",mb_convert_encoding($Item['phone'],'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFechaR",mb_convert_encoding($Item['beginDate'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaSeguimientoR",mb_convert_encoding($Item['fechaSeguimiento'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaBR",mb_convert_encoding($Item['beginDate'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sMarcaR",mb_convert_encoding($Item['marca'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sSerieR",mb_convert_encoding($Item['serie'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sTelefonoR",mb_convert_encoding($Item['phone'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sEstadoR",mb_convert_encoding($Item['status'],'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ResultsBlockReparacion",true);
			}
	} 
	
	
		if(count($oAdv->getServiceReparados())>0){
		foreach ($oAdv->getServiceReparados() as $Item){
				$tpl->set_var("iIdRe",$Item['id']);
				$tpl->set_var("sNumeroRe",$Item['numero']);
				$tpl->set_var("sApellidoRe",mb_convert_encoding($Item['lastName'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sNombreRe",mb_convert_encoding($Item['name'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sDNIRe",mb_convert_encoding($Item['dni'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sCreditoRe",mb_convert_encoding($Item['credito'],'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFacturaRe",mb_convert_encoding($Item['bill'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaCompraRe",mb_convert_encoding($Item['fecha'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sArticuloRe",mb_convert_encoding($Item['modelo'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sServicioRe",mb_convert_encoding($Item['servicio'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sPhoneRe",mb_convert_encoding($Item['phone'],'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFechaRe",mb_convert_encoding($Item['beginDate'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaSeguimientoRe",mb_convert_encoding($Item['fechaSeguimiento'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaBRe",mb_convert_encoding($Item['beginDate'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sMarcaRe",mb_convert_encoding($Item['marca'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sSerieRe",mb_convert_encoding($Item['serie'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sTelefonoRe",mb_convert_encoding($Item['phone'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sEstadoRe",mb_convert_encoding($Item['status'],'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ResultsBlockReparacione",true);
			}
	} 
	
		if(count($oAdv->getServiceFina())>0){
		foreach ($oAdv->getServiceFina() as $Item){
				$tpl->set_var("iIdRe",$Item['id']);
				$tpl->set_var("sNumeroRe",$Item['numero']);
				$tpl->set_var("sApellidoRe",mb_convert_encoding($Item['lastName'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sNombreRe",mb_convert_encoding($Item['name'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sDNIRe",mb_convert_encoding($Item['dni'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sCreditoRe",mb_convert_encoding($Item['credito'],'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFacturaRe",mb_convert_encoding($Item['bill'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaCompraRe",mb_convert_encoding($Item['fecha'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sArticuloRe",mb_convert_encoding($Item['modelo'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sServicioRe",mb_convert_encoding($Item['servicio'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sPhoneRe",mb_convert_encoding($Item['phone'],'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFechaRe",mb_convert_encoding($Item['beginDate'],'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaSeguimientoRe",mb_convert_encoding($Item['fechaSeguimiento'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaBRe",mb_convert_encoding($Item['beginDate'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sMarcaRe",mb_convert_encoding($Item['marca'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sSerieRe",mb_convert_encoding($Item['serie'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sTelefonoRe",mb_convert_encoding($Item['phone'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sEstadoRe",mb_convert_encoding($Item['status'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ResultsBlockFina",true);
			}
	} 
	
	
	if(count($oAdv->getServiceDevo())>0){
		foreach ($oAdv->getServiceDevo() as $Item){
				$tpl->set_var("iIdDevo",$Item['id']);
				$tpl->set_var("sNumeroDevo",$Item['numero']);
				$tpl->set_var("sApellidoDevo",mb_convert_encoding($Item['lastName'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sNombreDevo",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sDNIDevo",mb_convert_encoding($Item['dni'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sCreditoDevo",mb_convert_encoding($Item['credito'], 'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFacturaDevo",mb_convert_encoding($Item['bill'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaCompraDevo",mb_convert_encoding($Item['fecha'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sArticuloDevo",mb_convert_encoding($Item['articulo'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sModeloDevo",mb_convert_encoding($Item['modelo'], 'UTF-8', 'ISO-8859-1'));
				
				$tpl->set_var("sFechaDevo",mb_convert_encoding($Item['beginDate'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sFechaBDevo",mb_convert_encoding($Item['fechaSeguimiento'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sMarcaBDevo",mb_convert_encoding($Item['marca'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sSerieDevo",mb_convert_encoding($Item['serie'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sServicioDevo",mb_convert_encoding($Item['servicio'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sTelefonoDevo",mb_convert_encoding($Item['phone'], 'UTF-8', 'ISO-8859-1'));
				$tpl->set_var("sEstadoDevo",mb_convert_encoding($Item['status'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ResultsBlockDevo",true);
			}
	} 

	
if(count($oAdv->getMarcas())>0){
		foreach ($oAdv->getMarcas() as $Item){
				$tpl->set_var("iMarca",$Item['id']);
				$tpl->set_var("sMarca",mb_convert_encoding($Item['name']), 'UTF-8', 'ISO-8859-1');
				$tpl->parse("MarcasBlock",true);
			}
	} 
	
	if(count($oAdv->getStatus())>0){
		foreach ($oAdv->getStatus() as $Item){
				$tpl->set_var("iEstado",$Item['id']);
				$tpl->set_var("sEstado",mb_convert_encoding($Item['name']), 'UTF-8', 'ISO-8859-1');
				$tpl->parse("EstadoBlock",true);
			}
	} 
	
	
	if(count($oAdv->getServicesOnIn())>0){
		foreach ($oAdv->getServicesOnIn() as $Item){
				$tpl->set_var("iServicio",$Item['id']);
				$tpl->set_var("sServicio",mb_convert_encoding($Item['name'].' - '.$Item['address'].' / '.$Item['phone'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ServiciosOnBlock",true);
			}
	} 
			
			
			
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>