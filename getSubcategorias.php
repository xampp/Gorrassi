<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017")){
	if (   ($user->can(IS_ADMIN,$_SESSION['sysUser'])) ||( $user->can(IS_USER,$_SESSION['sysUser'])) ||  ($user->can(IS_EDIT,$_SESSION['sysUser']))            ) {
		
		$q = intval($_GET['q']);
		$sub = intval($_GET['sub']);
		//var_dump($sub);
		if (empty($sub)){
			$sub=0;
		}
		
			if(count($oAdv->getSubCategorias($q))>0){
				
				$sHtml = "<option value=''>Seleccione una subcategoria</option>";
				
		foreach ($oAdv->getSubCategorias($q) as $Item){
			
			//var_dump($Item['id']);
			//var_dump($sub);
			
			
			if($Item['id'] != $sub){
						$sHtml .= "<option value='".$Item['id']."'>".($Item['description'])."</option>";
						
					}else{
						$sHtml .= "<option selected='selected' value='".$Item['id']."'>".($Item['description'])."</option>";
						
					}
			
			/*$sHtml .= "<option value='".$Item['id']."'>".utf8_encode($Item['description'])."</option>"; */
			
			}
	} 	 else {
		$sHtml = "<option value=''>Sin opciones</option>";
	}
	echo $sHtml;	
		
		
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}

?>
