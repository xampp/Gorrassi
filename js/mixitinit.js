$(function() {

  $("#container").mixItUp({
      load: {
   // sort: 'order:asc',
    page: 1
  },
    animation: {
             animateChangeLayout: false,
        animateResizeContainer: true,
        animateResizeTargets: false,
      effects: 'fade stagger(100ms)',
        staggerSequence: function(i){
            return i % 3;
    }

  },

   layout: {
        display: 'inline-block',
        containerClass: 'list',
        containerClassFail: 'fail'
    },
     callbacks: {
        onMixLoad: function(){
            $(this).mixItUp('setOptions', {
                animation: {
                    enable: false    
                },
            });
        }
    },

   pagination: {
    limit: 6,
    generatePagers: true,
    maxPagers: 5,
    pagerClass: 'btn',
    prevButtonHTML: '«',
    nextButtonHTML: '»'
  }
});

   var inputText;
  var $matching = $();

  // Delay function
  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();

  $("#input").keyup(function(){
    // Delay function invoked to make sure user stopped typing
    delay(function(){
      inputText = $("#input").val().toLowerCase();
      
      // Check to see if input field is empty
      if ((inputText.length) > 0) {            
        $( '.mix').each(function() {
          $this = $("this");
          
           // add item to be filtered out if input text matches items inside the title  , #hora, #fecha 
           if($(this).children('#title').text().toLowerCase().match(inputText)) {
            $matching = $matching.add(this);
          }
          else {
            // removes any previously matched item
            $matching = $matching.not(this);
          }
        });
        $("#container").mixItUp('filter', $matching);
      }

      else {
        // resets the filter to show all item if input is empty
        $("#container").mixItUp('filter', 'all');
      }
    }, 200 );
  });
})