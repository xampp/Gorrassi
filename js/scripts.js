/*------MENU ADAPTIVO-----------*/
$(document).ready(main);
var contador=1;
function main(){
$('.bt-menu').click(function(){
//$('nav').toggle();
if (contador ==1){
$('nav').animate({
left:'0'
});
contador=0;

  } else {
contador=1;
$('nav').animate({
      left:'-100%'  
    });
  }
})};
/*---------------------acordeon--------------------*/
 $('dl dd').hide();
       $('dl dt').click(function(){
          if ($(this).hasClass('activo')) {
               $(this).removeClass('activo');
               $(this).next().slideUp();
          } else {
               $('dl dt').removeClass('activo');
               $(this).addClass('activo');
               $('dl dd').slideUp();
               $(this).next().slideDown();
          }
       });
/*---------------------slider noticias--------------------*/

try {var W = $('#event_rotator').width(),
    N = $('#slider .event').length,
    C = 0,
    intv;

if(N<=1)$('#left, #right').hide(); 
$('#slider').width( W*N );

$('#left, #right').click(function(){
     C = (this.id=='right'? ++C : --C) < 0 ? N-1 : C%N ;
     $('#slider').stop().animate({left: -C*W }, 80 );
});
/*auto slide*/
function auto(){
  intv = setInterval(function(){
      $('#right').click();
  }, 3000 );
}
auto();
/*autoslide*/
$('#event_rotator').hover(function( e ){
  return e.type=='mouseenter' ? clearInterval(intv) : auto();
});
} catch (error) { throw error; }

/*------------------------swipebox------------------------*/
;( function( $ ) {

  $( '.swipebox' ).swipebox( {
    useCSS : true, // false will force the use of jQuery for animations
    useSVG : true, // false to force the use of png for buttons
    initialIndexOnArray : 0, // which image index to init when a array is passed
    hideCloseButtonOnMobile : false, // true will hide the close button on mobile devices
    removeBarsOnMobile : true, // false will show top bar on mobile devices
    hideBarsDelay : 3000, // delay before hiding bars on desktop
    videoMaxWidth : 1140, // videos max width
    beforeOpen: function() {}, // called before opening
    afterOpen: null, // called after opening
    afterClose: function() {}, // called after closing
    loopAtEnd: false // true will return to the first image after the last image is reached
  } );

} )( jQuery );


/*------------------------parallax------------------------*/
      
      $(document).ready(function(){
          $('section[data-type="parallax_section"]').each(function(){
              var $bgobj = $(this); // Variable para asignacion de objeto
              $(window).scroll(function() {
                $window = $(window);
                  var yPos = -($window.scrollTop() / $bgobj.data('speed'));
                  // cordinadas del background
                  var coords = '50% '+ yPos + 'px';
                  // moviendo el background
                  $bgobj.css({ backgroundPosition: coords });
              });
          });
      });

/*------------------------mixitUP------------------------*/

/*------------------------caja abrecierra------------------------*/
$( "#open-busca" ).click(function() {
  $( "#caja-busca" ).toggle();
});

  var swiper = new Swiper('.swiper-container', {
        slidesPerView: 'auto',
              pagination: {
        el: '.swiper-pagination',
      },
        navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
        centeredSlides: true,
        autoplay: {
      delay: 3000,
      },
        autoplayDisableOnInteraction: false,
        paginationClickable: true
    });

      var swiper = new Swiper('.swiper-container-productos', {
      slidesPerView:3,
      spaceBetween: 20,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

      $(document).ready(function(){
  $(".Modal").hide();
    $(".abre_modal").click(function(){
      $(".Modal").fadeToggle();
    });
    $(".Close").click(function(){
      $(".Modal").fadeOut();
    });
});
// Esc Key, hide menu.
$(document).keydown(function(e) {
if(e.keyCode == 27) {
    $(".Modal").hide(300);
}
});
