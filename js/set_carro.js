
simpleCart({ 
cartColumns: [

  { view: function(item, column){
				   
		var formatter = new Intl.NumberFormat('en-US', {
  		style: 'currency',
  		currency: 'USD',
		});
		   
    return "<div class='item-image'><img src='"+item.get('image')+"'/></div>"
			+"<div class='info-carrito'>"
			+"<div class='item-name'>"+item.get('name')+"</div>"
				+"<div class='item-news' style='font-size: 12px;'><br>"+item.get('marca')+"/ "+item.get('modelo')+"</div>"
			+"<div class='item-decrement'><a href='javascript:;' class='simpleCart_decrement fa fa-minus-square'></a></div>"
			
			+"<div class='item-quantity'>"+item.get('quantity')+"</div>"
			+"<div class='item-increment'><a href='javascript:;' class='simpleCart_increment fa fa-plus-square'></a></div>"
			+"<div class='item-price'>"+formatter.format(item.get('price'))+"</div></div>"
			+"<div class='subtotal'>"
			
			+"</div></div></div>";

	/*"<div class='item-image'><img src='"+item.get('image')+"'/></div>"
	//+"<div class='info-carrito'>"
	+"<div class='wrap_cont'>"
	+"<div class='item-name'>"+item.get('name')+"</div>"
	//+"<div class='item-decrement'><a href='javascript:;' class='simpleCart_decrement fa fa-minus-square'></a></div>"
	+"<div class='item-quantity'>"+item.get('quantity')+"</div>"
	//+"<div class='item-decrement'><a href='javascript:;' class='simpleCart_decrement fa fa-minus-square'></a></div>"
	+"<div class='wrap_cont3'>"+"<div class='item-price'>$ "+item.get('price')+"</div>"
	+"</div></div></div>";
	*/

		}, attr: 'wrapper' 
	},
  { view: "remove" , text: "&times;" , label: true }                       
],
  checkout: { 
        type: "SendForm" , 
        url: "https://pascualgorrassi.com.ar/checkout.php" ,
        //url: "checkout.php" ,
        method: "POST" 
    }

});

simpleCart.bind( "afterAdd" , function( item ){
  toastr.success(item.get("quantity") + " " + item.get("name") + " se añadió a tu carrito!")
});

simpleCart.bind( 'beforeRemove' , function( item ){
 toastr.warning(item.get('name') + " fue borrado de tu carrito")
});
