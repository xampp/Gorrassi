<?php

include_once 'config.php';

ob_start();
  
if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_USERWEB,$_SESSION['sysUser'])) ) {
				$oDataUser=$user->getName();
	  /* 
				$tpl->set_var("sSysUserName",'<div class="iniciar_sesion" > Hola, '.$oDataUser['name'].'  '.$oDataUser['lastName'].'</div>'); 
		*/
	}

} else {
		header("location:".URL_BASE);
} 
$content = $_POST;
//$content = $_POST;
//var_dump($content);
//($content);
if ($content && $content['itemCount']>0){
	$oData = new stdClass();
	$oData->name	= $oDataUser['name'];
	$oData->lastName	= $oDataUser['lastName'];
	//$oData->dni	= $grandTotal;
	$oData->email = $oDataUser['userName'];
	$oData->phone = $oDataUser['phone'];
	$oData->cellPhone = $oDataUser['phone'];
	$sCode=isset($content['postcode'])?' - '.$content['postcode']:'';	
	$oData->descrip = $oDataUser['address'].$sCode;
	$oData->obser = $oDataUser['address'].' - '.'<br><br>'.$content['message'];
	$oData->status = 1;	
	$oData->recibe = 'PAGINA WEB';
	$result = $oAdv->newPedido($oData);
  $body='';
	$body .= '<strong>PEDIDO ONLINE</strong>'."<br><br>";
	$body .= '=================================='."<br>";
	$body .= "Nombre: ".$oDataUser['name']."<br>";
	$body .= "Apellido/s: ".$oDataUser['lastName']."<br>";
	$body .= "Email: ".$oDataUser['userName']."<br>";
	$body .= "Telefono: ".$oDataUser['phone']."<br>";
	$body .= "Domicilio: ".$oDataUser['address']."<br>";
	//$body .= "CPA: ".$content['postcode']."<br>";
	$body .= "<br>";
	$body .= 'El cliente ha pedido:'."<br>";
	$body .= '=================================='."<br>";
  
	for($i=1; $i < $content['itemCount'] + 1; $i++) {
		$name = 'item_name_'.$i;
		$quantity = 'item_quantity_'.$i;
		$price = 'item_price_'.$i;
		$todo = 'item_options_'.$i;

		$parte = $content[$todo];
		$pieces = explode(",", $parte);

		$vemosPrimero = $pieces[0];
		$parteUno = explode(":", $vemosPrimero);

		if ($parteUno[0]=="brand"){
		    $color=$pieces[3];
		    $marca= $pieces[2];
		}else{
		    $color=$pieces[2];
		    $marca= $pieces[1];
		}

		//$color=$pieces[2];
		//$marca= $pieces[1];

		//var_dump($todo);

		//var_dump($color);

		//var_dump($marca);exit;

		$total = $content[$quantity]*$content[$price];
		$grandTotal=0;
		$grandTotal += $total;
		$body .= 'Orden de Pedido #'.$i.': <br><strong>'.$content[$name].'/'.$marca.'/'.$color."</strong><br>".'Cant. x '.$content[$quantity].' --- Precio por unidad $'.number_format($content[$price], 2, '.', '')."<br>".'Subtotal $'.number_format($total, 2, '.', '')."<br>"; 
		$body .= '=================================='."<br>";

		$oDataM = new stdClass();
		$oDataM->cod	= $content[$name];
		$oDataM->idOrden	= $result->lastId;
		$oDataM->size	= $marca;
		$oDataM->color	= $color;
		$oDataM->quantity = $content[$quantity];
		$oDataM->precioUnidad = $content[$price];
		$oDataM->total = $content[$quantity]*$content[$price];
		$resultM = $oAdv->newPedidoMovements($oDataM);
	}

	$resultActualizo = $oAdv->updatePedido($result->lastId,$grandTotal);

	$body .= '<strong>Total del Pedido: $'.number_format($grandTotal, 2, '.', '')."</strong><br>";
	$body .= '=================================='."<br>";
	$body .= "<br>";
	$body .= "Comentario extra: ".$content['message']."<br><br><br>";
	$body .= "UN OPERADOR DE NUESTRA EMPRESA SE CONTACTARA CON USTED A LA BREVEDAD.<br><br>";

	$body .= "Muchas Gracias por su pedido.<br>";
	$body .= "A la brevedad nos comunicaremos.<br><br>";

	$body .= "<strong>RECUERDE QUE ESTA OPERACION SE ENCUENTRA EN NUESTROS <a target='_blank' href='https://pascualgorrassi.com.ar/terms.php'>TERMINOS Y CONDICIONES</a></strong><br><br>";
	$newEmail=$utils->sendEmail($oDataUser['userName'], 'Pedido Online Pascual Gorrassi', $body);
	//var_dump($newEmail);exit;
	$newEmail=$utils->sendEmail('ventas@pascualgorrassi.com.ar', 'Han realizado un Pedido Online Pascual Gorrassi ',$body);

	// mail($to, $subject, $body, $headers);
	Header('Location: exito.php');
	die();
	ob_end_flush();

} else {
	 Header('Location: checkout.php');
  die();
  ob_end_flush();
}

  ?>