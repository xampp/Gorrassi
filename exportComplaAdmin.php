	<?php
	/*
	hacer todos los includes
	y redireccionar a login
	*/
	include_once 'config.php';

	if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017")){
		if ($user->can(IS_ADMIN,$_SESSION['sysUser'])) {
				
		  require_once 'includes/excell/PHPExcel.php';

			$objPHPExcel = new PHPExcel();
			//Informacion del excel
			   $objPHPExcel->
			    getProperties()
			        ->setCreator("innovar-groupmdq.com.ar")
			        ->setLastModifiedBy("innovar-groupmdq.com.ar")
			        ->setTitle("Reclamos")
			        ->setSubject("Reclamos")
			        ->setDescription("innovar-groupmdq.com.ar")
			        ->setKeywords("innovar-groupmdq.com.ar")
			        ->setCategory("Reclamos");  

			$oUserData = $oAdv->getService(1);
			//var_dump($oMeterData);exit;
			if ($oUserData) {
			$objPHPExcel->setActiveSheetIndex(0);

	    // Rename sheet
				$objPHPExcel->getActiveSheet()->setTitle('RECLAMOS');
			   $objPHPExcel->getActiveSheet()->SetCellValue('A1','N° RECLAMO');
				 $objPHPExcel->getActiveSheet()->SetCellValue('B1','APELLIDO');
				 $objPHPExcel->getActiveSheet()->SetCellValue('C1','NOMBRE');
				 $objPHPExcel->getActiveSheet()->SetCellValue('D1','FECHA INGRESO');
				 $objPHPExcel->getActiveSheet()->SetCellValue('E1','MARCA');
				 $objPHPExcel->getActiveSheet()->SetCellValue('F1','ARTICULO');
				 $objPHPExcel->getActiveSheet()->SetCellValue('G1','TELEFONO');
				 $objPHPExcel->getActiveSheet()->SetCellValue('H1','CELULAR');
				 
				 $objPHPExcel->getActiveSheet()->SetCellValue('I1','DEFECTO');
				 $objPHPExcel->getActiveSheet()->SetCellValue('J1','OBSERVACIONES');
				 $objPHPExcel->getActiveSheet()->SetCellValue('K1','ESTADO');
				/* $objPHPExcel->getActiveSheet()->SetCellValue('L1','ENVIO');
				 $objPHPExcel->getActiveSheet()->SetCellValue('M1','VISIBLE');*/
				 
				 //$objPHPExcel->getActiveSheet()->SetCellValue('H1','TARIFA');
				// $objPHPExcel->getActiveSheet()->SetCellValue('I1','CARGO FIJO');
				// $objPHPExcel->getActiveSheet()->SetCellValue('J1','RUTA');
				// $objPHPExcel->getActiveSheet()->SetCellValue('K1','ORDEN');
				 
				// $objPHPExcel->getActiveSheet()->SetCellValue('L1','FECHA ALTA');
				// $objPHPExcel->getActiveSheet()->SetCellValue('M1','LECTURA INICIAL');
				// $objPHPExcel->getActiveSheet()->SetCellValue('N1','TARIFA');
				// $objPHPExcel->getActiveSheet()->SetCellValue('O1','EMAIL');
				// $objPHPExcel->getActiveSheet()->SetCellValue('P1','CBU');
				  //$objPHPExcel->getActiveSheet()->SetCellValue('Q1','FECHA ALTA');
				  
				    //$objPHPExcel->getActiveSheet()->SetCellValue('R1','NOMBRE CONDOMINO');
					//$objPHPExcel->getActiveSheet()->SetCellValue('S1','APELLIDO CONDOMINO');
					//$objPHPExcel->getActiveSheet()->SetCellValue('T1','TELEFONO CONDOMINO');
					//$objPHPExcel->getActiveSheet()->SetCellValue('U1','DOCUMENTO CONDOMINO');
					//$objPHPExcel->getActiveSheet()->SetCellValue('V1','TIPO CONDOMINO');
					//$objPHPExcel->getActiveSheet()->SetCellValue('W1','DOCUMENTACION CONDOMINO');

	      $i=2; 		 
				foreach ($oUserData as $capitalization){
					if ($capitalization['estado']==1){
						$estado="ACTIVO";					
					} else {					
						$estado="FINALIZADO";
					}
								
					 $objPHPExcel->getActiveSheet()->SetCellValue('A'.$i,$capitalization['id']);
					 $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i,mb_convert_encoding($capitalization['lastName'], 'UTF-8', 'ISO-8859-1'));
					 $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i,mb_convert_encoding($capitalization['name']), 'UTF-8', 'ISO-8859-1');
					 
					 $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i,mb_convert_encoding($capitalization['beginDate'], 'UTF-8', 'ISO-8859-1');
					 $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i,mb_convert_encoding($capitalization['marca'], 'UTF-8', 'ISO-8859-1'));
					 
					 $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i,mb_convert_encoding($capitalization['articulo'], 'UTF-8', 'ISO-8859-1'));
					 $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i,mb_convert_encoding($capitalization['phone'], 'UTF-8', 'ISO-8859-1'));			 			 
					 $objPHPExcel->getActiveSheet()->SetCellValue('H'.$i,$capitalization['cellPhone']);			 
					 $objPHPExcel->getActiveSheet()->SetCellValue('I'.$i,$capitalization['problem']);
					 $objPHPExcel->getActiveSheet()->SetCellValue('J'.$i,$capitalization['obser']);
					 $objPHPExcel->getActiveSheet()->SetCellValue('K'.$i,$estado);					
					/* $objPHPExcel->getActiveSheet()->SetCellValue('L'.$i,$capitalization['envio']);
					 $objPHPExcel->getActiveSheet()->SetCellValue('M'.$i,$capitalization['visible']);*/			 			 
					 // $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i,utf8_encode($capitalization['brand']));
					// $objPHPExcel->getActiveSheet()->SetCellValue('N'.$i,$capitalization->tarifa);
					// $objPHPExcel->getActiveSheet()->SetCellValue('O'.$i,$capitalization->email);
					//$objPHPExcel->getActiveSheet()->SetCellValue('P'.$i,$capitalization->cbu);			
					 // $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$i,$capitalization->userDischargeDate);			 
					// $objPHPExcel->getActiveSheet()->SetCellValue('R'.$i,$capitalization->userCName);
					 //$objPHPExcel->getActiveSheet()->SetCellValue('S'.$i,$capitalization->userCLastName);
					 //$objPHPExcel->getActiveSheet()->SetCellValue('T'.$i,$capitalization->userCphone);			 
					 //$objPHPExcel->getActiveSheet()->SetCellValue('U'.$i,$capitalization->userCDocument);
					// $objPHPExcel->getActiveSheet()->SetCellValue('V'.$i,$capitalization->userCType);
					 //$objPHPExcel->getActiveSheet()->SetCellValue('W'.$i,$capitalization->userCDocumentation);
					 $i++;
			  }
		  }
			
			// Redirect output to a client?s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel; charset=utf-8');
			header('Content-Disposition: attachment;filename="ExportarReclamos.xls"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}else{
			header('location: login.php');
		}
	}else{
		header('location: login.php');	
	}

	$tpl->pparse("main");

?>