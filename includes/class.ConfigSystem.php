<?php
class configSystem{

	private $cnx;
	
	public function __construct($cn){
		$this->cnx=$cn;
	}
	
	public function getConfigSystemNoUsada(){
	
		$sSQL ="
			SELECT 
				`name`,
				cuit,
				address,
				province,
				city,
				zip,
				location,
				phone,
				email,
				web,
				logo,
				pointSaleWater,
				pointSaleRent,
				pointSalePark,
				pointSaleConection,
				firstday,
				secondday,
				interests,
                public_opening,
				m3,
				m31,
				m32,
				m33,
				numberWaterA,
				numberWaterB,
				numberNoteWaterA,
				numberNoteWaterB,
				numberDebitWaterA,
				numberDebitWaterB,
				numberConectionA,
				numberConectionB,
				numberNoteConectionA,
				numberNoteConectionB,
				numberDebitConectionA,
				numberDebitConectionB,
				numberParkA,
				numberParkB,
				numberNoteParkA,
				numberNoteParkB,
				numberDebitParkA,
				numberDebitParkB,
				numberRentA,
				numberRentB,
				numberNoteRentA,
				numberNoteRentB,
				numberDebitRentA,
				numberDebitRentB,
				invoice,
				interests,
				dolar,
				seat

			FROM parameters ";
		try{
		     
			$res=mysqli_query($this->cnx,$sSQL);
			
			if($res){
				$row = mysqli_fetch_assoc($res);
				if ($row){
				     
					$oData = new stdClass();
					foreach($row as $field => $value){
						$oData->$field = $value;
					}
					return $oData;
				}else{
					throw new Exception("ERR0004");
				}
			}else{
				throw new Exception("ERR0005");
			}
		}catch (Exception $e){
			return $e->getMessage();
		}
	}
	
		public function getConfigSystem(){
	
		$sSQL ="
			SELECT 
				`name`,
				address,
				phone,
				email,
				web,
				logo
				

			FROM parameters ";
		try{
		     
			$res=mysqli_query($this->cnx,$sSQL);
			
			if($res){
				$row = mysqli_fetch_assoc($res);
				if ($row){
				     
					$oData = new stdClass();
					foreach($row as $field => $value){
						$oData->$field = $value;
					}
					return $oData;
				}else{
					throw new Exception("ERR0004");
				}
			}else{
				throw new Exception("ERR0005");
			}
		}catch (Exception $e){
			return $e->getMessage();
		}
	}
	

public function editConfigSytem($vData){
try{
         
		if($vData){

		     
			$vDataToUpdate = array(
			'`name`'=>$vData["configSystemName"],
			'cuit'=>$vData["configSystemCuit"],
			'`phone`'=>$vData["configSystemPhone"],
			'email'=>$vData["configSystemEmail"],
			'`web`'=>$vData["configSystemWeb"],
			'pointSaleWater'=>(int)$vData["configSystemPointSaleWater"],
			'pointSaleRent'=>(int)$vData["configSystemPointSaleRent"],
			'pointSalePark'=>(int)$vData["configSystemPointSalePark"],
			'pointSaleConection'=>(int)$vData["configSystemPointSaleConection"],
			'address'=>$vData["configSystemAddress"],
			'`location`'=>$vData["configSystemLocation"],
			'numberWaterA'=>(int)$vData["configSystemNumberWaterA"],
			'firstday'=>(int)$vData["configSystemFirstDay"],
			'secondday'=>(int)$vData["configSystemSecondDay"],
			'interests'=>$vData["configSystemInterests"],
			'`zip`'=>(int)$vData["configSystemZip"],
             '`city`'=>(int)$vData["configSystemCity"],
             '`province`'=>(int)$vData["configSystemProvince"],
             '`public_opening`'=>$vData["configSystemPublic"],
			 '`m3`'=>$vData["configSystemM3"],
			 '`m31`'=>$vData["configSystemM31"],
			 '`m32`'=>$vData["configSystemM32"],
			 '`m33`'=>$vData["configSystemM33"],
			 'numberWaterB'=>(int)$vData["configSystemNumberWaterB"],
			'numberNoteWaterA'=>(int)$vData["configSystemNumberNoteWaterA"],
			'numberNoteWaterB'=>(int)$vData["configSystemNumberNoteWaterB"],
			'numberDebitWaterA'=>(int)$vData["configSystemNumberDebitWaterA"],
			'numberDebitWaterB'=>(int)$vData["configSystemNumberDebitWaterB"],
				'numberConectionA'=>(int)$vData["configSystemNumberConectionA"],
				'numberConectionB'=>(int)$vData["configSystemNumberConectionB"],
				'numberNoteConectionA'=>(int)$vData["configSystemNumberNoteConectionA"],
				'numberNoteConectionB'=>(int)$vData["configSystemNumberNoteConectionB"],
				'numberDebitConectionA'=>(int)$vData["configSystemNumberDebitConectionA"],
				'numberDebitConectionB'=>(int)$vData["configSystemNumberDebitConectionB"],
				'numberParkA'=>(int)$vData["configSystemNumberParkA"],
				'numberParkB'=>(int)$vData["configSystemNumberParkB"],
				'numberNoteParkA'=>(int)$vData["configSystemNumberNoteParkA"],
				'numberNoteParkB'=>(int)$vData["configSystemNumberNoteParkB"],
				'numberDebitParkA'=>(int)$vData["configSystemNumberDebitParkA"],
				'numberDebitParkB'=>(int)$vData["configSystemNumberDebitParkB"],
				'numberRentA'=>(int)$vData["configSystemNumberRentA"],
				'numberRentB'=>(int)$vData["configSystemNumberRentB"],
				'numberNoteRentA'=>(int)$vData["configSystemNumberNoteRentA"],
				'numberNoteRentB'=>(int)$vData["configSystemNumberNoteRentB"],
				'numberDebitRentA'=>(int)$vData["configSystemNumberDebitRentA"],
				'numberDebitRentB'=>(int)$vData["configSystemNumberDebitRentB"],
				'interests'=>(int)$vData["configSystemInterests"],
				'invoice'=>(int)$vData["configSystemInvoice"],
				'seat'=>(int)$vData["configSystemSeat"],
				'dolar'=>$vData["configSystemDolar"]
				
			); 

			foreach($vDataToUpdate as $sField => $sValue){
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sField." = ".$sValue.', ';
				}else{
					$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
		
			$oData = new StdClass();
			$sSQL ="UPDATE parameters SET ".$sValueList;
			
	
			$res=mysqli_query($this->cnx,$sSQL);
            
			
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0012";
			}
		}
		}catch (Exception $e){
			$oData->status = "ERR0014";
		}
		return $oData;
	}

	
	public function uploadImage($oImage,$iConfigSystemId){
		$sDirectory = LOGO_IMAGE_PATH.$iConfigSystemId."/";
		if(!is_dir($sDirectory)){
			mkdir($sDirectory);
		}
		$sDestination = $sDirectory.$oImage['configSytemLogoImage']['name'];
		move_uploaded_file($oImage['configSytemLogoImage']['tmp_name'], $sDestination);
		
		return $sDestination;
	}
	
	public function getImage(){
		$sSQL ="SELECT `logo` FROM parameters";
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$oData = mysqli_fetch_assoc($res);
		}else{
			$oData = null;
		}
		return $oData;					
	}
	
	public function updateImage($iiConfigSystemId,$sImage){
		$sSQL ="UPDATE parameters SET `logo`='$sImage'";
		$res=mysqli_query($this->cnx,$sSQL);
		//var_dump($sSQL);
		if($res){
			$oData->status = "OK";
		}else{
			$oData->status = "ERR0022";
		}
		return $oData;					
	}
	
	
	

	
	
}
	
?>