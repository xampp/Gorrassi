<?php
define("MNU_ROOT",0); //id de menu principal
define("MNU_FILE",1); //id de menu archivo
//******* MISCELANEOUS ******
$host  = $_SERVER['HTTP_HOST'];
$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$base = "http://" . $host . $uri;

define("URL_BASE",$base);
define("URL_ADMIN",URL_BASE.'/Admin/');
define("URL_TO_OPEN_MAIL",URL_BASE."/viewMail.php"); //Ruta al visor de mails
define("RETRIEVE_PASSWORD_MAIL",URL_BASE."/templates/mail/retrievePassword.html"); //Ruta al mail de recuperacion de passwords
define("EMAIL_FROM_DATA","Innovar Group Mdq <info@innovar-groupmdq.com.ar>"); //Header from del envio de mails
define("DAYS_TO_EXPIRE",30); //Dias antes que expire un usuario
define("PROPERTY_IMAGE_PATH","images/properties/");
define("LOGO_IMAGE_PATH","images/logo/");


//****************************************
// PERMISOS DE USUARIOS DEL SISTEMA
//****************************************
define("IS_ADMIN",1);
define("IS_EDIT",4);
define("IS_USER",2);
define("IS_SECURITY",3);
define("IS_USERWEB",9);
define("SYSTEM","PASCUAL2017");

?>