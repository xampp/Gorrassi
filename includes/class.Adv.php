<?php
class adv{

	private $cnx;

	public function __construct($cn){
		$this->cnx=$cn;

	}

	function wordlimit($string,$length)
	{
		$words = explode(' ', $string);
		if (count($words) > $length)
		{
			return implode(' ', array_slice($words, 0, $length));
		}
		else
		{
			return $string;
		}
	}

// Gets our data
	public function fetchData($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$result = curl_exec($ch);
		curl_close($ch); 
		return $result;
	}
	
	public function getErrorMsj($errorCode, $sLang){
		$sSQL ="SELECT * FROM dictionary WHERE label = 'lbl_$errorCode' AND lang = '$sLang'";
		$res = mysqli_query($this->cnx
			,$sSQL);
		if($res){
			$row = mysqli_fetch_assoc($res);
			if($row){
				return $row['description'];
			}else{
				return false;
			}
		}
	}
	
	public function searchAdvById($iId){
		try{
			//$oData = new StdClass();
			$oData = array();
			$sSQL ="SELECT * FROM adv WHERE id = $iId";
			
		//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
		//var_dump($row);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
			//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;

	}
	
	
	
			//productos por búsqueda
	public function getProductsSearchId($name){
		try{
			$sSQL ="
			SELECT Distinct a.id, a.file, a.file1,a.envio,a.file2,a.file3,a.model as model,a.name as name,a.quantity,
			a.cuotas, a.description,a.value,a.codBar,a.cantidadCuotas,a.valorCuota,a.valorFinanciado,b.name as brand,CONCAT(t.cuota,' ',t.name,' DE $ ',round( ( round(a.value) + (round(a.value) * (t.rec/100))) / t.cuota )) as condicion
			FROM adv a 
			INNER JOIN brands b ON a.brand=b.id 
			INNER JOIN terms t ON a.conditionPay=t.id 
			WHERE a.status=1 AND a.id=$name AND t.visible=1 
			ORDER BY a.value asc limit 1";
			
			//var_dump($sSQL);exit;
			$oData = array();
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	

	public function get_Images(){
		$sSQL ="
		SELECT id, file	as name, description
		FROM adv where type=0 and status=2";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}

	
	public function get_Numero(){
		$sSQL ="
		SELECT (numero+1) as numero FROM `orders` WHERE type=0 order by numero desc limit 1";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData=$row['numero'];
			}
			return $vData;
		}
	}
	
	
	public function register($vData){
		$sValueList = '';
	  $sFieldList = '';
		if($vData){
			foreach($vData as $sField => $sValue){
				$sFieldList.= "`".$sField."`, ";
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= '\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
		}
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO adv
			(".$sFieldList.")
			VALUES 
			(".$sValueList.")";
					//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status = "OK";
				$oData->lastId = mysqli_insert_id($this->cnx);
			}else{
				$oData->status = "ERR0007";
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
		}

		
		return $oData;
	}


	
	public function add($idADV,$id){
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO movements
			(adv,user,date,paid,value,datePaid,pay)
			VALUES 
			($idADV,$id,'".date("Y-m-d")."', 0, (select value from parameters),'',0)";
					//var_dump($sSQL);
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
					$oData->status = "OK";
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			

			return $oData;
		}



		public function searchNewsAdv($iMinLimit, $iRecPerPage,& $iRecordsTotal){
			$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
			id,file,email, type FROM adv WHERE status=1";
		//var_dump($sSQL);exit;
			try{
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
					while ($row = mysqli_fetch_array($res)){
						$oData = new stdClass();
						$oData->id 			= $row['id'];
						$oData->file 		= $row['file'];
						$oData->email 	= $row['email'];
						$oData->type 	= $row['type'];
						$vData[]=$oData;
					}
				}else{
					throw new Exception("ERR0006");
				}
			}catch (Exception $e){
				return $e->getMessage();
			}
			$sSQL ="SELECT FOUND_ROWS() as Total";
			$res=mysqli_query($this->cnx,$sSQL);
			$row=mysqli_fetch_assoc($res);
			$iRecordsTotal=$row['Total'];
			return $vData;			
		}


		public function searchTotalAdv(){
			$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
			id FROM adv WHERE status=2";
		//var_dump($sSQL);exit;
			try{
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
					$vData= array();
					while ($row = mysqli_fetch_array($res)){
						$oData = new stdClass();
						$oData->id 			= $row['id'];
						$vData[]=$oData;
					}
				}else{
					throw new Exception("ERR0006");
				}
			}catch (Exception $e){
				return $e->getMessage();
			}
			$sSQL ="SELECT FOUND_ROWS() as Total";
			$res=mysqli_query($this->cnx,$sSQL);
			$row=mysqli_fetch_assoc($res);
			$iRecordsTotal=$row['Total'];
			return $vData;			
		}

		public function activate($iID, $activar){
			$sSQL ="UPDATE adv SET status = $activar WHERE id=$iID";
			$res = mysqli_query($this->cnx
				,$sSQL);
			if($res){
				return true;
			}else{
				return false;
			}

		}

		public function solicitar($iID){
			$sSQL ="UPDATE movements SET paid = 2 WHERE user = $iID AND paid = 0";
			$res = mysqli_query($this->cnx
				,$sSQL);
			if($res){
				return true;
			}else{
				return false;
			}

		}

		public function searchAdvertising($iUserID,$sUserDescription,$sUserEmail,$iMinLimit, $iRecPerPage,& $iRecordsTotal){
			$sWhere = null;
			if(!$iMinLimit){
				$iMinLimit = 0;
			}
			$sLimit = " LIMIT $iMinLimit, $iRecPerPage";
			if($iUserID){
				$sWhere=" WHERE u.id = $iUserID";
			}
			if($sUserDescription){
				if(!$sWhere){
					$sWhere=" WHERE u.description LIKE '%$sUserDescription%'";
				}else{
					$sWhere.=" AND u.description LIKE '%$sUserDescription%'";
				}
			}

			if($sUserEmail){
				if(!$sWhere){
					$sWhere=" WHERE u.email LIKE '%$sUserEmail%'";
				}else{
					$sWhere.=" AND u.email LIKE '%$sUserEmail%'";
				}
			}


			$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
			u.id as id, 
			u.name as name,
			u.email,
			u.phone,
			u.file,
			u.company,
			u.description,
			CASE u.typeBill 
			WHEN '1' THEN 'PAYPAL' 
			ELSE 'SOPDCART'
			END as pay,
			CASE u.status 
			WHEN '1' THEN 'NEW'
			WHEN '2' THEN 'GOOD'			  
			ELSE 'BAD'
			END as status,
			u.type
			FROM adv u
			$sWhere $sLimit";


		//var_dump($sSQL);exit;
			try{
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
					$vData= array();
					while ($row = mysqli_fetch_array($res)){
						$oData = new stdClass();
						$oData->userID 			= $row['id'];
						$oData->userDescription = $row['description'];
						$oData->userPhone 		= $row['phone'];
						$oData->userName 		= $row['name'];
						$oData->userEmail 		= $row['email'];
						$oData->userPay 		= $row['pay'];
						$oData->userCompany 	= $row['company'];
						$oData->userType 		= $row['type'];
						$oData->userFile 		= $row['file'];
						$oData->userStatus 		= $row['status'];
						$vData[]=$oData;
					}
				}else{
					throw new Exception("ERR0006");
				}
			}catch (Exception $e){
				return $e->getMessage();
			}
			$sSQL ="SELECT FOUND_ROWS() as Total";
			$res=mysqli_query($this->cnx,$sSQL);
			$row=mysqli_fetch_assoc($res);
			$iRecordsTotal=$row['Total'];
			return $vData;			
		}

		public function getProductsOffer22(){
			$sSQL ="
			SELECT id, file, file1,file2,file3,name,cuotas, description,value
			FROM adv where offer=1 and status=1";
			
			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while ($row = mysqli_fetch_array($res)){
					$oData = new stdClass();
					$oData->id 			= $row['id'];
					$oData->file 		= $row['file'];
					$oData->file1 		= $row['file1'];
					$oData->file2		= $row['file2'];
					$oData->file3 		= $row['file3'];
					$oData->name 	= $row['name'];
					$oData->cuotas 	= $row['cuotas'];
					$oData->description 	= $row['description'];
					$vData[]=$oData;
				}
				return $vData;
			}
		}

		public function getProductsOffer(){
			$sSQL ="
			SELECT a.id, a.file, a.file1,a.file2,a.model,a.file3,a.name as name,a.cuotas, a.description,a.value,a.cantidadCuotas,a.valorCuota,a.valorFinanciado,b.name as brand
			FROM adv a INNER JOIN brands b ON a.brand=b.id where a.offer=1 and a.status=1 order by a.id desc limit 3";
			
			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
		}

		public function getSubCategorys($cate){
			$sSQL ="
			SELECT id, lower(trim(description)) as description
			FROM subcategory where category = $cate";
			
			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
		}

		public function getCategorys(){
			$sSQL ="
			select c.id, c.description,GROUP_CONCAT(CONCAT(s.id,'-',s.description)) as prueba,

			GROUP_CONCAT(s.description) as sub,
			GROUP_CONCAT(s.id) as subId
			FROM category c left JOIN subcategory s ON c.id=s.category
			GROUP BY c.id";

	//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
		}

//productos por búsqueda
		public function getProductsSearch($name){
			$sSQL ="
			SELECT a.id, a.file, a.file1,a.file2,a.model,a.file3,a.name as name,a.cuotas, a.description,a.value,b.name as brand,a.quantity,
			a.cantidadCuotas,a.valorCuota,a.valorFinanciado
			FROM adv a 
			INNER JOIN brands b ON a.brand=b.id
			INNER JOIN terms t ON a.conditionPay=t.id 			
			WHERE a.status=1 AND (a.name like '%$name%' OR a.description like '%$name%' OR b.name like '%$name%') AND t.visible=1
			ORDER BY a.value asc";

	//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				//var_dump($res);exit;
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				//var_dump(count($vData));exit;
				return $vData;
			}
			//var_dump($vData);exit;
		}


	//productos por seleccion
		public function getProductsSearchEmail($name){
			$sSQL ="
			SELECT a.id, a.file, a.file1,a.file2,a.model,a.file3,a.name as name,a.cuotas, a.description,a.value,b.name as brand,a.quantity,
			a.cantidadCuotas,a.valorCuota,a.valorFinanciado
			FROM adv a 
			INNER JOIN brands b ON a.brand=b.id
			INNER JOIN terms t ON a.conditionPay=t.id 			
			WHERE a.status=1 AND a.id in ($name) AND t.visible=1
			ORDER BY a.value asc";

			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
		}



	//productos por web en categoria y subcategoria
public function getProductsByArt($name){
	
	$datos = explode("/", $name);

	if ($datos[0]>=1 && $datos[0]<=9) {
		$Where ="WHERE a.status=1 and a.category = $datos[0] "; 
	}
	if ($datos[1]){
		$Where.="AND a.subcategory = $datos[1]";
	}

	
	$sSQL ="
	SELECT Distinct a.id, a.file,a.model, a.file1,a.file2,a.file3,a.name as name,a.cuotas, a.description,a.value,a.cantidadCuotas,a.valorCuota,a.valorFinanciado,b.name as brand
	FROM adv a INNER JOIN brands b ON a.brand=b.id $Where ORDER BY a.value";
	
	//var_dump($sSQL);exit;
	$res=mysqli_query($this->cnx,$sSQL);
	if($res){
		$vData= array();
		while($row = mysqli_fetch_assoc($res)){
			$vData[]=$row;
		}
		return $vData;
	}
	
}


public function getNameProductsByArt($name){
	
	$datos = explode("/", $name);

	if ($datos[0]>=1 && $datos[0]<=9){
		$Where ="WHERE a.id = $datos[0] ";
	}

	$sub="";
	if (count($datos)>1) {
		$sub=",(Select s.description FROM subcategory s Where s.id=$datos[1] and s.category=$datos[0]) as subcategoria";
	}
	
	$sSQL ="
	SELECT Distinct a.description as categoria $sub
	FROM category a $Where";
	
	//var_dump($sSQL);exit;
	$res=mysqli_query($this->cnx,$sSQL);
	if($res){
		$vData= array();
		while($row = mysqli_fetch_assoc($res)){
			$vData[]=$row;
		}
		return $vData;
	}
	
}



	//submunu

		public function getProductsByArtSub($name){

			if ($name>=1 && $name<=9) {
				$Where ="WHERE a.status=1 and a.category = $name "; 
			}

			
			$sSQL ="
			SELECT Distinct a.id, a.file, a.file1,a.model as model,a.file2,a.file3,a.name as name,a.cuotas, a.description,a.quantity,
			b.name as brand,a.value,a.subcategory as category,a.cantidadCuotas,a.valorCuota,a.valorFinanciado
			FROM adv a INNER JOIN brands b ON a.brand=b.id $Where ORDER BY a.value";
			
			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
				
		}


			//servicios tecnicos cargados
		public function getServicesOn(){

			$sSQL ="
			SELECT Distinct a.id,b.name as brand,a.name,a.address,a.observaciones,a.phone,a.cellPhone 
			FROM services a INNER JOIN brands b ON a.brand=b.id";


			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
			
		}


			//servicios tecnicos cargados
		public function getServicesOnIn(){

			$sSQL ="
			SELECT Distinct a.id,b.name as brand,a.name,a.address,a.observaciones,a.phone,a.cellPhone 
			FROM services a INNER JOIN brands b ON a.brand=b.id
			";
	        //WHERE type=1

			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
			
		}


		//servicios tecnicos cargados
		public function getServices($name){

			if(is_numeric ($name)){

				$sSQL ="
				SELECT Distinct a.numero as id,concat('Orden: 0001-',' ',YEAR(NOW()),LPAD(a.numero,4,0)) as brand,
				concat('Marca: ',bra.name,'<br>','Articulo: ',a.articulo, '<br>',' Problema: ',a.problem,' ', a.descrip) as name,
				concat('Fecha Ingreso',' ',a.beginDate) as address,
				concat('<strong>Estado: </strong>',b.name) as observaciones,
				a.seguimiento as phone,
				a.articulo as phone1


				FROM orders  a INNER JOIN order_status b ON a.status=b.id 
				INNER JOIN brands bra ON a.marca=bra.id
				WHERE a.type=0 AND (a.dni=$name or a.numero=$name)";

			} else {
				$sSQL ="
				SELECT Distinct a.id as id,b.name as brand,a.name as name,a.address as address,a.observaciones as observaciones,a.phone as phone,a.cellPhone as cellPhone 
				FROM services a INNER JOIN brands b ON a.brand=b.id WHERE a.type=0 AND b.name LIKE '%$name%'";
			}



			
			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
		}

	//productos totales
		public function getProducts(){
			$sSQL ="
			SELECT a.id, a.file, a.file1,a.file2,a.file3,a.model as model,a.name,a.cantidadCuotas as cuotas, 
			a.description,a.value,c.description as category,dateUpdate as sUpdate,a.codBar,a.quantity,
			s.description as subcategory
			,e.name as brand, if (a.status=1,'VISIBLE','NO VISIBLE') as visible,
			if(a.envio=1,'CONSULTAR ENVIO','ENVIO SIN CARGO') as envio,a.valorCuota,a.valorFinanciado
				FROM adv a INNER JOIN category c ON a.category=c.id INNER JOIN subcategory s ON a.subcategory=s.id
			INNER JOIN brands e ON a.brand=e.id ORDER BY e.name,a.name,a.value";
			
			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
			
		}

	//productos por id
		public function getProductsById($iId){
			try{
				$oData = new StdClass();
				$sSQL ="SELECT id, file, file1,conditionPay,quantity,dateUpdate,file2,file3,envio,status,model,codBar,offer,name,cuotas, description,value,category,subcategory,brand,valorFinanciado,valorCuota,cantidadCuotas
				FROM adv  WHERE id=$iId";
				
			//var_dump($sSQL);exit;
				$res=mysqli_query($this->cnx,$sSQL);
				$row = mysqli_fetch_assoc($res);
				if($row){
					$oData = $row;
					$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
				}else{
					$oData['queryStatus']="ERR0030";
				}
			}catch (Exception $e){
				$oData['queryStatus']="ERR0030";
			}
			return $oData;

		}


		public function newAdvUp($sNameUpdate){

			if($sNameUpdate){
				try{

					$oData = new StdClass();
				//aca comienza la lectura del archivo excell
					require_once 'Excel/reader.php';
					$data = new Spreadsheet_Excel_Reader();
					$data->setOutputEncoding('CP1251');
				//move_uploaded_file($sNameUpdate['sNameUpdate']['tmp_name'],'temp/products.xls');
				//var_dump($sNameUpdate);exit;
				//Ver donde esta el archivo temporal
					$data->read('temp/'.$sNameUpdate);
					error_reporting(E_ALL ^ E_NOTICE);


					for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {

						if ($data->sheets[0]['cells'][$i][3]!='' && $data->sheets[0]['cells'][$i][7]!='') {

							$oData = new StdClass();
							$sSQL ="
							INSERT INTO adv (description,file,name,file1,file2,file3,
								value,category,offer,cuotas,subcategory,cantidadCuotas,valorCuota,valorFinanciado,brand,status,model,quantity)
							VALUES ('".$data->sheets[0]['cells'][$i][1]."','".$data->sheets[0]['cells'][$i][2]."','".$data->sheets[0]['cells'][$i][3]."','".$data->sheets[0]['cells'][$i][4]."','".$data->sheets[0]['cells'][$i][5]."','".$data->sheets[0]['cells'][$i][6]."',".$data->sheets[0]['cells'][$i][7].",".$data->sheets[0]['cells'][$i][8].",
								".$data->sheets[0]['cells'][$i][9].",'".$data->sheets[0]['cells'][$i][10]."',".$data->sheets[0]['cells'][$i][11].
								",".$data->sheets[0]['cells'][$i][12].",".$data->sheets[0]['cells'][$i][13].",".$data->sheets[0]['cells'][$i][14].",".$data->sheets[0]['cells'][$i][15].
								",1,".$data->sheets[0]['cells'][$i][16].",".$data->sheets[0]['cells'][$i][17].")";
//var_dump($sSQL);exit;
							$res=mysqli_query($this->cnx,$sSQL);
						}
					}
					
					unlink('temp/'.$sNameUpdate);
					
					if($res){
						$oData->status = "OK";
					}else{
						$oData->status = "ERR0025";
					}
				}catch (Exception $e){
					$oData->status = "ERR0024";
				}
			} else {
				$oData = new StdClass();
				$oData->status = "ERR0024";
			}
			return $oData;
		}


		public function editProducts($vData){

			if($vData){

				$vDataToUpdate = array(
					'name'=>$vData->name,
					'description'=>$vData->description,
					'file'=>$vData->file,
					'file1'=>$vData->file1,
					'file2'=>'sinImagen.jpg',
					'file3'=>'sinImagen.jpg',
					'value'=>$vData->value,
					'cuotas'=>$vData->cuotas,
					'category'=>$vData->category,
					'valorCuota'=>$vData->valorCuota,
					'cantidadCuotas'=>$vData->cantidadCuotas,
					'valorFinanciado'=>$vData->valorFinanciado,
					'brand'=>$vData->brand,
					'offer'=>$vData->offer,
					'envio'=>$vData->envio,
					'status'=>$vData->status,
					'model'=>$vData->model,
					'quantity'=>$vData->quantity,
					'conditionPay'=>$vData->conditionPay,
					'dateUpdate'=>$vData->dateUpdate,
					'codBar'=>$vData->codBar,
					'subcategory'=>$vData->subcategory); 

        $sValueList = '';
				foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValue= is_null($sValue)? "null":$sValue;
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}

				}
				$sValueList = substr($sValueList,0,-2);

			}
			try{

				$oData = new StdClass();
				$sSQL ="
				UPDATE adv
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
				
				//var_dump($sSQL);exit;

				$res=mysqli_query($this->cnx,$sSQL);

			/*Var_dump($sSQL);
			Var_dump($res);
			exit;*/

			if($res){
				$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	public function deleteProductById($iId){
		try{
			$oData = new StdClass();
			$sSqlArchivo = "select file,file1,file2,file3 from adv where id = $iId";
			$res1=mysqli_query($cnx,$sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);
			
			$sSQL ="
			DELETE FROM adv WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				if ($fila['file']<>'sinImagen.jpg'){
					unlink("img_prod/".$fila['file']);
				}
				if ($fila['file1']<>'sinImagen.jpg'){
					unlink("img_prod/".$fila['file1']);
				}
				if ($fila['file2']<>'sinImagen.jpg'){
					unlink("img_prod/".$fila['file2']);
				}
				if ($fila['file3']<>'sinImagen.jpg'){
					unlink("img_prod/".$fila['file3']);
				}
				
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	//varios productos
	
	public function deleteProductByIds($iId){
		try{
			$oData = new StdClass();
			$sSqlArchivo = "select file,file1,file2,file3 from adv where id IN ($iId)";
			$res1=mysqli_query($cnx,$sSqlArchivo);
			//$fila = mysqli_fetch_assoc($res1);
			
			$sSQL ="
			DELETE FROM adv WHERE id IN ($iId)";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				
				
				if($res1){

					while($fila = mysqli_fetch_assoc($res1)){

						if ($fila['file']<>'sinImagen.jpg'){
							unlink("img_prod/".$fila['file']);
						}
						if ($fila['file1']<>'sinImagen.jpg'){
							unlink("img_prod/".$fila['file1']);
						}
						if ($fila['file2']<>'sinImagen.jpg'){
							unlink("img_prod/".$fila['file2']);
						}
						if ($fila['file3']<>'sinImagen.jpg'){
							unlink("img_prod/".$fila['file3']);
						}

					}

				}

				
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	
//slider todo	
	public function editSlider($vData){

		if($vData){
			$sValueList = '';
			$vDataToUpdate = array(
				'name'=>$vData->name,
				'description'=>$vData->description,
				'img'=>$vData->img);
		
			foreach($vDataToUpdate as $sField => $sValue){
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sField." = ".$sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
		}
		try{

			$oData = new StdClass();
			$sSQL ="
			UPDATE slider
			SET ".$sValueList.
			" WHERE id = ".$vData->id;

			$res=mysqli_query($this->cnx,$sSQL);

			if($res){
				$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function deleteSliderById($iId){
		try{
			$oData = new StdClass();
			$sSqlArchivo = "select img from slider where id = $iId";
			$res1=mysqli_query($this->cnx,$sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);
			
			$sSQL ="
			DELETE FROM slider WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				unlink("img_prod/slider/".$fila['img']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	public function getSliderById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id,name,description,img
			FROM slider  WHERE id=$iId";

			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	//sliders totales
	public function getSliders(){
		$sSQL ="
		SELECT id, name,description,img
		FROM slider";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			//var_dump($vData);exit;
			return $vData;
		}
	}
	
	public function newSlider($vData){
		$sValueList = '';
		$sFieldList = '';
		if($vData){
			foreach($vData as $sField => $sValue){
				$sFieldList.= "`".$sField."`, ";
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= '\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
		}
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO slider
			(".$sFieldList.")
			VALUES 
			(".$sValueList.")";
					//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status = "OK";
				$oData->lastId = mysqli_insert_id($this->cnx);
			}else{
				$oData->status = "ERR0007";
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
		}

		//var_dump($oData);exit;
		return $oData;
	}
	
//service todo

	public function editService($vData){

		if($vData){

			/*$vDataToUpdate = array();
			foreach ($vData as $sField => $sValue){
				$vDataToUpdate[]= $sField =>$sValue;
			}*/


			$vDataToUpdate = array(
				'name'=>$vData->name,
				'lastName'=>$vData->lastName,
				'phone'=>$vData->phone,
				//'cellPhone'=>$vData->cellPhone,
				'status'=>$vData->status,
				'problem'=>$vData->problem,
				//'descrip'=>$vData->descrip,
				'obser'=>$vData->obser,
				'email'=>$vData->email,
				'marca'=>$vData->marca,
			//'recibe'=>$vData->recibe,
				'credito'=>$vData->credito,
			//'serie'=>$vData->serie,
				'bill'=>$vData->bill,
				'dni'=>$vData->dni,
				'modelo'=>$vData->modelo,
				'articulo'=>$vData->articulo,
				'fecha'=>$vData->fecha,
				//'comentario'=>$vData->comentario,
				//'endDate'=>$vData->endDate
				); 

			foreach ($vData as $sField => $sValue){				
				switch ($sField) {
					case 'cellPhone':
						$vDataToUpdate['cellPhone']=$sValue;
						break;
					case 'descrip':
						$vDataToUpdate['descrip']=$sValue;
						break;
					case 'recibe':
						$vDataToUpdate['recibe']=$sValue;
						break;
					case 'serie':
						$vDataToUpdate['serie']=$sValue;
						break;
					case 'comentario':
						$vDataToUpdate['comentario']=$sValue;
						break;
					case 'endDate':
						$vDataToUpdate['endDate']=$sValue;
						break;
					default:
						// code...
						break;
				}				
			}
			

			$sValueList = '';
			foreach($vDataToUpdate as $sField => $sValue){
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sField." = ".$sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
				}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{

			$oData = new StdClass();
			$sSQL ="
			UPDATE orders
			SET ".$sValueList.
			" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;

			$res=mysqli_query($this->cnx,$sSQL);

			if($res){
				$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function editServiceMode($vData){

		if($vData){
			
			$vDataToUpdate = array(
				'comentario'=>$vData->comentario); 
					
			$sValueList = '';
			foreach($vDataToUpdate as $sField => $sValue){
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sField." = ".$sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
				}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{

			$oData = new StdClass();
			$sSQL ="
			UPDATE orders
			SET ".$sValueList.
			" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;

			$res=mysqli_query($this->cnx,$sSQL);

			if($res){
				$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	
public function editServiceNext($vData){

	if($vData){
		
		$vDataToUpdate = array(
			'status'=>$vData->status,
			'seguimiento'=>$vData->seguimiento,
			'comentario'=>$vData->comentario,
			'orden'=>$vData->orden,
			'endDate'=>$vData->endDate,
			'fechaSeguimiento'=>$vData->fechaSeguimiento,
			'servicio'=>$vData->servicio); 
		
		$sValueList = '';

		foreach($vDataToUpdate as $sField => $sValue){
			if(is_int($sValue) || is_float($sValue)){
				$sValueList .= $sField." = ".$sValue.', ';
			}else{
				$sValue= is_null($sValue)? "null":$sValue;
				$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
			}
			
		}
		$sValueList = substr($sValueList,0,-2);

	}
	try{

		$oData = new StdClass();
		$sSQL ="
		UPDATE orders
		SET ".$sValueList.
		" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;

		$res=mysqli_query($this->cnx,$sSQL);

		if($res){
			$oData->status = "OK";
			
		}else{
			$oData->status = "ERR0037";
		}
	}catch (Exception $e){
		$oData->status = "ERR0037";
	}
	//var_dump('entro aca fin que');
	return $oData;
}

	
	
	
	
	
	public function deleteServiceById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysqli_query($sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);*/
			
			$sSQL ="
			DELETE FROM orders WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	public function getServiceById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id, name,numero,dni,articulo,lastName,phone,beginDate,problem,email,obser,descrip,status,endDate,marca,credito,fecha,serie,bill,modelo,recibe,
			seguimiento,servicio,comentario,orden,cellPhone,fechaSeguimiento
			FROM orders  WHERE id=$iId";

			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
		//sliders totales
	public function getService($n){
		$sSQL ="
		SELECT a.id, a.name,a.status as estado,a.lastName,a.cellPhone,a.articulo,a.phone,a.beginDate,
		a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,a.dni as dni,
		b.name as marca,a.credito,a.fecha,a.serie,a.modelo,a.numero
		FROM orders as a 
		INNER JOIN brands b ON a.marca=b.id 
		INNER JOIN order_status as c 
		ON a.status=c.id where a.type=$n ORDER BY a.beginDate ASC";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	
	public function getServiceNews(){
		$sSQL ="
		SELECT a.id, a.name,a.numero,a.lastName,a.dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
		a.credito,a.fecha,a.serie,a.modelo
		FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id WHERE a.status=1 AND a.type=0 ORDER BY a.id DESC";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	public function getServiceRepa(){
		$sSQL ="
		SELECT a.id, a.numero,a.name,a.lastName,a.dni as dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
		a.credito,a.fecha,a.serie,a.modelo as modelo,a.fechaSeguimiento,d.name as servicio, d.phone 
		FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id 
		INNER JOIN services d ON a.servicio=d.id WHERE a.status<>1 AND a.status=2 AND a.type=0 ORDER BY a.id DESC";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	public function getServiceReparados(){
		$sSQL ="
		SELECT a.id, a.name,a.numero,a.lastName,a.dni as dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
		a.credito,a.fecha,a.serie,a.modelo as modelo,a.fechaSeguimiento,d.name as servicio, d.phone 
		FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id 
		INNER JOIN services d ON a.servicio=d.id WHERE a.status<>1 AND a.type=0 AND a.status=3  ORDER BY a.id DESC";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	
	public function getServiceFina(){
		$sSQL ="
		SELECT a.id, a.name,a.numero,a.lastName,a.dni as dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
		a.credito,a.fecha,a.serie,a.modelo as modelo,a.fechaSeguimiento,d.name as servicio, d.phone 
		FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id 
		INNER JOIN services d ON a.servicio=d.id WHERE a.status<>1 AND a.type=0 AND a.status=4 ORDER BY a.id DESC";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	public function getServiceDevo(){
		$sSQL ="
		SELECT a.id, a.name,a.numero,a.articulo,a.lastName,a.dni as dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
		a.credito,a.fecha,a.serie,a.modelo as modelo,a.fechaSeguimiento,d.name as servicio, d.phone 
		FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id 
		INNER JOIN services d ON a.servicio=d.id WHERE a.status<>1 AND a.type=0 AND a.status=5 ORDER BY a.id DESC";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	
	
	
	public function deletePedidoById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysqli_query($sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);*/
			
			$sSQL ="
			DELETE FROM pedido WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	
	
	
	public function getPedidoById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id, name,numero,dni,articulo,lastName,phone,beginDate,problem,email,obser,descrip,status,endDate,marca,credito,fecha,serie,bill,modelo,recibe,
			seguimiento,servicio,comentario,orden,cellPhone,fechaSeguimiento,
			(select nombre from parametros) as empresa,
			(select domicilio from parametros) as address,
			(select web from parametros) as web,
			(select email from parametros) as emailEmpresa,
			(select telefono from parametros) as phoneEmpresa,
			(select logo from parametros) as logo	

			FROM pedido WHERE id=$iId";

			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				if(count($this->getPedidosDetails($iId))>0){
					foreach ($this->getPedidosDetails($iId) as $ItemM){
						$detalle = $detalle . 'NOMBRE: '.$ItemM['cod'].'<br>'.$ItemM['size'].'<br>'.$ItemM['color'].'<br> CANTIDAD: '.$ItemM['quantity'].'<br> PRECIO UNIDAD: '.$ItemM['precioUnidad'].'<br> PRECIO TOTAL: '.$ItemM['total'].' <br><br>';
					} 
				} 
				$oData['articulos'] = $detalle;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	
	
	
	public function editPedido($vData){

		if($vData){
			
			$vDataToUpdate = array(
			/*'name'=>$vData->name,
			'lastName'=>$vData->lastName,
			'phone'=>$vData->phone,
			'cellPhone'=>$vData->cellPhone,*/
			'status'=>$vData->status,
			/*'problem'=>$vData->problem,
			'descrip'=>$vData->descrip,
			'obser'=>$vData->obser,
			'email'=>$vData->email,
			'marca'=>$vData->marca,
			'recibe'=>$vData->recibe,
			'credito'=>$vData->credito,
			'serie'=>$vData->serie,
			'bill'=>$vData->bill,
			'dni'=>$vData->dni,
			'modelo'=>$vData->modelo,
			'articulo'=>$vData->articulo,
			'fecha'=>$vData->fecha,*/
			'comentario'=>$vData->comentario
		); 
			
			$sValueList = '';	
//'endDate'=>$vData->endDate
			
			foreach($vDataToUpdate as $sField => $sValue){
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sField." = ".$sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
				}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{

			$oData = new StdClass();
			$sSQL ="
			UPDATE pedido
			SET ".$sValueList.
			" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;

			$res=mysqli_query($this->cnx,$sSQL);

			if($res){
				$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	public function getPedidosDetails($id){
		$sSQL ="
		SELECT id, cod,size,color,precioUnidad,total,quantity
		FROM pedido_next where idOrden = $id";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	
	
	
	
	public function getPedidos($type){

		if ($type==1) {
			$sWhere = " WHERE c.id=1 OR c.id=3";
		} else {
			$sWhere = " WHERE c.id in (2,4,5,6) ";
		}

		$sSQL ="
		SELECT a.id, a.name,a.status as estado,a.lastName,a.cellPhone,a.articulo,a.phone,a.beginDate,
		a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,a.dni as dni,
		a.credito,a.fecha,a.serie,a.modelo,a.numero
		FROM pedido as a 
		INNER JOIN pedido_status as c			
		ON a.status=c.id $sWhere ORDER BY a.beginDate ASC";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	

	public function getStatusPedido(){
		$sSQL ="
		SELECT id,name
		FROM pedido_status order by id";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	
	
	
	
	
	public function getStatus(){
		$sSQL ="
		SELECT id,name
		FROM order_status order by id";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	public function updatePedido($iID, $activar){
		$sSQL ="UPDATE pedido SET dni = $activar WHERE id=$iID";
		$res = mysqli_query($this->cnx
			,$sSQL);
		if($res){
			return true;
		}else{
			return false;
		}
		
	}
	
	
	public function newPedido($vData){
		$sValueList = '';
		$sFieldList = '';
		if($vData){
			foreach($vData as $sField => $sValue){
				$sFieldList.= "`".$sField."`, ";
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= '\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
		}
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO pedido
			(".$sFieldList.")
			VALUES 
			(".$sValueList.")";
					//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status = "OK";
				$oData->lastId = mysqli_insert_id($this->cnx);
			}else{
				$oData->status = "ERR0007";
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
		}

		
		return $oData;
	}
	
	
	public function newPedidoMovements($vData){
		$sValueList = '';
		$sFieldList = '';
		if($vData){
			foreach($vData as $sField => $sValue){
				$sFieldList.= "`".$sField."`, ";
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= '\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
		}
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO pedido_next
			(".$sFieldList.")
			VALUES 
			(".$sValueList.")";
					//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status = "OK";
				$oData->lastId = mysqli_insert_id($this->cnx);
			}else{
				$oData->status = "ERR0007";
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
		}

		
		return $oData;
	}
	
	public function newService($vData){
		$sValueList = '';
		$sFieldList = '';
		if($vData){
			foreach($vData as $sField => $sValue){
				$sFieldList.= "`".$sField."`, ";
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= '\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
		}
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO orders
			(".$sFieldList.")
			VALUES 
			(".$sValueList.")";
					//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status = "OK";
				$oData->lastId = mysqli_insert_id($this->cnx);
			}else{
				$oData->status = "ERR0007";
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
		}

		
		return $oData;
	}
	
	
	
	public function searchOrderByIdOrder($iId){
		

		$sSQL ="SELECT 
		a.id,
		a.numero,
		CONCAT(a.lastName,' ',a.name) as userName,
		a.dni,
		a.articulo as articulo,
		a.phone,
		a.beginDate,
		a.problem,
		a.email,
		a.obser,
		a.descrip,
		c.name as status,
		a.endDate,
		b.name as marca,
		a.credito,
		a.fecha,
		a.serie,
		a.bill,
		a.modelo,
		a.recibe,
		a.seguimiento,
		(select nombre from parametros) as empresa,
		(select domicilio from parametros) as address,
		(select web from parametros) as web,
		(select email from parametros) as emailEmpresa,
		(select telefono from parametros) as phoneEmpresa,
		(select logo from parametros) as logo					


		FROM orders a 
		INNER JOIN brands b ON a.marca = b.id 
		INNER JOIN order_status c ON a.status = c.id 
		WHERE a.id = $iId limit 1";

	//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);

	//var_dump($sSQL);exit;



		if ($res) {
			$oData = new stdClass();
			$oData = mysqli_fetch_assoc($res);
			return $oData;

		} 
		
	}
	
	
	
	
	
	
	
	
	
	
	public function newServices($vData){
		$sValueList = '';
		$sFieldList = '';
		if($vData){
			foreach($vData as $sField => $sValue){
				$sFieldList.= "`".$sField."`, ";
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= '\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
		}
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO services
			(".$sFieldList.")
			VALUES 
			(".$sValueList.")";
					//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status = "OK";
				$oData->lastId = mysqli_insert_id($this->cnx);
			}else{
				$oData->status = "ERR0007";
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
		}

		
		return $oData;
	}
	
	
	public function editServices($vData){

		if($vData){
			
			$vDataToUpdate = array(
				'name'=>$vData->name,
				'phone'=>$vData->phone,
				'cellPhone'=>$vData->cellPhone,
				'email'=>$vData->email,
				'observaciones'=>$vData->obser,
				'brand'=>$vData->brand,
				'type'=>$vData->type,
				'address'=>$vData->address); 
			
			$sValueList = '';
			foreach($vDataToUpdate as $sField => $sValue){
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sField." = ".$sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
				}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{

			$oData = new StdClass();
			$sSQL ="
			UPDATE services
			SET ".$sValueList.
			" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);

			if($res){
				$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function deleteServicesById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysqli_query($sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);*/
			
			$sSQL ="
			DELETE FROM services WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	public function getServicesById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id,name,address,observaciones,phone,brand,email,cellPhone,type
			FROM services  WHERE id=$iId";

			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	
	public function newMarca($vData){
		$sValueList = '';
		$sFieldList = '';
		if($vData){
			foreach($vData as $sField => $sValue){
				$sFieldList.= "`".$sField."`, ";
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= '\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
		}
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO brands
			(".$sFieldList.")
			VALUES 
			(".$sValueList.")";
					//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status = "OK";
				$oData->lastId = mysqli_insert_id($this->cnx);
			}else{
				$oData->status = "ERR0007";
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
		}

		
		return $oData;
	}
	
	public function newCondicion($vData){
		$sValueList = '';
		$sFieldList = '';
		if($vData){
			foreach($vData as $sField => $sValue){
				$sFieldList.= "`".$sField."`, ";
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= '\''. rawurldecode($sValue).'\', ';
				}
			}
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
		}
		try{
			$oData = new StdClass();
			$sSQL ="
			INSERT INTO terms
			(".$sFieldList.")
			VALUES 
			(".$sValueList.")";
					//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status = "OK";
				$oData->lastId = mysqli_insert_id($this->cnx);
			}else{
				$oData->status = "ERR0007";
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
		}

		
		return $oData;
	}
	
	public function editCondicion($vData){

		if($vData){
			
			$vDataToUpdate = array(
				'name'=>$vData->name,
				'rec'=>$vData->rec,
				'cuota'=>$vData->cuota,
				'visible'=>$vData->visible
			); 
			
			$sValueList = '';
			foreach($vDataToUpdate as $sField => $sValue){
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sField." = ".$sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
				}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{

			$oData = new StdClass();
			$sSQL ="
			UPDATE terms
			SET ".$sValueList.
			" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);

			if($res){
				$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function editMarca($vData){

		if($vData){
			
			$vDataToUpdate = array(
				'name'=>$vData->name); 
			
			$sValueList = '';
			foreach($vDataToUpdate as $sField => $sValue){
				if(is_int($sValue) || is_float($sValue)){
					$sValueList .= $sField." = ".$sValue.', ';
				}else{
					$sValue= is_null($sValue)? "null":$sValue;
					$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
				}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{

			$oData = new StdClass();
			$sSQL ="
			UPDATE brands
			SET ".$sValueList.
			" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);

			if($res){
				$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function deleteMarcaById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysqli_query($sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);*/
			
			$sSQL ="
			DELETE FROM brands WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	public function deleteCondicionById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysqli_query($sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);*/
			
			$sSQL ="
			DELETE FROM terms WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	public function getMarcaById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id,name 
			FROM brands  WHERE id=$iId";

			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	public function getCondicionById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id,name,rec,visible,cuota
			FROM terms  WHERE id=$iId";

			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	public function getMarcas(){
		$sSQL ="
		SELECT id, name
		FROM brands";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	public function getCondiciones($con=null){

		$Where='';	
		if ($con==1) {
			$Where .="WHERE visible=1 ORDER BY cuota"; 
		}	



		$sSQL ="
		SELECT id, name,rec,if(visible=1,'VISIBLE','NO VISIBLE') as visible,cuota
		FROM terms $Where";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	public function getCategorias(){
		$sSQL ="
		SELECT id, description
		FROM category";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		if($res){
			$vData= array();
			while($row = mysqli_fetch_assoc($res)){
				$vData[]=$row;
			}
			return $vData;
		}
	}
	
	
	public function getSubCategorias($q){
		$sSQL ="
		SELECT id, description
		FROM subcategory WHERE category=$q";

			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		$i=0;
		if($res){
			while($row = mysqli_fetch_assoc($res)){
				$oData[$i]['id'] = $row['id'];
				$oData[$i]['description'] = $row['description'];
				
				$i++;
			}
			return $oData;
		}
		else{
			return null;
		}
	}
	
	
			//buscamos niveles por id
	public function editConceptById($iId,$val){
		try{
			//$oData = new StdClass();
			$valor=floatval($val);
			$newDate=date("Y-m-d"); 
			
			$sSQL ="UPDATE adv SET  
			value=".$valor.",
			dateUpdate='".$newDate."'
			WHERE id = $iId";
			
			

//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			//$row = mysqli_fetch_assoc($res);
			if($res){
				//$oData = $row;
				$oData['queryStatus'] = "OK";
				$oData['dateUpdate'] = $newDate;
				
				//var_dump($oData);exit;
			}else{
				
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			//echo 'error';
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
}	
?>