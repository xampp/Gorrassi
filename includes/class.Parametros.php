<?php
class parametros{
	
	private $cnx;

	public function __construct($cn){
		$this->cnx=$cn;
	}

//sacar datos para la sesi�n	
	
	public function getParametros(){
		$sSQL ="
			SELECT 
				id,
				nombre,
				domicilio,
				telefono,
				cft,tea,web,
				email,
				precios
				
			
			FROM parametros

			WHERE id = 1";
			
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				$row = mysqli_fetch_assoc($res);
				if ($row){
					
					return $vData[]=$row;
				}
			}
	}
	
	public function getConfigSystem(){
	
		$sSQL ="
			SELECT 
				nombre as name,
				domicilio as address,
				telefono as phone,
				email as email,
				web as web,
				logo
				

			FROM parametros ";
		try{
		     
			$res=mysqli_query($this->cnx,$sSQL);
			
			if($res){
				$row = mysqli_fetch_assoc($res);
				if ($row){
				     
					$oData = new stdClass();
					foreach($row as $field => $value){
						$oData->$field = $value;
					}
					return $oData;
				}else{
					throw new Exception("ERR0004");
				}
			}else{
				throw new Exception("ERR0005");
			}
		}catch (Exception $e){
			return $e->getMessage();
		}
	}
	
		public function editParametros($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'nombre'=>$vData->nombre,
			'domicilio'=>$vData->domicilio,
			'telefono'=>$vData->telefono,
			'web'=>$vData->web,
			'email'=>$vData->email,
			'precios'=>$vData->precios
			
			); 
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE parametros
				SET ".$sValueList.
				" WHERE id = 1";

			$res=mysqli_query($this->cnx,$sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
		
}	
?>