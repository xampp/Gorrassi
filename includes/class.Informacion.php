<?php
class informacion{

	private $cnx;
	
	public function __construct($cn){
		$this->cnx=$cn;
	}
	
	//editar Novedad
	public function editInformacion($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'mensaje'=>$vData->mensaje,
			'titulo'=>$vData->titulo,
			'idEdificio'=>(int)$vData->idEdificio); 
			
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE informacion
				SET ".$sValueList.
				" WHERE id = ".$vData->id;

			$res=mysqli_query($this->cnx,$sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	
	//agregar Informacion
		public function newInformacion($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO informacion 
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
						$oData->status = "OK";
				}else{
					$oData->status = "5";
				}
			}catch (Exception $e){
				$oData->status = "5";
			}
			
		
		return $oData;
	}

	
	
	
	
	
		public function deleteInformacionById($iId){
		try{
			$oData = new StdClass();
			$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysqli_query($this->cnx,$sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);
			
			$sSQL ="
				DELETE FROM informacion WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
		
	
	public function searchInformacionById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT * FROM informacion WHERE id = $iId";
				
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	
	
	
	public function getInformaciones($iId){
		$sSQL ="
			SELECT n.id,n.mensaje,e.nombre,n.titulo	
			FROM informacion n INNER JOIN users u ON n.idAdmin=u.id
			INNER JOIN edificios e ON n.idEdificio=e.id
			WHERE u.sha = '".$iId."'
			AND u.status=1 ORDER BY n.fecha
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	
	
}	


?>