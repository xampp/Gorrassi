<?php


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once("email/class.phpmailer.php");
require_once("email/class.smtp.php");
require_once("email/class.exception.php");

class Utils{
	private $numeros =    array("-", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve");
	private $numerosX =   array("-", "un", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve");
	private $numeros100 = array("-", "ciento", "doscientos", "trecientos", "cuatrocientos", "quinientos", "seicientos", "setecientos", "ochocientos", "novecientos");
	private $numeros11 =  array("-", "once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve");
	private $numeros10 =  array("-", "-", "-", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa");
  private $cnx;

	public function __construct($cn){
		$this->cnx=$cn;
	}
	
	public function pageWhitOutBar($cnx,$sPage){
		$res = mysqli_query($cnx,"SELECT count(*) AS total FROM without_bar WHERE `page` = '$sPage'");
		if($res){
			$row = mysqli_fetch_assoc($res);
			if((int)$row['total'] >= 1){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
									
public function sendEmail($vDest,$sTitle,$sMessage){
			

	$mail = new PHPMailer();
	try {

    //Server settings
    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'mail.pascualgorrassi.com.ar';          //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username = 'ventas@pascualgorrassi.com.ar';          //SMTP username
    $mail->Password   = 'O4}M8Bq%R^Q$';                         //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom('ventas@pascualgorrassi.com.ar');
		$mail->FromName = "Pascual Gorrassi";
    
    //Attachments
	  $mail->AddEmbeddedImage('img/logoL.png','logo','','base64',"image/png");	
    
    //Content
		$mail->IsHTML(true);
		$mail->Subject = $sTitle;
		$mail->AddAddress($vDest);
   

		/*
		$mail->SMTPSecure = 'tls';
		$mail->Host = "smtp.gmail.com";
		$mail->Port = "587"; 
		$mail->SMTPDebug = 0;
		$mail->SMTPAuth = true;
		$mail->Username = "ventas@pascualgorrassi.com.ar";
		$mail->Password = "VentasPG1756"; // agregar /
		$mail->From = "ventas@pascualgorrassi.com.ar";
		*/
		/*
		  $mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPSecure = 'ssl';
			$mail->Host = "pascualgorrassi.com.ar";
			$mail->Port = "465"; 
			$mail->SMTPDebug = 1;
			$mail->SMTPAuth = true;
			$mail->Username = "pedido_online@pascualgorrassi.com.ar";
			$mail->Password = "DT%1PfGjX@sl";
			$mail->From = "pedido_online@pascualgorrassi.com.ar";
		*/
			
	                
	  $body = "<html>
		         <body>
			        <table border='0' width='100%' cellpadding='15'>
						  <tr>
							  <td bgcolor='#fafafa;'><img src='cid:logo'></td>
						  </tr>
						  <tr>
							<td bgcolor='#d8d8d8'>";
		
		$body .= $sMessage;

		$body .= "<br>
				     </td>
					    </tr>
					    <td bgcolor='#fafafa;'><small><font color='#ffffff'>Pascual Gorrassi<br>
						    www.pascualgorrassi.com.ar</font></small></font> </td>
					    </tr>
					    </table>
					  </body>
					  </html>";

		$mail->Body = $body;
	 	//$mail->Send();
		if(!$mail->Send()) {
	 	  //var_dump($mail);//
	 	  //echo($mail->ErrorInfo);
			return $mail->ErrorInfo;
		} else {
	 	  //var_dump($mail);
		  //echo 'Message has been sent';
		  return true;
	  }

	} catch (Exception $e) {
	    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}
	
	public function getRetrievePasswordMessage($tpl,$sURLToOpenMail,$sCustomerName,$sUserName,$sUserPassword){
		$tpl->load_file("mail/retrievePassword.html","main");
		$tpl->set_var("sURLToOpenMail",$sURLToOpenMail);
		$tpl->set_var("sCustomerName",$sCustomerName);
		$tpl->set_var("sUserName",$sUserName);
		$tpl->set_var("sUserPassword",$sUserPassword);
		$tpl->parse("main",false);
		return $tpl->getParsedVar("main");
	}
	
	public function sumaDia($fecha,$dia){ //Formato dd/mm/AAAA
		list($day,$mon,$year) = explode('/',$fecha);
		return date('Y-m-d',mktime(0,0,0,$mon,$day+$dia,$year));
	} 

	public function checkData($oData){
		foreach($oData as $sField => $sValue){
			
			if(is_null($sValue) || (string)$sValue == ""){
				return false;
				exit;
			}
		}
		return true;
	} 
	
	
	public function validarCampos($oData,$rePass,$user){
		$oResult = new stdClass();
		if ($oData->password === $rePass) {
			$nameUser = $user->checkUser($oData->userName,$oData->email);
			if ($nameUser === FALSE){
				if (!filter_var($oData->email, FILTER_VALIDATE_EMAIL)) {
					$oResult->Status = "Invalid email format"; 
				} else {
					$oResult->Status = "OK";
				}
			} else {
				$oResult->Status = 'Nombre de Usuario o Email ya en uso';
			}
		} else {
			$oResult->Status = 'Las contrase&ntilde;as no coinciden';
		}
		return $oResult;
	} 
	
	
	public function getUserTypes($tpl,$user,$userType,$sPrefix){
		$vData = $user->getUserTypes();
		foreach($vData as $Item){
			if($userType == $Item->typeID){
				$tpl->set_var("sSelected","selected='selected'");
			}else{
				$tpl->set_var("sSelected","");		
			}
			$tpl->set_var("sUserType",$Item->typeName);
			$tpl->set_var("userTypeID",$Item->typeID);

			$tpl->set_var("sNewUserType",$Item->typeName);
			$tpl->set_var("newUserTypeID",$Item->typeID);

			$tpl->set_var("sEditUserType",$Item->typeName);
			$tpl->set_var("editUserTypeID",$Item->typeID);

			$tpl->parse("UserTypeBlock",true);
			$tpl->parse("NewUserTypeBlock",true);
			$tpl->parse("EditUserTypeBlock",true);
		}
	}
	
		public function getUserTypeDocument($tpl,$user){
		$vData = $user->getUserTypeDocument();
		foreach($vData as $Item){
			
			//$tpl->set_var("sUserTypeDocument",$Item->typeName);
			//$tpl->set_var("userTypeDocumentID",$Item->typeID);

			$tpl->set_var("sNewUserTypeDocument",$Item->typeName);
			$tpl->set_var("newUserTypeDocumentID",$Item->typeID);

			$tpl->set_var("sEditUserTypeDocument",$Item->typeName);
			$tpl->set_var("editUserTypeDocumentID",$Item->typeID);

			//$tpl->parse("UserTypeDocumentBlock",true);
			$tpl->parse("NewUserTypeDocumentBlock",true);
			$tpl->parse("EditUserTypeDocumentBlock",true);
		}
	}
	
	public function getUserStatus($tpl,$user){
		$vData = $user->getUserStatus();
		foreach($vData as $Item){
			if($Item->statusID != 1){ //No se muestra el estado eliminado en el alta
				$tpl->set_var("sNewUserStatus",$Item->statusName);
				$tpl->set_var("newUserStatusID",$Item->statusID);
				$tpl->parse("NewUserStatusBlock",true);

				$tpl->set_var("sEditUserStatus",$Item->statusName);
				$tpl->set_var("editUserStatusID",$Item->statusID);
				$tpl->parse("EditUserStatusBlock",true);
			}/*else{
				$tpl->set_var("sEditUserStatus",$Item->statusName);
				$tpl->set_var("editUserStatusID",$Item->statusID);
				$tpl->parse("EditUserStatusBlock",true);
			}*/
		}
	}

	public function getUserCategories($tpl,$user){
		$vData = $user->getUserCategories();
		foreach($vData as $Item){
			$tpl->set_var("sNewUserCategory",$Item->categoryName);
			$tpl->set_var("newUserCategoryID",$Item->categoryID);
			$tpl->parse("NewUserCategoryBlock",true);

			$tpl->set_var("sEditUserCategory",$Item->categoryName);
			$tpl->set_var("editUserCategoryID",$Item->categoryID);
			$tpl->parse("EditUserCategoryBlock",true);
		}
	}

	public function getCondominiusTypes($tpl,$condominium){
		$vData = $condominium->getCondominiumTypes();
		foreach($vData as $Item){
			$tpl->set_var("sCondominiumType",$Item->condominiumName);
			$tpl->set_var("condominiumTypeID",$Item->condominiumID);
			$tpl->parse("CondominiumTypeBlock",true);

			$tpl->set_var("sEditCondominiumType",$Item->condominiumName);
			$tpl->set_var("editCondominiumTypeID",$Item->condominiumID);
			$tpl->parse("EditCondominiumTypeBlock",true);
		}
	}
	public function getCondominiumDataTypes($tpl,$condominium){
		$vData = $condominium->getCondominiumDataTypes();
		foreach($vData as $Item){
			$tpl->set_var("sCondominiumData",$Item->condominiumDataName);
			$tpl->set_var("condominiumDataID",$Item->condominiumDataID);
			$tpl->parse("CondominiumDataBlock",true);

			$tpl->set_var("sEditCondominiumData",$Item->condominiumDataName);
			$tpl->set_var("editCondominiumDataID",$Item->condominiumDataID);
			$tpl->parse("EditCondominiumDataBlock",true);
		}
	}
	public function getCondominiumStorages($tpl,$condominium){
		$vData = $condominium->getCondominiumStorages();
		foreach($vData as $Item){
			$tpl->set_var("sCondominiumStorage",$Item->condominiumStorageName);
			$tpl->set_var("condominiumStorageID",$Item->condominiumStorageID);
			$tpl->parse("CondominiumStorageBlock",true);

			$tpl->set_var("sEditCondominiumStorage",$Item->condominiumStorageName);
			$tpl->set_var("editCondominiumStorageID",$Item->condominiumStorageID);
			$tpl->parse("EditCondominiumStorageBlock",true);
		}
	}
	public function getProvinces($tpl,$db){
		$vData = $db->getProvinces();
		foreach($vData as $Item){
			$tpl->set_var("sNewUserProvince",$Item->name);
			$tpl->set_var("newUserProvinceID",$Item->id);
			$tpl->parse("NewUserProvinceBlock",true);

			$tpl->set_var("sEditUserProvince",$Item->name);
			$tpl->set_var("editUserProvinceID",$Item->id);
			$tpl->parse("EditUserProvinceBlock",true);
		}
	}
	
	public function getMeterStatus($tpl,$meter,$iMeterStatus){
		$vData = $meter->getMeterStatus();
		foreach($vData as $Item){
/*			if($iMeterStatus == (int)$Item['id']){
				$tpl->set_var("sEditMeterStatusSelected","selected='selected'");
			}else{
				$tpl->set_var("sEditMeterStatusSelected","");		
			}
*/
			$tpl->set_var("sEditMeterStatus",$Item['description']);
			$tpl->set_var("iEditMeterStatusId",$Item['id']);
			$tpl->parse("EditMeterStatusBlock",true);

			if($Item['id'] != 5 && $Item['id'] != 6){
				$tpl->set_var("sNewMeterStatus",$Item['description']);
				$tpl->set_var("iNewMeterStatusId",$Item['id']);
				$tpl->parse("NewMeterStatusBlock",true);
			}
		}
	}

	public function getPropertyTypes($tpl,$property){
		$vData = $property->getPropertyTypes();
		foreach($vData as $Item){
			$tpl->set_var("sEditMeterPropertyType",$Item['description']);
			$tpl->set_var("iEditMeterPropertyTypeId",$Item['id']);
			$tpl->set_var("sNewMeterPropertyType",$Item['description']);
			$tpl->set_var("iNewMeterPropertyTypeId",$Item['id']);

			$tpl->parse("EditMeterPropertyTypeBlock",true);
			$tpl->parse("NewMeterPropertyTypeBlock",true);
		}
	}

	public function getPropertyRates($tpl,$property,$edit=true){
		$vData = $property->getPropertyRates();
		foreach($vData as $Item){
		    if ($edit) {
					$tpl->set_var("sEditMeterRateType",$Item['description']);
					$tpl->set_var("iEditMeterRateTypeId",$Item['id']);
			}
			
			$tpl->set_var("sNewMeterRateType",$Item['description']);
			$tpl->set_var("iNewMeterRateTypeId",$Item['id']);
			
              if ($edit) {
							$tpl->parse("EditMeterRateTypeBlock",true);
			  }         
			
			$tpl->parse("NewMeterRateTypeBlock",true);
		}
	}

	public function getPropertyImage($property,$iPropertyId){
		$vData = $property->getImage($iPropertyId);
		$tpl->set_var("sEditMeterPropertyImage",$Item['id']);

	}
	
	public function getDocumentation($tpl,$user) {
	$vData = $user->getDocumentationTypes();
		foreach($vData as $Item){
			$tpl->set_var("sDocDescription",$Item->description);
			$tpl->set_var("iDocId",$Item->id);
			$tpl->parse ("EmptyDocTypeBlock",true);
			$tpl->set_var("sEditDocDescription",$Item->description);
			$tpl->set_var("iEditDocId",$Item->id);
			$tpl->parse ("EditEmptyDocTypeBlock",true);
		}
	}
	
	
		public function getMeterUserCategories($tpl,$user){
		$vData = $user->getUserCategories();
		foreach($vData as $Item){
			$tpl->set_var("sNewMeterCategory",$Item->categoryName);
			$tpl->set_var("newMeterCategoryID",$Item->categoryID);
			$tpl->parse("NewMeterCategoryBlock",true);

			$tpl->set_var("sEditMeterCategory",$Item->categoryName);
			$tpl->set_var("editMeterCategoryID",$Item->categoryID);
			$tpl->parse("EditMeterCategoryBlock",true);
		}
	}
	
	
		public function getMeterUserTypeDocument($tpl,$user){
		$vData = $user->getUserTypeDocument();
		foreach($vData as $Item){
			$tpl->set_var("sNewMeterTypeDocument",$Item->typeName);
			$tpl->set_var("newMeterTypeDocumentID",$Item->typeID);
			$tpl->parse("NewMeterTypeDocumentBlock",true);

			$tpl->set_var("sEditMeterTypeDocument",$Item->typeName);
			$tpl->set_var("editMeterTypeDocumentID",$Item->typeID);
			$tpl->parse("EditMeterTypeDocumentBlock",true);
		}
	}

	public function showUserSearchForm($db,$user,$condominium,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $iUserType, $vPOST){
		// USERS TYPES
		$this->getUserTypes($tpl,$user,$iUserType,null);
		// TYPE DOCUMENT
		$this->getUserTypeDocument($tpl,$user);
		// USERS STATUS
		$this->getUserStatus($tpl,$user);
		// USERS CATEGORIES
		$this->getUserCategories($tpl,$user);
		// CONDOMINIUMS TYPES
		$this->getCondominiusTypes($tpl,$condominium);
		// CONDOMINIUMS DATA TYPES
		$this->getCondominiumDataTypes($tpl,$condominium);
		// CONDOMINIUMS DATA STORAGE
		$this->getCondominiumStorages($tpl,$condominium);
		// PROVINCES
		$this->getProvinces($tpl,$db);
		// DOCUMENTACION
		$this->getDocumentation($tpl,$user);

		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("dUserDischarge",date("d/m/Y"));
		$tpl->set_var("userId",$vPOST['userID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("iCUIT",$vPOST['userCUIT']);
		$tpl->set_var("userName",$vPOST['userName']);
		$tpl->set_var("userLastName",$vPOST['userLastName']);
		$tpl->set_var("sUserAddress",$vPOST['userAddress']);
		$tpl->set_var("sUserCity",$vPOST['userCity']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	
	
		public function showUserAdminSearchForm($db, $user,$tpl, $iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST){
		// USERS TYPES
		//$this->getUserTypes($tpl,$user,$iUserType,null);
		// USERS STATUS
		//$this->getUserStatus($tpl,$user);
        
		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("userAdminId",$vPOST['userAdminID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("userAdminName",$vPOST['userAdminName']);
		$tpl->set_var("userAdminLastName",$vPOST['userAdminLastName']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	
	
	
	public function showConceptSearchForm($db,$concept,$property,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $iConceptType,$iConceptOrder, $vPOST,$iConceptSalePoint){

		// CONCEPT TYPES
		$this->getConceptTypes($tpl,$concept,$iConceptType,null);
		// CONCEPT SALEPOINT
		$this->getConceptSalePoint($tpl,$concept,$iConceptSalePoint,null);
        // CONCEPT RATES
		$this->getConceptRates($tpl,$property);


		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("dConceptDischarge",date("d/m/Y"));
		$tpl->set_var("conceptId",$vPOST['conceptID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("conceptName",$vPOST['conceptName']);
		
		if(!is_null($vPOST['conceptOrder'])&& $vPOST['conceptOrder']!="" ){
		              if ($vPOST['conceptOrder']==1) {
				$tpl->set_var("sSelectedOrder1","selected='selected'");
				} else {
				$tpl->set_var("sSelectedOrder2","selected='selected'");
				}
			}else{
				$tpl->set_var("sSelectedOrder0","selected='selected'");		
			}
			
		
		
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	
	
	public function showPropertySearchForm($db,$property,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage,$vPOST){


		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("Id",$vPOST['ID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("Name",$vPOST['Name']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}


	public function showSearchForm($tpl,$pay,$payType,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST,$billYear,$bill){

		// FORMAS DE PAGO
		$this->getSummaryPaymentTypes($tpl,$pay,$payType,null);
	// BILL YEARS
		$this->getBillYear($tpl,$bill,$billYear,null);
		

		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	


		public function getSummaryPaymentTypes($tpl,$pay){
		$vData = $pay->getMethodPayment();
		foreach($vData as $Item){
		    $tpl->set_var("sNewSummaryPayment",$Item['description']);
			$tpl->set_var("newSummaryPaymentID",$Item['id']);
			

		$tpl->parse("NewSummaryPaymentBlock",true);
		}
	}

	
	public function getBillYear($tpl,$bill){
		$vData = $bill->getBillYear();
		foreach($vData as $Item){
		    $tpl->set_var("sBillYear",$Item['year']);
		$tpl->parse("BillYearBlock",true);
		}
	}
	
	
	
		public function showMethodPaymentSearchForm($db,$pay,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage,$vPOST){


		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("Id",$vPOST['ID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("Name",$vPOST['Name']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}
	
	public function showCategoriesSearchForm($db,$property,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage,$vPOST){


		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("Id",$vPOST['ID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("Name",$vPOST['Name']);
		$tpl->set_var("Percent",$vPOST['percent']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	

	
	public function showConfigSystem($db,$configSystem,$tpl){
//para cargar las provincias
		// PROVINCES
		$vData = $db->getProvinces();
		$oData = $configSystem->getConfigSystem();
		
// para ver la provincia que tiene cargada
		foreach($vData as $Item){
		    $tpl->set_var("configSystemProvinceID",$Item->id);
			$tpl->set_var("configSystemProvince",$Item->name);
			if ($oData->province == $Item->id) {
				$tpl->set_var("systemProvinceSelected","selected='selected'");
				//para la ciudad
				// Ciudad
				$vData1 = $db->getCitiesByProvince($oData->province);
				$oData1 = $configSystem->getConfigSystem();
			}else{
				$tpl->set_var("systemProvinceSelected","");
			}
			$tpl->parse("ConfigSystemProvinceBlock",true);
		}	
// para ver la ciudad que tiene cargada
		foreach($vData1 as $Item1){
		    $tpl->set_var("configSystemCityID",$Item1->id);
			$tpl->set_var("configSystemCity",$Item1->name);
			if ($oData1->city == $Item1->id) {
				$tpl->set_var("systemCitySelected","selected='selected'");
			}else{
				$tpl->set_var("systemCitySelected","");
			}
			$tpl->parse("ConfigSystemCityBlock",true);
		}	
		
		
	    $tpl->set_var("configSystemName",$oData->name);
		$tpl->set_var("configSystemCuit",$oData->cuit);
		$tpl->set_var("configSystemAddress",$oData->address);
		$tpl->set_var("configSystemZip",$oData->zip);
		$tpl->set_var("configSystemLocation",$oData->location);
		$tpl->set_var("configSystemEmail",$oData->email);
		$tpl->set_var("configSystemWeb",$oData->web);
		$tpl->set_var("configSystemPointSaleWater",$oData->pointSaleWater);
		$tpl->set_var("configSystemPointSaleRent",$oData->pointSaleRent);
		$tpl->set_var("configSystemPointSalePark",$oData->pointSalePark);
		$tpl->set_var("configSystemPointSaleConection",$oData->pointSaleConection);
        $tpl->set_var("configSystemPublic",$oData->public_opening);
        $tpl->set_var("configSystemPhone",$oData->phone);
        $tpl->set_var("configSystemFirstDay",$oData->firstday);
        $tpl->set_var("configSystemSecondDay",$oData->secondday);
		
		$tpl->set_var("configSystemSeat",$oData->seat);
		$tpl->set_var("configSystemInvoice",$oData->invoice);
		$tpl->set_var("configSystemInterests",$oData->interests);
		
		$tpl->set_var("configSystemM3",$oData->m3);
		$tpl->set_var("configSystemM31",$oData->m31);
		$tpl->set_var("configSystemM32",$oData->m32);
		$tpl->set_var("configSystemM33",$oData->m33);
	   $tpl->set_var("configSystemNumberWaterA",$oData->numberWaterA);
	   $tpl->set_var("configSystemNumberWaterB",$oData->numberWaterB);
	   $tpl->set_var("configSystemNumberNoteWaterA",$oData->numberNoteWaterA);
	   $tpl->set_var("configSystemNumberNoteWaterB",$oData->numberNoteWaterB);
	   $tpl->set_var("configSystemNumberDebitWaterA",$oData->numberDebitWaterA);
	   $tpl->set_var("configSystemNumberDebitWaterB",$oData->numberDebitWaterB);
	   
	    $tpl->set_var("configSystemNumberConectionA",$oData->numberConectionA);
	   $tpl->set_var("configSystemNumberConectionB",$oData->numberConectionB);
	   $tpl->set_var("configSystemNumberNoteConectionA",$oData->numberNoteConectionA);
	   $tpl->set_var("configSystemNumberNoteConectionB",$oData->numberNoteConectionB);
	   $tpl->set_var("configSystemNumberDebitConectionA",$oData->numberDebitConectionA);
	   $tpl->set_var("configSystemNumberDebitConectionB",$oData->numberDebitConectionB);
	   
	    $tpl->set_var("configSystemNumberParkA",$oData->numberParkA);
	   $tpl->set_var("configSystemNumberParkB",$oData->numberParkB);
	   $tpl->set_var("configSystemNumberNoteParkA",$oData->numberNoteParkA);
	   $tpl->set_var("configSystemNumberNoteParkB",$oData->numberNoteParkB);
	   $tpl->set_var("configSystemNumberDebitParkA",$oData->numberDebitParkA);
	   $tpl->set_var("configSystemNumberDebitParkB",$oData->numberDebitParkB);
	
	    $tpl->set_var("configSystemNumberRentA",$oData->numberRentA);
	   $tpl->set_var("configSystemNumberRentB",$oData->numberRentB);
	   $tpl->set_var("configSystemNumberNoteRentA",$oData->numberNoteRentA);
	   $tpl->set_var("configSystemNumberNoteRentB",$oData->numberNoteRentB);
	   $tpl->set_var("configSystemNumberDebitRentA",$oData->numberDebitRentA);
	   $tpl->set_var("configSystemNumberDebitRentB",$oData->numberDebitRentB);
	   
	}

public function getConceptTypes($tpl,$concept,$conceptType,$sPrefix){
		$vData = $concept->getConceptTypes();
		foreach($vData as $Item){
			if($conceptType == $Item->typeID){
				$tpl->set_var("sSelected","selected='selected'");
			}else{
				$tpl->set_var("sSelected","");		
			}
			$tpl->set_var("sConceptType",$Item->typeName);
			$tpl->set_var("conceptTypeID",$Item->typeID);

			$tpl->set_var("sNewConceptType",$Item->typeName);
			$tpl->set_var("newConceptTypeID",$Item->typeID);

			$tpl->set_var("sEditConceptType",$Item->typeName);
			$tpl->set_var("editConceptTypeID",$Item->typeID);

			$tpl->parse("ConceptTypeBlock",true);
			$tpl->parse("NewConceptTypeBlock",true);
			$tpl->parse("EditConceptTypeBlock",true);
		}
	}
	
	public function getConceptSalePoint($tpl,$concept,$conceptSalePoint,$sPrefix){
		$vData = $concept->getSalePoint();
		
		foreach($vData as $Item){
			if($conceptSalePoint == $Item->SalePoint){
				$tpl->set_var("sSelected","selected='selected'");
			}else{
				$tpl->set_var("sSelected","");		
			}
			
			//$tpl->set_var("sConceptTypeName",$Item->typeName);
			//$tpl->set_var("conceptSalePoint",$Item->SalePoint);

			$tpl->set_var("sNewConceptSalePointName",utf8_decode($Item->typeName));
			$tpl->set_var("iNewConceptSalePoint",$Item->SalePoint);

			$tpl->set_var("sEditConceptSalePointName",utf8_decode($Item->typeName));
			$tpl->set_var("iEditConceptSalePoint",$Item->SalePoint);

			//$tpl->parse("ConceptSalePointBlock",true);
			$tpl->parse("NewConceptSalePointBlock",true);
			$tpl->parse("EditConceptSalePointBlock",true);
		}
	}
	
	
	

	public function getConceptByType($tpl,$concept,$conceptType,$sPrefix){
		$vData = $concept->getConceptByType($conceptType);
		foreach($vData as $Item){
			$tpl->set_var("sNewMovementsConcept",$Item->typeName);
			$tpl->set_var("iNewMovementsConceptId",$Item->typeID);
			
			$tpl->parse("NewMovementsConceptBlock",true);
		}
	}
	


public function getConceptRates($tpl,$property){
		$vData = $property->getPropertyRates();
		foreach($vData as $Item){
			$tpl->set_var("sNewConceptPropertyType",$Item['description']);
			$tpl->set_var("iNewConceptPropertyTypeId",$Item['id']);
			
			$tpl->parse("NewConceptPropertyTypeBlock",true);

            $tpl->set_var("sEditConceptPropertyType",$Item['description']);
			$tpl->set_var("iEditConceptPropertyTypeId",$Item['id']);
			
			$tpl->parse("EditConceptPropertyTypeBlock",true);
		}
	}
	
	
public function getConceptStatus($tpl,$user){
		$vData = $concept->getConceptStatus();
		foreach($vData as $Item){
			if($Item->statusID != 1){ //No se muestra el estado eliminado en el alta
				$tpl->set_var("sNewConceptStatus",$Item->statusName);
				$tpl->set_var("newConceptStatusID",$Item->statusID);
				$tpl->parse("NewConceptStatusBlock",true);

				$tpl->set_var("sEditConceptStatus",$Item->statusName);
				$tpl->set_var("editConceptStatusID",$Item->statusID);
				$tpl->parse("EditConceptStatusBlock",true);
			}/*else{
				$tpl->set_var("sEditConceptStatus",$Item->statusName);
				$tpl->set_var("editConceptStatusID",$Item->statusID);
				$tpl->parse("EditConceptStatusBlock",true);
			}*/
		}
	}

public function showMovementsSearchForm($tpl,$meter,$property,$iPropertyId,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST){
		
		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("iUserID",$vPOST['iUserID']);
		$tpl->set_var("sMeterSerial",$vPOST['meterSerial']);
		$tpl->set_var("sMeterUser",$vPOST['meterUser']);
		$tpl->set_var("sMeterApple",$vPOST['meterApple']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}




	public function showMeterSearchForm($db,$tpl,$meter,$property,$user,$iPropertyId,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST){
		// METER STATUS
		$this->getMeterStatus($tpl,$meter,$iUserType,null);
		// PROPERTY TYPES
		$this->getPropertyTypes($tpl,$property);
		// USERS CATEGORY
		$this->getMeterUserCategories($tpl,$user);
		// USERS TYPE DOCUMENT
		$this->getMeterUserTypeDocument($tpl,$user);
		// PROPERTY RATES
		$this->getPropertyRates($tpl,$property);
		// PROVINCES
		$this->getProvinces($tpl,$db);
		// IMAGE
		if($iPropertyId && $iPropertyId!=0){
			$this->getPropertyImage($property,$iPropertyId);
		}
		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("iUserID",$vPOST['iUserID']);
		$tpl->set_var("sMeterSerial",$vPOST['meterSerial']);
		$tpl->set_var("sMeterUser",$vPOST['meterUser']);
		$tpl->set_var("sMeterApple",$vPOST['meterApple']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}

	public function showMeasureSearchForm($tpl,$meter,$property,$iPropertyId,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST){
		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}

	public function showMonthsBilling($tpl,$bill,$vPOST){
	
	
	}
	
	
	
	public function showMovementsNewForm($tpl,$concept,$vPOST){
	    
		// CONCEPT 
		$this->getConceptByType($tpl,$concept,1,null);
		// CONCEPT 	
	}
 
    public function tresnumeros($n, $last) {
		//global $numeros100, $numeros10, $numeros11, $numeros, $numerosX;
		if ($n == 100) return "cien ";
		if ($n == 0) return "cero ";
		$r = "";
		$cen = floor($n / 100);
		$dec = floor(($n % 100) / 10);
		$uni = $n % 10;
		if ($cen > 0) $r .= $this->numeros100[$cen] . " ";

		switch ($dec) {
			case 0: $special = 0; break;
			case 1: $special = 10; break;
			case 2: $special = 20; break;
			default: $r .= $this->numeros10[$dec] . " "; $special = 30; break;
		}
		if ($uni == 0) {
			if ($special==30);
			else if ($special==20) $r .= "veinte ";
			else if ($special==10) $r .= "diez ";
			else if ($special==0);
		} else {
			if ($special == 30 && !$last) $r .= "y " . $this->numerosX[$n%10] . " ";
			else if ($special == 30) $r .= "y " . $this->numeros[$n%10] . " ";
			else if ($special == 20) {
				if ($uni == 3) $r .= "veintitr�s ";
				else if (!$last) $r .= "veinti" . $this->numerosX[$n%10] . " ";
				else $r .= "veinti" . $this->numeros[$n%10] . " ";
			} else if ($special == 10) $r .= $this->numeros11[$n%10] . " ";
			else if ($special == 0 && !$last) $r .= $this->numerosX[$n%10] . " ";
			else if ($special == 0) $r .= $this->numeros[$n%10] . " ";
		}
		return $r;
	}
 
    public function seisnumeros($n, $last) {
		if ($n == 0) return "cero ";
		$miles = floor($n / 1000);
		$units = $n % 1000;
		$r = "";
		if ($miles == 1) $r .= "mil ";
		else if ($miles > 1) $r .= $this->tresnumeros($miles, false) . "mil ";
		if ($units > 0) $r .= $this->tresnumeros($units, $last);
		return $r;
	}
 
    public function docenumeros($n) {
		$nTmp = explode(".",$n);
		$n=(float)$n;
		if ($n == 0) return "cero ";
		$millo = floor($n / 1000000);
		$units = $n % 1000000;
		$r = "";
		if ($millo == 1) $r .= "un mill�n ";
		else if ($millo > 1) $r .= $this->seisnumeros($millo, false) . "millones ";
		if ($units > 0) $r .= $this->seisnumeros($units, true);
		if ($nTmp[1]) $r .= " con " . $this->seisnumeros((int)$nTmp[1], true) . " centavos";
		return $r;
    }
	
	
	
		public function encrypt($string, $key) {
   $result = '';
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)+ord($keychar));
      $result.=$char;
   }
   return base64_encode($result);
}
		
public function decrypt($string, $key) {
   $result = '';
   $string = base64_decode($string);
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)-ord($keychar));
      $result.=$char;
   }
   return $result;
}
	
	
	
	
	
	
}
?>