<?php
class DB{
	
	private $cnx;

	public function __construct($cn){
    $this->cnx=$cn;
	}
	
	public function getLabel($sLabel, $sLang){
		$sSQL ="SELECT * FROM dictionary WHERE label = '$sLabel' AND lang = '$sLang'";
		try{
			$res = mysqli_query($this->cnx,$sSQL);
			if($res){
				$row = mysqli_fetch_assoc($res);
			}else{
				throw new Exception("ERR0001");
			}
			return $row['description'];
		}catch (Exception $e){
			return $e->getMessage();
		}
	}
	
	public function getGeoData($sZip){
		if($sZip){
			$sSQL ="SELECT c.name AS cityName, p.name AS provinceName 
					FROM cities c
					INNER JOIN provinces p ON c.id_province = p.id
					WHERE zip = $sZip";
				//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = new stdClass();
				$oData->city = $row['cityName'];
				$oData->province = $row['provinceName'];
				return $oData;	
			}else{
				return null;
			}
		}
	}
	
	public function getCities(){
		$sSQL ="SELECT c.name, c.id, c.id_province, c.zip
				FROM cities c ";
			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		
		$row = mysqli_fetch_array($res);
		if($row){
			while ($row = mysqli_fetch_array($res)){
				$oData = new stdClass();
				$oData->id = $row['id'];
				$oData->name = $row['name'];
				$oData->idProvince = $row['id_province'];
				$oData->zip = $row['zip'];
				$vData[]=$oData;
			}
			return $vData;			
		}else{
			return null;
		}
	}

	public function getCitiesByProvince($iId_province){
		$sSQL ="SELECT c.name, c.id, c.id_province, c.zip
				FROM cities c 
				WHERE c.id_province = $iId_province
				ORDER BY c.name";
			//var_dump($sSQL);exit;
		$res=mysqli_query($this->cnx,$sSQL);
		
		if($res){
			$i=0;
			while ($row = mysqli_fetch_assoc($res)){
				//$oData = array();
				$oData[$i]['id'] = $row['id'];
				$oData[$i]['name'] = $row['name'];
				$oData[$i]['idProvince'] = $row['id_province'];
				$oData[$i]['zip'] = $row['zip'];
				$i++;
			}
			return $oData;			
		}else{
			return null;
		}
	}
	
	public function getProvinces(){
		$sSQL ="SELECT * FROM provinces";
		$res=mysqli_query($this->cnx,$sSQL);
		$vData= array();
		while ($row = mysqli_fetch_array($res)){
			$oData = new stdClass();
			$oData->id = $row['id'];
			$oData->name = $row['name'];
			$vData[]=$oData;
		}
		return $vData;			
	}
	
	public function getProvincesbyId($iId){
		$sSQL ="SELECT `name` FROM provinces WHERE id = $iId";
		$res=mysqli_query($this->cnx,$sSQL);
		$row=mysqli_fetch_assoc($res);
		
		return $row['name'];
	}
	
	public function getCitiesbyId($iId){
		$sSQL ="SELECT `name` FROM cities WHERE id = $iId";
		$res=mysqli_query($this->cnx,$sSQL);
		$row=mysqli_fetch_assoc($res);
		
		return $row['name'];
	}
	
	
}
?>