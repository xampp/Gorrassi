<?php
Class Archivo{
	function __construct(){}
	
	public function save($sTempName, $sName){
		$vTemp = explode(".",$sName);
		$sName = date("YmdHis").".".$vTemp[count($vTemp)-1];
		if(move_uploaded_file($sTempName,'img_prod/slider/'.$sName)){
			return $sName;
		}else{
			return false;
		}
	}
	
	
	public function saveA($sTempName, $sName){
		$vTemp = explode(".",$sName);
		$sName = date("YmdHis").".".$vTemp[count($vTemp)-1];
		if(move_uploaded_file($sTempName,'temp/'.$sName)){
			return $sName;
		}else{
			return false;
		}
	}
	
	public function savePro($sTempName, $sName,$num){
		$vTemp = explode(".",$sName);
		$sName = date("YmdHis").$num.".".$vTemp[count($vTemp)-1];
		if(move_uploaded_file($sTempName,'img_prod/'.$sName)){
			return $sName;
		}else{
			return false;
		}
	}
	
}
?>