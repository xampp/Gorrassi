<?php
class Customer{
	public $permisos;
	private $cnx;
	
	public function __construct($cn){
		$this->cnx=$cn;
	}
	
//que permisos tiene el usuario y que usuario es
	
	public function can ($iPermission,$iId){
		$sSQL ="
			SELECT sysUsersType		
			FROM users 
			WHERE sha = '".$iId."'
			AND status=1 LIMIT 1";
              
			//var_dump($sSQL);exit;  
			  
			$res=mysqli_query($this->cnx,$sSQL);

			//probar si funciona bien
			if($res){
				$row = mysqli_fetch_assoc($res);
				
				if ($row){
					   if ($iPermission == $row['sysUsersType']){
						   $usuario=1;
					   } else {
						   $usuario=0;
					   }
					return (bool)($usuario);
				}
			}	
	}
	
	
		public function searchPersonalByName($sName){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT 
						first as name, 
						last as lastName, 
						id, 
						dni as cuit
					FROM customer 
					WHERE (CONCAT_WS(' ',`last`,`first`) LIKE '$sName%') 
					";
				
			//var_dump($sSQL); exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while ($row = mysqli_fetch_array($res)){
					$oData = new stdClass();
					$oData->userID 			= $row['id'];
					$oData->userName 		= $row['name'];
					$oData->userLastName 	= $row['lastName'];
					$oData->userCuit 	= $row['cuit'];
					$vData[]=$oData;
				}
				return $vData;
			}else{
				throw new Exception("ERR0006");
			}
		}catch (Exception $e){
			return $e->getMessage();
		}
		
	}
//sacar datos para la sesi�n	
	
	public function getName(){
		$sSQL ="
			SELECT 
				u.name,
				u.lastName,
				u.userName,
                u.password,
				u.address,
				u.phone,
				u.email_alt,
				u.id
			
			FROM users u

			WHERE sha = '".$_SESSION['sysUser']."' AND u.status=1";
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$row = mysqli_fetch_assoc($res);
				if ($row){
					
					return $vData[]=$row;
				}
			}
	}
	
	
	public function updatePassword($pass){
		$sSQL ="
			UPDATE
			users
			SET password='$pass'
			WHERE sha = '".$_SESSION['sysUser']."' AND status=1";
			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			$oData = new StdClass();
			if($res){
				$oData->status="OK";
			} else {
				$oData->status="ERR0011";
			}
			return $oData;
	}
	
	
		public function checkUser($userName,$userEmail){
		$sSQL ="
			SELECT 
				u.userName			

			FROM users u

			WHERE u.userName = '$userName' or u.email = '$userEmail' AND u.status in (0,1) limit 1";
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$row = mysqli_fetch_assoc($res);
				if ($row){
					return TRUE;
				} else {
					return FALSE;
				}
			}else {
					return FALSE;
				}
	}
	
		public function activation($activation){
		$sSQL ="
			UPDATE users
            SET status = 1
			WHERE codeActivation = '$activation' and status = 0";
			//var_dump($sSQL);exit;
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				    if (mysqli_affected_rows()>0){
						return TRUE;
					} else {
						
						return FALSE;
					}
				
					
			}else {
					return FALSE;
				}
	}
	
//login de los usuarios

	public function Login($sUserName,$sPass){
		$sSQL ="
			SELECT 
				u.id,
				u.name,
				u.lastName,
				u.userName,
				u.password,
				u.sysUsersType
				
			

			FROM users u

			WHERE u.userName = '$sUserName' AND u.password = '$sPass'  AND u.status=1

			LIMIT 1";
		try{
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$row = mysqli_fetch_assoc($res);
				if ($row){
					$_SESSION['sysUser'] = sha1($row['password'].$row['userName']);
					//$_SESSION['agent'] = sha1($_SERV['HTTP_USER_AGENT']); 
					$oData = new stdClass();
					$oData->userId = $row['id'];
					$this->permisos=$row['sysUsersType'];
					$oData->customerName = html_entity_decode($row['lastName']).", ".html_entity_decode($row['name']);
					return $oData;
				}else{
					throw new Exception("ERR0002");
				}
			}else{
				throw new Exception("ERR0003");
			}
		}catch (Exception $e){
			return $this->getErrorMsj($e->getMessage(),"SPA");
		}
		
	}
	
//obtine mensajes de error desde la base de datos	
	public function getErrorMsj($errorCode, $sLang){
		$sSQL ="SELECT * FROM dictionary WHERE label = 'lbl_$errorCode' AND lang = '$sLang'";
		$res = mysqli_query($this->cnx,$sSQL);
		if($res){
			$row = mysqli_fetch_assoc($res);
			if($row){
				return $row['description'];
			}else{
				return false;
			}
		}
	}
	
	
	public function getUserData($iId){
		$sSQL ="
			SELECT 
				u.id,
				u.name,
				u.lastName,
				u.address,
				u.id_city,
				u.id_province,
				u.zipCode,
				u.phone,
				u.cellPhone,
				u.email,
				u.cuit,
				CASE u.type
					WHEN 0 THEN 'Socio'
					WHEN 1 THEN 'No socio'
				END as `type`,
				CASE u.status
					WHEN 0 THEN 'Eliminado'
					WHEN 1 THEN 'Activo'
					WHEN 2 THEN 'Moroso'
				END as `status`,
				DATE_FORMAT(u.dischargeDate,'%d/%m/%Y') AS dischargeDate,
				u.condominiumID,
				u.category

			FROM users u

			WHERE u.id = $iId 

			LIMIT 1";
		try{
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$row = mysqli_fetch_assoc($res);
				if ($row){
					$oData = new stdClass();
					foreach($row as $field => $value){
						$oData->$field = $value;
					}
					return $oData;
				}else{
					throw new Exception("ERR0004");
				}
			}else{
				throw new Exception("ERR0005");
			}
		}catch (Exception $e){
			return $e->getMessage();
		}
	}
	
	public function retrievePassword($sUserName,$bSha1=false){
	
		if($bSha1){
			$sWhere = "u.sha1_usuario = '$sUserName'";
		}else{
			$sWhere = "u.usuario = '$sUserName'";
		}
		$sSQL ="
			SELECT 
				u.usuario,
				u.sha1_usuario,
				u.clave,
				c.razon,
				c.email
				
			FROM usuarios u
			INNER JOIN clientes c ON c.id_cli = u.id_cli
			WHERE $sWhere 
			LIMIT 1";
		
		$res=mysqli_query($this->cnx,$sSQL);
		$row = mysqli_fetch_assoc($res);
		if ($row){
			$oData = new stdClass();
			$oData->userName = $row['usuario'];
			$oData->sha1UserName = $row['sha1_usuario'];
			$oData->pass = $row['clave'];
			$oData->email = $row['email'];
			$oData->customerName = html_entity_decode($row['razon']);
			return $oData;
		}else{
			return null;
		}
		
	}
	
	public function deleteUserById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="
				UPDATE users SET `status` = 0 WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$sSQL1 ="
				UPDATE unidades SET `idUser` = 0 WHERE idUser = $iId";
				$res1=mysqli_query($this->cnx,$sSQL1);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}

	
	
		public function deleteCustomerById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="
				DELETE FROM customer WHERE id = $iId";
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	
	public function searchCustomerById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT * FROM customer WHERE id = $iId";
				
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
			
//garante			
			$sSQL1 ="SELECT * FROM customer WHERE id = (select a.garante from customer a WHERE a.id = $iId)";
			$res1=mysqli_query($this->cnx,$sSQL1);
			if($res1){
				while($row1 = mysqli_fetch_assoc($res1)){
					$vData[]=$row1;
				}
			}
			//bussines garante papa
			$sSQL22 ="SELECT * FROM business WHERE id = (select a.garante from customer a WHERE a.id = $iId)";
			$res22=mysqli_query($this->cnx,$sSQL22);
			if($res22){
				while($row22 = mysqli_fetch_assoc($res22)){
					$vData4[]=$row22;
				}
			}
			
			
			
			//business			
			$sSQL2 ="SELECT * FROM business WHERE idCus=$iId";
			$res2=mysqli_query($this->cnx,$sSQL2);
			if($res2){
				while($row2 = mysqli_fetch_assoc($res2)){
					$vData2[]=$row2;
				}
			}
			//property			
			$sSQL3 ="SELECT * FROM property WHERE idCus=$iId";
			$res3=mysqli_query($this->cnx,$sSQL3);
			if($res3){
				while($row3 = mysqli_fetch_assoc($res3)){
					$vData3[]=$row3;
				}
			}
			
				$oData['queryStatus'] = "OK";
				$oData['charges'] = $vData;
				$oData['busi'] = $vData2;
				$oData['busiGarante'] = $vData4;
				$oData['pro'] = $vData3;
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	
		public function searchUserById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT * FROM users WHERE id = $iId";
				
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}


	public function newCustomer($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO customer 
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysqli_insert_id();
				}else{
					$oData->status = "5";
				}
			}catch (Exception $e){
				$oData->status = "5";
			}
			
		
		return $oData;
	}

	public function newBusiness($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO business 
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysqli_insert_id();
				}else{
					$oData->status = "5";
				}
			}catch (Exception $e){
				$oData->status = "5";
			}
			
		
		return $oData;
	}

		public function newProperty($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO property
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysqli_insert_id();
				}else{
					$oData->status = "5";
				}
			}catch (Exception $e){
				$oData->status = "5";
			}
			
		
		return $oData;
	}

	
	public function getUserStatus(){
		$sSQL ="SELECT * FROM users_status";
		$res=mysqli_query($this->cnx,$sSQL);
		
		while ($row = mysqli_fetch_array($res)){
			$oData = new stdClass();
			$oData->statusID = $row['id'];
			$oData->statusName = $row['description'];
			$vData[]=$oData;
		}
		return $vData;			
	}
	
	
	public function getCustomers(){
		$sSQL ="
			SELECT c.id, c.first as nombre,c.last as apellido,c.dni,c.domicilio,c.tel as telefono, c.cel as celular	
			FROM customer c 
			ORDER BY c.last
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
        $vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}

	
	public function getEdificiosUnidadUser($iId){
		$sSQL ="
			SELECT un.id,e.nombre as nombreEdificio,e.direccion,un.nombre as nombreUnidad	
			FROM unidades un INNER JOIN edificios e ON un.idEdificio=e.id
			INNER JOIN users u ON un.idUser=u.id
			WHERE u.sha = '".$iId."'
			AND u.status=1 AND e.status=1 AND un.status=1 ORDER BY e.nombre,un.nombre
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	public function getUsers($iId){
		$sSQL ="
			SELECT u.id,CONCAT(u.name,' ',u.lastName) as nombre,
u.address,u.phone			
			FROM users u 
			WHERE u.idAdmin = (SELECT e.id FROM users e WHERE e.sha = '".$iId."')
			AND u.status=1 ORDER BY u.lastName, u.name
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
		public function getUsersBySecurity($iId){
		$sSQL ="
			SELECT u.id,CONCAT(u.lastName,', ',u.name) as nombre,
u.address,u.phone, uni.nombre as unidad			
			FROM users u 
			INNER JOIN unidades uni ON u.id=uni.idUser
			WHERE u.idAdmin = (SELECT e.idAdmin FROM users e WHERE e.sha = '".$iId."')
			AND u.status=1 AND u.sha<> '".$iId."' ORDER BY u.lastName, u.name
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
		public function getUnidades($iId){
		$sSQL ="
			SELECT un.id,un.nombre,CONCAT(u.name, ' ',u.lastname) as usuario,ed.nombre as edificio	
			FROM unidades un INNER JOIN edificios ed ON un.idEdificio=ed.id
			LEFT JOIN users u ON un.idUser=u.id
			WHERE un.idAdmin = (SELECT e.id FROM users e WHERE e.sha = '".$iId."')
			AND ed.status=1 AND un.status=1 and un.id<>118 ORDER BY ed.nombre,un.nombre
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
		public function getUnidadesBloqueo($iId){
		$sSQL ="
			SELECT un.id,un.nombre,CONCAT(u.name, ' ',u.lastname) as usuario,ed.nombre as edificio	
			FROM unidades un 
			INNER JOIN edificios ed ON un.idEdificio=ed.id
			INNER JOIN users u ON un.idUser=u.id
			WHERE un.idAdmin = (SELECT e.id FROM users e WHERE e.sha = '".$iId."')
			AND ed.status=1 AND un.status=1 and un.id<>118 
			ORDER BY ed.nombre,un.nombre
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	
	public function updateGarante($idC,$idG){
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE customer
				SET garante =".$idG.
				" WHERE id = ".$idC;

			$res=mysqli_query($this->cnx,$sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	
	
		public function editUser($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'name'=>$vData->name,
			'lastName'=>$vData->lastName,
			'address'=>$vData->address,
			'phone'=>$vData->phone,
			'userName'=>$vData->userName,
			'sysUsersType'=>$vData->sysUsersType,
			'password'=>$vData->password,
			'sha'=>$vData->sha); 
			
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE users
				SET ".$sValueList.
				" WHERE id = ".$vData->id;

			$res=mysqli_query($this->cnx,$sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
		public function editUserByUser($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'name'=>$vData->name,
			'lastName'=>$vData->lastName,
			'address'=>$vData->address,
			'phone'=>$vData->phone,
			'email_alt'=>$vData->email_alt,
			'password'=>$vData->password); 
			
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE users
				SET ".$sValueList.
				" WHERE id = ".$vData->id;

			$res=mysqli_query($this->cnx,$sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	
	public function searchUsers($iUserID,$sUserName,$sUserLastName,$sUserAddress,$sUserEmail,$iMinLimit, $iRecPerPage,& $iRecordsTotal){
		$sWhere = null;
		if(!$iMinLimit){
			$iMinLimit = 0;
		}
		$sLimit = " LIMIT $iMinLimit, $iRecPerPage";
		if($iUserID){
			$sWhere=" WHERE u.id = $iUserID";
		}
		if($sUserName){
			if(!$sWhere){
				$sWhere=" WHERE u.name LIKE '%$sUserName%'";
			}else{
				$sWhere.=" AND u.name LIKE '%$sUserName%'";
			}
		}
		if($sUserLastName){
			if(!$sWhere){
				$sWhere=" WHERE u.lastName LIKE '%$sUserLastName%'";
			}else{
				$sWhere.=" AND u.lastName LIKE '%$sUserLastName%'";
			}
		}
		if($sUserAddress){
			if(!$sWhere){
				$sWhere=" WHERE u.address LIKE '%$sUserAddress%'";
			}else{
				$sWhere.=" AND u.address LIKE '%$sUserAddress%'";
			}
		}
		if($sUserEmail){
			if(!$sWhere){
				$sWhere=" WHERE u.email LIKE '%$sUserEmail%'";
			}else{
				$sWhere.=" AND u.email LIKE '%$sUserEmail%'";
			}
		}
		if(!$sWhere){
			$sWhere=" WHERE p.id_idioma=8 AND r.id_idioma=8 AND l.id_idioma=8";		
		}else{
			$sWhere.=" AND p.id_idioma=8 AND r.id_idioma=8 AND l.id_idioma=8";		
		}
		
		$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
		u.id as id, 
		u.name as name,
		u.lastName as lastName,
		u.email,
		u.phone,
		u.address,
		p.nombre as country,
		r.nombre as region,
		l.nombre as localidad
		FROM users u
		INNER JOIN paises p ON u.id_pais=p.id
INNER JOIN regiones r ON u.id_region=r.id
INNER JOIN localidades l ON u.id_localidad=l.id

		$sWhere $sLimit";
		

		//var_dump($sSQL);exit;
		try{
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while ($row = mysqli_fetch_array($res)){
					$oData = new stdClass();
					$oData->userID 			= $row['id'];
					$oData->userName 		= $row['name'];
					$oData->userLastName 	= $row['lastName'];
					$oData->userAddress 	= $row['address'];
					$oData->userPhone 		= $row['phone'];
					$oData->userEmail 		= $row['email'];
					$oData->userCountry 		= $row['country'];
					$oData->userRegion 		= $row['region'];
					$oData->userLocalidad 		= $row['localidad'];
					$vData[]=$oData;
				}
			}else{
				throw new Exception("ERR0006");
			}
		}catch (Exception $e){
			return $e->getMessage();
		}
		$sSQL ="SELECT FOUND_ROWS() as Total";
		$res=mysqli_query($this->cnx,$sSQL);
		$row=mysqli_fetch_assoc($res);
		$iRecordsTotal=$row['Total'];
		return $vData;			
	}
	
	
	public function registrarReclamo($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO reclamos 
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysqli_query($this->cnx,$sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysqli_insert_id();
				}else{
					$oData->status = "5";
				}
			}catch (Exception $e){
				$oData->status = "5";
			}
			
		
		return $oData;
	}
	
	
	public function getNovedadesUser($iId){
		$sSQL ="
			SELECT e.nombre as nombreEdificio, n.mensaje as descripcion,n.fecha	
			FROM unidades un INNER JOIN edificios e ON un.idEdificio=e.id
			INNER JOIN users u ON un.idUser=u.id
			INNER JOIN novedades n ON n.idEdificio=e.id
			WHERE u.sha = '".$iId."'
			AND u.status=1 AND e.status=1 AND un.status=1 ORDER BY n.fecha desc limit 10
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
		public function getInformacionUser($iId){
		$sSQL ="
			SELECT e.nombre as nombreEdificio, n.mensaje as descripcion,n.fecha,n.titulo as titulo	
			FROM unidades un INNER JOIN edificios e ON un.idEdificio=e.id
			INNER JOIN users u ON un.idUser=u.id
			INNER JOIN informacion n ON n.idEdificio=e.id
			WHERE u.sha = '".$iId."'
			AND u.status=1 AND e.status=1 AND un.status=1 ORDER BY n.fecha desc limit 10
			";
			
			//var_dump($sSQL);
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$vData= array();
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
		public function resetPassword($sUserName){
		$sSQL ="
			SELECT 
				u.password
			FROM users u

			WHERE u.userName = '$sUserName' AND u.status=1

			LIMIT 1";
		try{
			$res=mysqli_query($this->cnx,$sSQL);
			if($res){
				$row = mysqli_fetch_assoc($res);
				if ($row){
					$oData = new stdClass();
					$oData->password=$row['password'];
					$oData->userName=$sUserName;
					return $oData;
				}else{
					throw new Exception("ERR0004");
				}
			}else{
				throw new Exception("ERR0004");
			}
		}catch (Exception $e){
			return $this->getErrorMsj($e->getMessage(),"SPA");
		}
		
	}
	
	
	
	
}	
?>