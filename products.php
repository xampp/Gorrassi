<?php
include_once 'config.php';
$tpl->load_file("/products/products.html", "bodyContent");
//$sAction 			= $_POST['sAction'];

$bBoll= !empty($_POST['sAction']) && isset($_POST['sAction']);
$sAction = $bBoll?$_POST['sAction']:null;

$parametros=$para->getParametros();

switch ($sAction){
	case "search":
		$name = $_POST['search'];
		//$tpl->set_var("sValueSearchBus",$name);
		
		if(count($oAdv->getProductsSearch($name))>0){
			
			
			//var_dump($oAdv->getProductsSearch($name));exit;
			
			foreach ($oAdv->getProductsSearch($name) as $Item){
				$tpl->set_var("iId",$utils->encrypt($Item['id'],"sapopep16+3/"));
				$tpl->set_var("sBrand",($Item['brand']));
				$tpl->set_var("sName",ucwords(mb_strtolower(($Item['name']))));
				$tpl->set_var("sDescription",($Item['description']));
				$tpl->set_var("sDescription1",($oAdv->wordlimit($Item['model'],4)));
				
					if ($parametros['precios']=='S') {
				$tpl->set_var("iValue",(round($Item['value'])));
				}
				$tpl->set_var("sFile",($Item['file']));
				$tpl->set_var("sFile1",($Item['file1']));
				$tpl->set_var("sFile2",($Item['file2']));
				$tpl->set_var("sDisplayResulDestacado","");
				
				$tpl->set_var("sTarget",'target="_blank"');
				
			if (strlen($Item['cuotas'])>3 OR ($Item['cuotas']<>'')){
					$tpl->set_var("sDisplayResulDestacado","");
				
				} else {
					$tpl->set_var("sDisplayResulDestacado","display:none!important;");
				}
				
				$tpl->set_var("sFile3",($Item['file3']));
				$tpl->set_var("sPlan",($Item['cuotas']));
				
				if ($Item['cantidadCuotas']>0){
					$tpl->set_var("sCartel",'Con Crédito Personal');
					$tpl->set_var("sCuotasNew",$Item['cantidadCuotas'].' cuotas de $ '.$Item['valorCuota']);
					$tpl->set_var("cft",'CFT: '.$parametros['cft'].'% ');
					$tpl->set_var("tea",'TEA: '.$parametros['tea'].'%');
					$tpl->set_var("iFinanciado",($Item['valorFinanciado']));
				} else {
					$tpl->set_var("sCuotasNew",'');
					$tpl->set_var("cft",'');
					$tpl->set_var("tea",'');
					$tpl->set_var("iFinanciado",($Item['value']));
				}
				
				$tpl->parse("ProductosXArt",true);
			}
		} 
		else {
			$tpl->set_var("sDisplayResul","display:none!important;");
			$tpl->set_var("sResultado","<h2>Página vacía.</h2><br><p>No se han encontrado resultados para su búsqueda...</p><br><img src='img/logoL.png'>");
			}
			
	break;
		
	case "sendByEmail":
		
			$name = $_POST['productos'];

			if(count($oAdv->getProductsSearchEmail($name))>0){
				foreach ($oAdv->getProductsSearchEmail($name) as $Item){
					$tpl->set_var("iId",$utils->encrypt($Item['id'],"sapopep16+3/"));
					$tpl->set_var("sBrand",($Item['brand']));
					$tpl->set_var("sName",ucwords(mb_strtolower(($Item['name']))));
					$tpl->set_var("sDescription",($Item['description']));
					$tpl->set_var("sDescription1",($oAdv->wordlimit($Item['model'],4)));
						if ($parametros['precios']=='S') {
					$tpl->set_var("iValue",(round($Item['value'])));
					}
					$tpl->set_var("sFile",($Item['file']));
					$tpl->set_var("sFile1",($Item['file1']));
					$tpl->set_var("sFile2",($Item['file2']));
					$tpl->set_var("sDisplayResulDestacado","");
					
				if (strlen($Item['cuotas'])>3 OR ($Item['cuotas']<>'')){
						$tpl->set_var("sDisplayResulDestacado","");
					
					} else {
						$tpl->set_var("sDisplayResulDestacado","display:none!important;");
					}
					
					$tpl->set_var("sFile3",($Item['file3']));
					$tpl->set_var("sPlan",($Item['cuotas']));
					
					if ($Item['cantidadCuotas']>0){
						$tpl->set_var("sCartel",'Con Crédito Personal');
						$tpl->set_var("sCuotasNew",$Item['cantidadCuotas'].' cuotas de $ '.$Item['valorCuota']);
						$tpl->set_var("cft",'CFT: '.$parametros['cft'].'% ');
						$tpl->set_var("tea",'TEA: '.$parametros['tea'].'%');
						$tpl->set_var("iFinanciado",($Item['valorFinanciado']));
					} else {
						$tpl->set_var("sCuotasNew",'');
						$tpl->set_var("cft",'');
						$tpl->set_var("tea",'');
						$tpl->set_var("iFinanciado",($Item['value']));
					}				
					
					$tpl->parse("ProductosXArt",true);
				}
			} 
			else {
				$tpl->set_var("sDisplayResul","display:none!important;");
				$tpl->set_var("sResultado","<h2>Página vacía.</h2><br><p>No se han encontrado resultados para su búsqueda...</p><br><img src='img/logoL.png'>");
				}
	break;
	
	default:
		$bBoll= empty($_GET['select']) || !isset($_GET['select']);
	  
		if($_GET['art'] && $bBoll){
			
			$name = $utils->decrypt($_GET['art'],"sapopep16+3/");
			
			//var_dump($oAdv->getProductsByArt($name));
			
			if(count($oAdv->getProductsByArt($name))>0){
			
			$names=$oAdv->getNameProductsByArt($name);
			$tpl->set_var("sCate",($names[0]["categoria"]));
			$tpl->set_var("sSubC","<strong class='descIndice'>".(' > '.$names[0]["subcategoria"]))."<strong>";
			
			foreach ($oAdv->getProductsByArt($name) as $Item){
				if ($parametros['precios']=='S') {
					$tpl->set_var("iValue",(round($Item['value'])));
				} 
				$tpl->set_var("sBrand",($Item['brand']));
				$tpl->set_var("sName",ucwords(mb_strtolower(($Item['name']))));
				$tpl->set_var("sDescription",($Item['description']));
				$tpl->set_var("sDescription1",($oAdv->wordlimit($Item['model'],4)));
				$tpl->set_var("sFile",($Item['file']));
				$tpl->set_var("sFile1",($Item['file1']));
				$tpl->set_var("sFile2",($Item['file2']));
				$tpl->set_var("sFile3",($Item['file3']));
				
				
				if ($Item['cantidadCuotas']>0){
					$tpl->set_var("sCartel",'Con Crédito Personal');
					$tpl->set_var("sCuotasNew",$Item['cantidadCuotas'].' cuotas de $ '.$Item['valorCuota']);
					$tpl->set_var("cft",'CFT: '.$parametros['cft'].'% ');
					$tpl->set_var("tea",'TEA: '.$parametros['tea'].'%');
					$tpl->set_var("iFinanciado",($Item['valorFinanciado']));
				} else {
					$tpl->set_var("sCuotasNew",'');
					$tpl->set_var("cft",'');
					$tpl->set_var("tea",'');
					$tpl->set_var("iFinanciado",($Item['value']));
				}
				
				if (strlen($Item['cuotas'])>3 OR ($Item['cuotas']<>'')){
					$tpl->set_var("sDisplayResulDestacado","");
				
				} else {
					$tpl->set_var("sDisplayResulDestacado","display:none!important;");
				}
							
				$tpl->set_var("sPlan",($Item['cuotas']));
				$tpl->set_var("iId",$utils->encrypt($Item['id'],"sapopep16+3/"));
				$tpl->parse("ProductosXArt",true);
				}
			} else {
					$tpl->set_var("sDisplayResul","display:none!important;");
					$tpl->set_var("sResultado","<h2>Página vacía.</h2><br><p>No se han encontrado resultados...</p><br><img src='img/logoL.png'>");
				}
		} else {
		  //var_dump("hola");
		  if ($_GET['art'] && $_GET['select'] && ($utils->decrypt($_GET['select'],"sapopep16+3/") =='25_pepe') ) {		
				//busco por revista		
				$name12 = $utils->decrypt($_GET['select'],"sapopep16+3/");
				//$name = $utils->decrypt($_GET['art'],"sapopep16+3/");
				$name = base64_decode($_GET['art']);		
					//$name = $_POST['productos'];
			  if(count($oAdv->getProductsSearchEmail($name))>0){
					foreach ($oAdv->getProductsSearchEmail($name) as $Item){
						$tpl->set_var("iId",$utils->encrypt($Item['id'],"sapopep16+3/"));
						$tpl->set_var("sBrand",($Item['brand']));
						$tpl->set_var("sName",ucwords(mb_strtolower(($Item['name']))));
						$tpl->set_var("sDescription",($Item['description']));
						$tpl->set_var("sDescription1",($oAdv->wordlimit($Item['model'],4)));
							if ($parametros['precios']=='S') {
						$tpl->set_var("iValue",(round($Item['value'])));
						} 
						$tpl->set_var("sFile",($Item['file']));
						$tpl->set_var("sFile1",($Item['file1']));
						$tpl->set_var("sFile2",($Item['file2']));
						$tpl->set_var("sDisplayResulDestacado","");
						
					if (strlen($Item['cuotas'])>3 OR ($Item['cuotas']<>'')){
							$tpl->set_var("sDisplayResulDestacado","");
						
						} else {
							$tpl->set_var("sDisplayResulDestacado","display:none!important;");
						}
						
						$tpl->set_var("sFile3",($Item['file3']));
						$tpl->set_var("sPlan",($Item['cuotas']));
						
						if ($Item['cantidadCuotas']>0){
							$tpl->set_var("sCartel",'Con Crédito Personal');
							$tpl->set_var("sCuotasNew",$Item['cantidadCuotas'].' cuotas de $ '.$Item['valorCuota']);
							$tpl->set_var("cft",'CFT: '.$parametros['cft'].'% ');
							$tpl->set_var("tea",'TEA: '.$parametros['tea'].'%');
							$tpl->set_var("iFinanciado",($Item['valorFinanciado']));
						} else {
							$tpl->set_var("sCuotasNew",'');
							$tpl->set_var("cft",'');
							$tpl->set_var("tea",'');
							$tpl->set_var("iFinanciado",($Item['value']));
						}							
						$tpl->parse("ProductosXArt",true);
				  }//foreach
			  }else {
					$tpl->set_var("sDisplayResul","display:none!important;");
					$tpl->set_var("sResultado","<h2>Página vacía.</h2><br><p>No se han encontrado resultados para su búsqueda...</p><br><img src='img/logoL.png'>");
				}		
			} else {			
				$tpl->set_var("sDisplayResul","display:none!important;");
				$tpl->set_var("sResultado","<h2>Página vacía.</h2><br><p>No se han encontrado resultados...</p><br><img src='img/logoL.png'>");	
			}	
	  } 

	break;			
}	
		
$tpl->pparse("main");
?> 