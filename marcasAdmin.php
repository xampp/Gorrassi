<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser']))) {
		
		
		$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);		
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		
		
		if($user->can(IS_ADMIN,$_SESSION['sysUser'])){
			
			$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
		
	} 
		
		$tpl->load_file("pg/admin/marcas.html", "bodyContent");
		$tpl->load_file("pg/admin/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		//$tpl->set_var("sSysUserName","Administrador");
		$sAction 			= $_POST['sAction'];
		
		
	
		
		switch ($sAction){
			case "editMarca":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['editName'];
					$oData->id	 		= (int)($_POST['iEditID']);
					
					

					$resultEdit = $oAdv->editMarca($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado la marca.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "searchMarcaById":
	
		$oData = $oAdv->getMarcaById($_POST['ediID']);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	case "newMarca":

					$oData = new stdClass();
					$oData->name	= $_POST['newName'];
							
					$result = $oAdv->newMarca($oData);
					
					if ($result->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado una nueva marca.");
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}
					
				
					
	break;
	case "deleteMarca":
		
			$oData = $oAdv->deleteMarcaById($_POST['iID']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	default:
	break;
}
		
if(count($oAdv->getMarcas())>0){
		foreach ($oAdv->getMarcas() as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sName",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));
				$tpl->parse("ResultsBlock",true);
			}
	} 
			
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>