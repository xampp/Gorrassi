<?php
include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017")){
	if ($user->can(IS_SECURITY,$_SESSION['sysUser'])) {
		
		$tpl->load_file("pg/security/service.html", "bodyContent");
		$tpl->load_file("pg/security/menu.html","menu");
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		$oData=$user->getName();
			$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);	
			$tpl->set_var("sSysUserLetter",$oData['name'][0]);	
			$tpl->set_var("sSysUserEmail",$oData['userName']);	
			$tpl->set_var("sSysUserNameRol",$oData['rol']);	
		$sAction 			= $_POST['sAction'];
		
		
	
		
		switch ($sAction){
			case "editService":
			  
					$oData = new stdClass();
					$oData->name	= $_POST['editNombre'];
					$oData->email = htmlentities($_POST['editEmail']);
					$oData->phone = htmlentities($_POST['editTelefono']);
					$oData->descrip = ($_POST['editAcc']);
					$oData->problem = ($_POST['editProblema']);
					$oData->obser = htmlentities($_POST['editObservaciones']);
					$oData->status = 1;
					$oData->orden	= $_POST['editOrden'];
					$oData->credito = htmlentities($_POST['editCredito']);
					$oData->marca = htmlentities($_POST['editMarca']);
					$oData->modelo = htmlentities($_POST['editModelo']);
					$oData->serie = htmlentities($_POST['editSerie']);
					$oData->fecha = htmlentities($_POST['editFecha']);
					$oData->recibe = htmlentities($_POST['editRecibe']);
					$oData->bill = htmlentities($_POST['editFactura']);

					$resultEdit = $oAdv->editService($oData);
					
					if ($resultEdit->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A modificado el service.");
						
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
						
					}
					
	break;
	case "searchServiceById":
	
		$oData = $oAdv->getServiceById($_POST['iID']);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	case "newService":

					$oData = new stdClass();
					$oData->name	= $_POST['newNombre'];
					$oData->lastName	= $_POST['newApellido'];
					$oData->dni	= $_POST['newDNI'];
					$oData->email = htmlentities($_POST['newEmail']);
					$oData->phone = htmlentities($_POST['newTelefono']);
					$oData->cellPhone = htmlentities($_POST['newCelular']);
					$oData->descrip = ($_POST['newAcc']);
					$oData->problem = ($_POST['newProblema']);
					$oData->obser = htmlentities($_POST['newObservaciones']);
					$oData->status = 1;
					$oData->credito = htmlentities($_POST['newCredito']);
					$oData->marca = (int)($_POST['newMarca']);
					$oData->modelo = htmlentities($_POST['newModelo']);
					$oData->serie = htmlentities($_POST['newSerie']);
					$oData->fecha = htmlentities($_POST['newFecha']);
					$oData->recibe = htmlentities($_POST['newRecibe']);
					$oData->articulo = htmlentities($_POST['newArticulo']);
					$oData->bill = htmlentities($_POST['newFactura']);
					//$oData->endDate = htmlentities($_POST['newFechaPromesa']);
					//$oData->endDate = htmlentities($_POST['newFechaPromesa']);
					

					$result = $oAdv->newService($oData);
	

					if ($result->status=="OK"){
						$tpl->set_var("sDisplayError","display:none;");
						$tpl->set_var("sDisplayOK","");
						$tpl->set_var("sResult","A agregado un Nuevo Servicio.");
					}else {
						$tpl->set_var("sDisplayError","");
						$tpl->set_var("sDisplayOK","display:none;");
						$tpl->set_var("sResult","");
					}
					 
					
	break;
	case "deleteService":
		
			$oData = $oAdv->deleteServiceById($_POST['iID']);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");
			}
			
		echo json_encode($oData);
		exit;
	break;
	default:
	break;
}
				
			
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>