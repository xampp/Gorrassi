<?php

include_once 'config.php';

if (!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && (SYSTEM=="PASCUAL2017") ){
	if ( ($user->can(IS_ADMIN,$_SESSION['sysUser']))  ||  ($user->can(IS_EDIT,$_SESSION['sysUser']))){

		$oData=$user->getName();
		$tpl->set_var("sSysUserName",$oData['name'].'  '.$oData['lastName']);
		$tpl->set_var("sSysUserLetter",$oData['name'][0]);
		$tpl->set_var("sSysUserEmail",$oData['userName']);
		$tpl->set_var("sSysUserNameRol",$oData['rol']);
		//$tpl->set_var("iId",$oData['id']);	

	if($user->can(IS_ADMIN,$_SESSION['sysUser'])){

		$tpl->set_var("sUsuarioAdmin",'<li><a href="usersAdmin.php"><i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span></a></li>');
  } 

	$tpl->load_file("pg/admin/pedido.html", "bodyContent");

	$tpl->load_file("pg/admin/menu.html","menu");

	$tpl->set_var("sDisplayError","display:none;");

	$tpl->set_var("sDisplayOK","display:none;");

	$tpl->set_var("sResult","");

	//$tpl->set_var("sSysUserName","Administrador");
 
	$sAction 	= isset($_POST['sAction'])?$_POST['sAction']:null;
		
	$men = isset($_GET['New'])?$_GET['New']: null;

	switch ($men){

		case "ns":

		$tpl->set_var("sDisplaySd","display:none;");

		break;

		case "sd":

		$tpl->set_var("sDisplayNs","display:none;");

		break;

		default:

		$tpl->set_var("sDisplayNs","display:none;");

		break;
	}
	
	switch ($sAction){

		case "editService":

			$oData = new stdClass();

			/*
			$oData->name	= $_POST['editNombre'];

			$oData->email = htmlentities($_POST['editEmail']);

			$oData->phone = htmlentities($_POST['editTelefono']);

			$oData->cellPhone = htmlentities($_POST['editCelular']);

			$oData->descrip = ($_POST['editAcc']);

			$oData->problem = ($_POST['editProblema']);

			$oData->obser = htmlentities($_POST['editObservaciones']);

			//$oData->status = (int)($_POST['editStatus']);

			$oData->status = (int)($_POST['editStatus']);

			$oData->credito = htmlentities($_POST['editCredito']);

			$oData->marca = (int)($_POST['editMarca']);

			$oData->modelo = htmlentities($_POST['editModelo']);

			$oData->serie = htmlentities($_POST['editSerie']);

			$oData->fecha = htmlentities($_POST['editFecha']);

			$oData->endDate = htmlentities($_POST['editFechaPromesa']);

			$oData->recibe = htmlentities($_POST['editRecibe']);

			$oData->bill = htmlentities($_POST['editFactura']);

			$oData->lastName	= $_POST['editApellido'];

			$oData->articulo = htmlentities($_POST['editArticulo']);

			$oData->dni	= $_POST['editDNI'];
			*/
			$oData->status = (int)($_POST['editStatus']);

			$oData->comentario	= $_POST['editComentarios'];

			$oData->id	 		= (int)($_POST['iEditID']);

			$resultEdit = $oAdv->editPedido($oData);
			
			if ($resultEdit->status=="OK"){
	
				if ($_POST['editStatus']==5){

					//$resultActualizar = $oAdv->updateStockByIdOrdenNegativo($_POST['iEditID']);

				}

				$tpl->set_var("sDisplayError","display:none;");

				$tpl->set_var("sDisplayOK","");

				$tpl->set_var("sResult","A modificado el service.");
			
			}else {

				$tpl->set_var("sDisplayError","");

				$tpl->set_var("sDisplayOK","display:none;");

				$tpl->set_var("sResult","");
				
			}
			
	break;

	case "searchServiceById":
	
		$oData = $oAdv->getPedidoById($_POST['iID']);

		//var_dump($oData);exit;

		if($oData['queryStatus'] != "OK"){

			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");

		}

		echo json_encode($oData);

		exit;

	break;
	
	case "newService":

		$oData = new stdClass();

			$oData->name	= $_POST['newNombre'];

			$oData->lastName	= $_POST['newApellido'];

			$oData->dni	= $_POST['newDNI'];

			$oData->email = htmlentities($_POST['newEmail']);

			$oData->phone = htmlentities($_POST['newTelefono']);

			$oData->cellPhone = htmlentities($_POST['newCelular']);

			$oData->descrip = ($_POST['newAcc']);

			$oData->problem = ($_POST['newProblema']);

			$oData->obser = htmlentities($_POST['newObservaciones']);

			$oData->status = 1;

			$oData->credito = htmlentities($_POST['newCredito']);

			$oData->marca = (int)($_POST['newMarca']);

			$oData->modelo = htmlentities($_POST['newModelo']);

			$oData->serie = htmlentities($_POST['newSerie']);

			$oData->fecha = htmlentities($_POST['newFecha']);

			$oData->recibe = htmlentities($_POST['newRecibe']);

			$oData->articulo = htmlentities($_POST['newArticulo']);

			$oData->bill = htmlentities($_POST['newFactura']);

			$oData->numero = $oAdv->get_Numero();

			//$oData->endDate = htmlentities($_POST['newFechaPromesa']);

			//$oData->endDate = htmlentities($_POST['newFechaPromesa']);

    //var_dump($oData);exit;

		$result = $oAdv->newService($oData);

		if($result->status != "OK"){
			$result->status = $db->getLabel("lbl_".$oData->status,"SPA");
  	}
	
		echo json_encode($result);

		exit;
				
	break;

	/*case "newService":



					$oData = new stdClass();

					$oData->name	= $_POST['newNombre'];

					$oData->lastName	= $_POST['newApellido'];

					$oData->dni	= $_POST['newDNI'];

					$oData->email = htmlentities($_POST['newEmail']);

					$oData->phone = htmlentities($_POST['newTelefono']);

					$oData->descrip = ($_POST['newAcc']);

					$oData->problem = ($_POST['newProblema']);

					$oData->obser = htmlentities($_POST['newObservaciones']);

					$oData->status = 1;

					$oData->credito = htmlentities($_POST['newCredito']);

					$oData->marca = (int)($_POST['newMarca']);

					$oData->modelo = htmlentities($_POST['newModelo']);

					$oData->serie = htmlentities($_POST['newSerie']);

					$oData->fecha = htmlentities($_POST['newFecha']);

					$oData->recibe = htmlentities($_POST['newRecibe']);

					$oData->articulo = htmlentities($_POST['newArticulo']);

					$oData->bill = htmlentities($_POST['newFactura']);

					



					$result = $oAdv->newService($oData);

	



					if ($result->status=="OK"){

						$tpl->set_var("sDisplayError","display:none;");

						$tpl->set_var("sDisplayOK","");

						$tpl->set_var("sResult","A agregado un Nuevo Servicio.");

					}else {

						$tpl->set_var("sDisplayError","");

						$tpl->set_var("sDisplayOK","display:none;");

						$tpl->set_var("sResult","");

					}

					 
	break;*/

	case "deletePedido":
		
			$oData = $oAdv->deletePedidoById($_POST['iID']);

			if($oData->status != "OK"){

				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA");

			}
			

		echo json_encode($oData);

		exit;

	break;

	default:

	break;

}

if(count($oAdv->getStatusPedido())>0){

	foreach ($oAdv->getStatusPedido() as $Item){

		$tpl->set_var("iEstado",$Item['id']);

		$tpl->set_var("sEstado",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));

		$tpl->parse("EstadoBlock",true);
	}
} 

if(count($oAdv->getPedidos(1))>0){
	foreach ($oAdv->getPedidos(1) as $Item){

			$tpl->set_var("iId1",$Item['id']);

			$tpl->set_var("sNombre1",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sDni1",mb_convert_encoding($Item['dni'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sEmail1",mb_convert_encoding($Item['email'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sNumero1",mb_convert_encoding($Item['id'], 'UTF-8', 'ISO-8859-1'));

			$detalle='';

			$idOrden = $Item['id'];

			if(count($oAdv->getPedidosDetails($idOrden))>0){

				foreach ($oAdv->getPedidosDetails($idOrden) as $ItemM){

				$detalle = $detalle . $ItemM['cod'].' - '.$ItemM['size'].' - '.$ItemM['color'].' - '.$ItemM['quantity'].' - '.$ItemM['precioUnidad'].' - '.$ItemM['total'].' <br>';					
				}
			}
			else {
				$detalle="SIN ARTICULOS";
			}
							
			$tpl->set_var("verPedido",$detalle);

			$tpl->set_var("sApellido1",mb_convert_encoding($Item['lastName'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sFecha1",mb_convert_encoding(is_null($Item['fecha'])?'':$Item['fecha'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sFechaB1",mb_convert_encoding($Item['beginDate'], 'UTF-8', 'ISO-8859-1'));

			//$tpl->set_var("sMarca1",mb_convert_encoding(is_null($Item['marca'])?'':, 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sSerie1",mb_convert_encoding(is_null($Item['serie'])?'':$Item['serie'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sModelo1",mb_convert_encoding(is_null($Item['modelo'])?'':$Item['modelo'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sTelefono1",mb_convert_encoding(is_null($Item['phone'])?'':$Item['phone'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sEstado1",mb_convert_encoding(is_null($Item['status'])?'':$Item['status'], 'UTF-8', 'ISO-8859-1'));

			$tpl->parse("ResultsEsperandoBlock",true);

		}
}

if(count($oAdv->getPedidos(2))>0){

	foreach ($oAdv->getPedidos(2) as $Item){

			$tpl->set_var("iId",$Item['id']);

			$tpl->set_var("sNombre",mb_convert_encoding($Item['name'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sDni",mb_convert_encoding($Item['dni'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sEmail",mb_convert_encoding($Item['email'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sNumero",mb_convert_encoding($Item['id'], 'UTF-8', 'ISO-8859-1'));

		

			$detalle='';

			$idOrden = $Item['id'];

			if(count($oAdv->getPedidosDetails($idOrden))>0){

				foreach ($oAdv->getPedidosDetails($idOrden) as $ItemM){

				$detalle = $detalle . $ItemM['cod'].' - '.$ItemM['size'].' - '.$ItemM['color'].' - '.$ItemM['quantity'].' - '.$ItemM['precioUnidad'].' - '.$ItemM['total'].' <br>';
				
				} 

			} 

			else {

				$detalle="SIN ARTICULOS";

			}
						
			$tpl->set_var("verPedido",$detalle);
			
			$tpl->set_var("sApellido",mb_convert_encoding($Item['lastName'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sFecha",mb_convert_encoding(is_null($Item['fecha'])?'':$Item['fecha'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sFechaB",mb_convert_encoding($Item['beginDate'], 'UTF-8', 'ISO-8859-1'));

			//$tpl->set_var("sMarca",mb_convert_encoding($Item['marca'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sSerie",mb_convert_encoding(is_null($Item['serie'])?'':$Item['serie'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sModelo",mb_convert_encoding(is_null($Item['modelo'])?'':$Item['modelo'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sTelefono",mb_convert_encoding($Item['phone'], 'UTF-8', 'ISO-8859-1'));

			$tpl->set_var("sEstado",mb_convert_encoding($Item['status'], 'UTF-8', 'ISO-8859-1'));

			$tpl->parse("ResultsBlockEnds",true);

		}

  } 				
}else{		
		header('location: login.php');
  }
}else{
	header('location: login.php');
}

$tpl->pparse("main");

?>