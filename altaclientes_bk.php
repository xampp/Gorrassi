<?php
include_once 'config.php';
$tpl->load_file("/slider/slider.html", "slider");
$tpl->load_file("/altaclientes/altaclientes.html", "bodyContent");
$tpl->set_var("sWebSin",'style="display: none"');

$sAction 	= isset($_POST['sAction'])?$_POST['sAction']:null;

switch ($sAction){
	case "newUser":
		$oData					    = new stdClass();
		$oData->name 			  = htmlentities($_POST['newNombre']);
		$oData->lastName	 	= htmlentities($_POST['newApellido']);
		$oData->address 		= htmlentities($_POST['newDomicilio']);
		$oData->userName		= $_POST['newEmail'];
		$oData->password  	= $_POST['newPassword'];
		$oData->phone	 		  = $_POST['newTelefono'];
		$oData->status	 		= 1;
		//$oData->last_ip	 		= $_SERVER['REMOTE_ADDR'];
		$oData->sha	 			  = sha1($oData->password . $oData->userName);

		$oData->sysUsersType	= (int)9;
		$oData->idAdmin	 		  = (int)0;
		
		$result = $user->newUser($oData);
		if ($result->status=="OK"){
			$newEmail=$utils->sendEmail($oData->userName, 'Bienvenidos A Pascual Gorrassi', 'Sus datos para acceder al sistema son:<br><br>Usuario : '.$oData->userName.'<br>Password : '.$oData->password.'<br><br>Para acceder ingrese a <a href=https://pascualgorrassi.com.ar>https://pascualgorrassi.com.ar</a><br><br>');			
			$tpl->set_var("sDisplayError","display:none;");
			$tpl->set_var("sDisplayOK","");
			$tpl->set_var("sResult","Gracias por registrarte.");
			
		}else {
			$tpl->set_var("sDisplayError","");
			$tpl->set_var("sDisplayOK","display:none;");
			$tpl->set_var("sResult","");
		}

	break;
	case "editUser":
		$oData					  = new stdClass();
		$oData->name 			= htmlentities($_POST['editNombre']);
		$oData->lastName	= htmlentities($_POST['editApellido']);
		$oData->address 	= htmlentities($_POST['editDomicilio']);
		$oData->phone	 		= $_POST['editTelefono'];
		$oData->id	 		  = (int)($_POST['iEditID']);
		$oData->userName		  = $_POST['editEmail'];
		$oData->password  		= $_POST['editPassword'];
		$oData->sysUsersType	= (int)$_POST['editTipo'];
		$oData->sha	 			= sha1($oData->password . $oData->userName);
		
		
		$result = $user->editUser($oData);
		if ($result->status=="OK"){
		  $tpl->set_var("sDisplayError","display:none;");
			$tpl->set_var("sDisplayOK","");
			$tpl->set_var("sResult","A modificado el Usuario.");
			
		}else {
			$tpl->set_var("sDisplayError","");
			$tpl->set_var("sDisplayOK","display:none;");
			$tpl->set_var("sResult","");
		}
	break;
	default:
	break;
}


$tpl->pparse("main");
?>