<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Repository\TermsRepository;

/**
 * @ORM\Entity(repositoryClass=TermsRepository::class)
 */
class Terms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="rec", type="string", length=15, nullable=false)
     */
    private $rec;

    /**
     * @var int
     *
     * @ORM\Column(name="visible", type="integer", nullable=false, options={"default"="1"})
     */
    private $visible = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="cuota", type="integer", nullable=false)
     */
    private $cuota;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRec(): ?string
    {
        return $this->rec;
    }

    public function setRec(string $rec): self
    {
        $this->rec = $rec;

        return $this;
    }

    public function getVisible(): ?int
    {
        return $this->visible;
    }

    public function setVisible(int $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getCuota(): ?int
    {
        return $this->cuota;
    }

    public function setCuota(int $cuota): self
    {
        $this->cuota = $cuota;

        return $this;
    }


}
