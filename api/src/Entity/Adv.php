<?php

namespace App\Entity;

use App\Repository\AdvRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdvRepository::class)
 */

class Adv
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false, options={"comment"="breve comentario de la publicación"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=250, nullable=false, options={"default"="sinImagen.jpg"})
     */
    private $file = 'sinImagen.jpg';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateBegin", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP","comment"="fecha de alta"})
     */
    private $datebegin = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="file1", type="string", length=250, nullable=false, options={"default"="sinImagen.jpg"})
     */
    private $file1 = 'sinImagen.jpg';

    /**
     * @var string
     *
     * @ORM\Column(name="file2", type="string", length=250, nullable=false, options={"default"="sinImagen.jpg"})
     */
    private $file2 = 'sinImagen.jpg';

    /**
     * @var string
     *
     * @ORM\Column(name="file3", type="string", length=250, nullable=false, options={"default"="sinImagen.jpg"})
     */
    private $file3 = 'sinImagen.jpg';

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var int
     *
     * @ORM\Column(name="category", type="integer", nullable=false)
     */
    private $category;

    /**
     * @var int
     *
     * @ORM\Column(name="offer", type="integer", nullable=false)
     */
    private $offer = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="cuotas", type="string", length=150, nullable=false)
     */
    private $cuotas;

    /**
     * @var int
     *
     * @ORM\Column(name="subcategory", type="integer", nullable=false)
     */
    private $subcategory;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidadCuotas", type="integer", nullable=false)
     */
    private $cantidadcuotas;

    /**
     * @var string
     *
     * @ORM\Column(name="valorCuota", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorcuota;

    /**
     * @var string
     *
     * @ORM\Column(name="valorFinanciado", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorfinanciado;

    /**
     * @var int
     *
     * @ORM\Column(name="brand", type="integer", nullable=false)
   
     * @ORM\ManyToOne(targetEntity="Brands", inversedBy="adv")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
	 
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=250, nullable=false)
     */
    private $model;

    /**
     * @var int
     *
     * @ORM\Column(name="envio", type="integer", nullable=false)
     */
    private $envio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateUpdate", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dateupdate = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="conditionPay", type="integer", nullable=true)
     */
    private $conditionpay;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codBar", type="string", length=250, nullable=true)
     */
    private $codbar;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getDatebegin(): ?\DateTimeInterface
    {
        return $this->datebegin;
    }

    public function setDatebegin(\DateTimeInterface $datebegin): self
    {
        $this->datebegin = $datebegin;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFile1(): ?string
    {
        return $this->file1;
    }

    public function setFile1(string $file1): self
    {
        $this->file1 = $file1;

        return $this;
    }

    public function getFile2(): ?string
    {
        return $this->file2;
    }

    public function setFile2(string $file2): self
    {
        $this->file2 = $file2;

        return $this;
    }

    public function getFile3(): ?string
    {
        return $this->file3;
    }

    public function setFile3(string $file3): self
    {
        $this->file3 = $file3;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCategory(): ?int
    {
        return $this->category;
    }

    public function setCategory(int $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getOffer(): ?int
    {
        return $this->offer;
    }

    public function setOffer(int $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getCuotas(): ?string
    {
        return $this->cuotas;
    }

    public function setCuotas(string $cuotas): self
    {
        $this->cuotas = $cuotas;

        return $this;
    }

    public function getSubcategory(): ?int
    {
        return $this->subcategory;
    }

    public function setSubcategory(int $subcategory): self
    {
        $this->subcategory = $subcategory;

        return $this;
    }

    public function getCantidadcuotas(): ?int
    {
        return $this->cantidadcuotas;
    }

    public function setCantidadcuotas(int $cantidadcuotas): self
    {
        $this->cantidadcuotas = $cantidadcuotas;

        return $this;
    }

    public function getValorcuota(): ?string
    {
        return $this->valorcuota;
    }

    public function setValorcuota(string $valorcuota): self
    {
        $this->valorcuota = $valorcuota;

        return $this;
    }

    public function getValorfinanciado(): ?string
    {
        return $this->valorfinanciado;
    }

    public function setValorfinanciado(string $valorfinanciado): self
    {
        $this->valorfinanciado = $valorfinanciado;

        return $this;
    }

    public function getBrand(): ?int
    {
        return $this->brand;
    }

    public function setBrand(int $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getEnvio(): ?int
    {
        return $this->envio;
    }

    public function setEnvio(int $envio): self
    {
        $this->envio = $envio;

        return $this;
    }

    public function getDateupdate(): ?\DateTimeInterface
    {
        return $this->dateupdate;
    }

    public function setDateupdate(\DateTimeInterface $dateupdate): self
    {
        $this->dateupdate = $dateupdate;

        return $this;
    }

    public function getConditionpay(): ?int
    {
        return $this->conditionpay;
    }

    public function setConditionpay(?int $conditionpay): self
    {
        $this->conditionpay = $conditionpay;

        return $this;
    }

    public function getCodbar(): ?string
    {
        return $this->codbar;
    }

    public function setCodbar(?string $codbar): self
    {
        $this->codbar = $codbar;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }


}
