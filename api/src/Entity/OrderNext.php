<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\OrderNextRepository;

/**
 * @ORM\Entity(repositoryClass=OrderNextRepository::class)
 */
class OrderNext
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idOrden", type="integer", nullable=false)
     */
    private $idorden;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beginDate", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $begindate = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="idService", type="integer", nullable=false)
     */
    private $idservice;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="text", length=65535, nullable=false)
     */
    private $comentario;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdorden(): ?int
    {
        return $this->idorden;
    }

    public function setIdorden(int $idorden): self
    {
        $this->idorden = $idorden;

        return $this;
    }

    public function getBegindate(): ?\DateTimeInterface
    {
        return $this->begindate;
    }

    public function setBegindate(\DateTimeInterface $begindate): self
    {
        $this->begindate = $begindate;

        return $this;
    }

    public function getIdservice(): ?int
    {
        return $this->idservice;
    }

    public function setIdservice(int $idservice): self
    {
        $this->idservice = $idservice;

        return $this;
    }

    public function getComentario(): ?string
    {
        return $this->comentario;
    }

    public function setComentario(string $comentario): self
    {
        $this->comentario = $comentario;

        return $this;
    }


}
