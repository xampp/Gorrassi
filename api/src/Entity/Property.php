<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Repository\PropertyRepository;

/**
 * @ORM\Entity(repositoryClass=PropertyRepository::class)
 */
class Property
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="propietario", type="string", length=200, nullable=false)
     */
    private $propietario;

    /**
     * @var string
     *
     * @ORM\Column(name="ubica", type="string", length=200, nullable=false)
     */
    private $ubica;

    /**
     * @var string
     *
     * @ORM\Column(name="matricula", type="string", length=200, nullable=false)
     */
    private $matricula;

    /**
     * @var string
     *
     * @ORM\Column(name="partida", type="string", length=200, nullable=false)
     */
    private $partida;

    /**
     * @var string
     *
     * @ORM\Column(name="circun", type="string", length=200, nullable=false)
     */
    private $circun;

    /**
     * @var string
     *
     * @ORM\Column(name="seccion", type="string", length=200, nullable=false)
     */
    private $seccion;

    /**
     * @var string
     *
     * @ORM\Column(name="manzana", type="string", length=200, nullable=false)
     */
    private $manzana;

    /**
     * @var string
     *
     * @ORM\Column(name="parcela", type="string", length=200, nullable=false)
     */
    private $parcela;

    /**
     * @var string
     *
     * @ORM\Column(name="subparcela", type="string", length=200, nullable=false)
     */
    private $subparcela;

    /**
     * @var \Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idCus", referencedColumnName="id")
     * })
     */
    private $idcus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPropietario(): ?string
    {
        return $this->propietario;
    }

    public function setPropietario(string $propietario): self
    {
        $this->propietario = $propietario;

        return $this;
    }

    public function getUbica(): ?string
    {
        return $this->ubica;
    }

    public function setUbica(string $ubica): self
    {
        $this->ubica = $ubica;

        return $this;
    }

    public function getMatricula(): ?string
    {
        return $this->matricula;
    }

    public function setMatricula(string $matricula): self
    {
        $this->matricula = $matricula;

        return $this;
    }

    public function getPartida(): ?string
    {
        return $this->partida;
    }

    public function setPartida(string $partida): self
    {
        $this->partida = $partida;

        return $this;
    }

    public function getCircun(): ?string
    {
        return $this->circun;
    }

    public function setCircun(string $circun): self
    {
        $this->circun = $circun;

        return $this;
    }

    public function getSeccion(): ?string
    {
        return $this->seccion;
    }

    public function setSeccion(string $seccion): self
    {
        $this->seccion = $seccion;

        return $this;
    }

    public function getManzana(): ?string
    {
        return $this->manzana;
    }

    public function setManzana(string $manzana): self
    {
        $this->manzana = $manzana;

        return $this;
    }

    public function getParcela(): ?string
    {
        return $this->parcela;
    }

    public function setParcela(string $parcela): self
    {
        $this->parcela = $parcela;

        return $this;
    }

    public function getSubparcela(): ?string
    {
        return $this->subparcela;
    }

    public function setSubparcela(string $subparcela): self
    {
        $this->subparcela = $subparcela;

        return $this;
    }

    public function getIdcus(): ?Customer
    {
        return $this->idcus;
    }

    public function setIdcus(?Customer $idcus): self
    {
        $this->idcus = $idcus;

        return $this;
    }


}
