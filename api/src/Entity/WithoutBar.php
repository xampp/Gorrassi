<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Repository\WithoutBarRepository;

/**
 * @ORM\Entity(repositoryClass=WithoutBarRepository::class)
 */
class WithoutBar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="page", type="string", length=255, nullable=true)
     */
    private $page;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPage(): ?string
    {
        return $this->page;
    }

    public function setPage(?string $page): self
    {
        $this->page = $page;

        return $this;
    }


}
