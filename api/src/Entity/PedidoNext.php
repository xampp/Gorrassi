<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Repository\PedidoNextRepository;

/**
 * @ORM\Entity(repositoryClass=PedidoNextRepository::class)
 */
class PedidoNext
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idOrden", type="integer", nullable=false)
     */
    private $idorden;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beginDate", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $begindate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="cod", type="string", length=200, nullable=false)
     */
    private $cod;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=200, nullable=false)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=200, nullable=false)
     */
    private $color;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="precioUnidad", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $preciounidad;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $total;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdorden(): ?int
    {
        return $this->idorden;
    }

    public function setIdorden(int $idorden): self
    {
        $this->idorden = $idorden;

        return $this;
    }

    public function getBegindate(): ?\DateTimeInterface
    {
        return $this->begindate;
    }

    public function setBegindate(\DateTimeInterface $begindate): self
    {
        $this->begindate = $begindate;

        return $this;
    }

    public function getCod(): ?string
    {
        return $this->cod;
    }

    public function setCod(string $cod): self
    {
        $this->cod = $cod;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPreciounidad(): ?string
    {
        return $this->preciounidad;
    }

    public function setPreciounidad(string $preciounidad): self
    {
        $this->preciounidad = $preciounidad;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }


}
