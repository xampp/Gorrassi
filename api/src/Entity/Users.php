<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Repository\UsersRepository;

/**
 * @ORM\Entity(repositoryClass=UsersRepository::class)
 */
class Users
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=120, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="userName", type="string", length=220, nullable=false, options={"comment"="username es el email de los usuarios, debe ser unico"})
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=120, nullable=false)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="blob", length=65535, nullable=false)
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codeActivation", type="blob", length=65535, nullable=true, options={"comment"="les enviamos a los usuarios dados de alta para chekear su email, un código, click y activan la cuenta"})
     */
    private $codeactivation;

    /**
     * @var int
     *
     * @ORM\Column(name="sysUsersType", type="integer", nullable=false, options={"default"="2","comment"="que tipo de usuario es, 1-administrador 2-usuario"})
     */
    private $sysuserstype = '2';

    /**
     * @var string
     *
     * @ORM\Column(name="sha", type="text", length=65535, nullable=false, options={"comment"="entre email y password"})
     */
    private $sha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=45, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone1", type="string", length=45, nullable=true)
     */
    private $phone1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone2", type="string", length=45, nullable=true)
     */
    private $phone2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone3", type="string", length=45, nullable=true)
     */
    private $phone3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email_alt", type="string", length=45, nullable=true)
     */
    private $emailAlt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone_pref", type="string", length=45, nullable=true)
     */
    private $phonePref;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false, options={"comment"="0-bloqueada 1-activada"})
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="string", length=45, nullable=true)
     */
    private $address;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idAdmin", type="integer", nullable=true)
     */
    private $idadmin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getCodeactivation()
    {
        return $this->codeactivation;
    }

    public function setCodeactivation($codeactivation): self
    {
        $this->codeactivation = $codeactivation;

        return $this;
    }

    public function getSysuserstype(): ?int
    {
        return $this->sysuserstype;
    }

    public function setSysuserstype(int $sysuserstype): self
    {
        $this->sysuserstype = $sysuserstype;

        return $this;
    }

    public function getSha(): ?string
    {
        return $this->sha;
    }

    public function setSha(string $sha): self
    {
        $this->sha = $sha;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone1(): ?string
    {
        return $this->phone1;
    }

    public function setPhone1(?string $phone1): self
    {
        $this->phone1 = $phone1;

        return $this;
    }

    public function getPhone2(): ?string
    {
        return $this->phone2;
    }

    public function setPhone2(?string $phone2): self
    {
        $this->phone2 = $phone2;

        return $this;
    }

    public function getPhone3(): ?string
    {
        return $this->phone3;
    }

    public function setPhone3(?string $phone3): self
    {
        $this->phone3 = $phone3;

        return $this;
    }

    public function getEmailAlt(): ?string
    {
        return $this->emailAlt;
    }

    public function setEmailAlt(?string $emailAlt): self
    {
        $this->emailAlt = $emailAlt;

        return $this;
    }

    public function getPhonePref(): ?string
    {
        return $this->phonePref;
    }

    public function setPhonePref(?string $phonePref): self
    {
        $this->phonePref = $phonePref;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getIdadmin(): ?int
    {
        return $this->idadmin;
    }

    public function setIdadmin(?int $idadmin): self
    {
        $this->idadmin = $idadmin;

        return $this;
    }


}
