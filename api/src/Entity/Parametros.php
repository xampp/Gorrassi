<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Repository\ParametrosRepository;

/**
 * @ORM\Entity(repositoryClass=ParametrosRepository::class)
 */
class Parametros
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="domicilio", type="string", length=150, nullable=false)
     */
    private $domicilio;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=150, nullable=false)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="cft", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $cft;

    /**
     * @var string
     *
     * @ORM\Column(name="tea", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $tea;

    /**
     * @var string
     *
     * @ORM\Column(name="web", type="string", length=200, nullable=false)
     */
    private $web;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=200, nullable=false)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="precios", type="string", length=1, nullable=false)
     */
    private $precios;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDomicilio(): ?string
    {
        return $this->domicilio;
    }

    public function setDomicilio(string $domicilio): self
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCft(): ?string
    {
        return $this->cft;
    }

    public function setCft(string $cft): self
    {
        $this->cft = $cft;

        return $this;
    }

    public function getTea(): ?string
    {
        return $this->tea;
    }

    public function setTea(string $tea): self
    {
        $this->tea = $tea;

        return $this;
    }

    public function getWeb(): ?string
    {
        return $this->web;
    }

    public function setWeb(string $web): self
    {
        $this->web = $web;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getPrecios(): ?string
    {
        return $this->precios;
    }

    public function setPrecios(string $precios): self
    {
        $this->precios = $precios;

        return $this;
    }


}
