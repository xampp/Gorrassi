<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use App\Repository\PedidoRepository;

/**
 * @ORM\Entity(repositoryClass=PedidoRepository::class)
 */
class Pedido
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="beginDate", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $begindate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="problem", type="text", length=65535, nullable=false)
     */
    private $problem;

    /**
     * @var string
     *
     * @ORM\Column(name="descrip", type="text", length=65535, nullable=false)
     */
    private $descrip;

    /**
     * @var string
     *
     * @ORM\Column(name="obser", type="text", length=65535, nullable=false)
     */
    private $obser;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=250, nullable=false)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="endDate", type="date", nullable=true)
     */
    private $enddate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serie", type="string", length=150, nullable=true)
     */
    private $serie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="credito", type="string", length=100, nullable=true)
     */
    private $credito;

    /**
     * @var int|null
     *
     * @ORM\Column(name="marca", type="integer", nullable=true)
     */
    private $marca;

    /**
     * @var string|null
     *
     * @ORM\Column(name="modelo", type="string", length=200, nullable=true)
     */
    private $modelo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha", type="string", length=100, nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="recibe", type="string", length=200, nullable=false)
     */
    private $recibe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bill", type="string", length=100, nullable=true)
     */
    private $bill;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dni", type="string", length=50, nullable=true)
     */
    private $dni;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lastName", type="string", length=250, nullable=true)
     */
    private $lastname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="articulo", type="string", length=250, nullable=true)
     */
    private $articulo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="servicio", type="integer", nullable=true)
     */
    private $servicio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="seguimiento", type="text", length=65535, nullable=true)
     */
    private $seguimiento;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechaSeguimiento", type="date", nullable=true)
     */
    private $fechaseguimiento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comentario", type="text", length=65535, nullable=true)
     */
    private $comentario;

    /**
     * @var int|null
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cellPhone", type="string", length=20, nullable=true)
     */
    private $cellphone;

    /**
     * @var int|null
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var int|null
     *
     * @ORM\Column(name="numero", type="integer", nullable=true)
     */
    private $numero;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBegindate(): ?\DateTimeInterface
    {
        return $this->begindate;
    }

    public function setBegindate(\DateTimeInterface $begindate): self
    {
        $this->begindate = $begindate;

        return $this;
    }

    public function getProblem(): ?string
    {
        return $this->problem;
    }

    public function setProblem(string $problem): self
    {
        $this->problem = $problem;

        return $this;
    }

    public function getDescrip(): ?string
    {
        return $this->descrip;
    }

    public function setDescrip(string $descrip): self
    {
        $this->descrip = $descrip;

        return $this;
    }

    public function getObser(): ?string
    {
        return $this->obser;
    }

    public function setObser(string $obser): self
    {
        $this->obser = $obser;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(?\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getCredito(): ?string
    {
        return $this->credito;
    }

    public function setCredito(?string $credito): self
    {
        $this->credito = $credito;

        return $this;
    }

    public function getMarca(): ?int
    {
        return $this->marca;
    }

    public function setMarca(?int $marca): self
    {
        $this->marca = $marca;

        return $this;
    }

    public function getModelo(): ?string
    {
        return $this->modelo;
    }

    public function setModelo(?string $modelo): self
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(?string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getRecibe(): ?string
    {
        return $this->recibe;
    }

    public function setRecibe(string $recibe): self
    {
        $this->recibe = $recibe;

        return $this;
    }

    public function getBill(): ?string
    {
        return $this->bill;
    }

    public function setBill(?string $bill): self
    {
        $this->bill = $bill;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(?string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getArticulo(): ?string
    {
        return $this->articulo;
    }

    public function setArticulo(?string $articulo): self
    {
        $this->articulo = $articulo;

        return $this;
    }

    public function getServicio(): ?int
    {
        return $this->servicio;
    }

    public function setServicio(?int $servicio): self
    {
        $this->servicio = $servicio;

        return $this;
    }

    public function getSeguimiento(): ?string
    {
        return $this->seguimiento;
    }

    public function setSeguimiento(?string $seguimiento): self
    {
        $this->seguimiento = $seguimiento;

        return $this;
    }

    public function getFechaseguimiento(): ?\DateTimeInterface
    {
        return $this->fechaseguimiento;
    }

    public function setFechaseguimiento(?\DateTimeInterface $fechaseguimiento): self
    {
        $this->fechaseguimiento = $fechaseguimiento;

        return $this;
    }

    public function getComentario(): ?string
    {
        return $this->comentario;
    }

    public function setComentario(?string $comentario): self
    {
        $this->comentario = $comentario;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(?int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(?int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }


}
