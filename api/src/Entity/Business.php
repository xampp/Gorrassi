<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\BusinessRepository;

/**
 * @ORM\Entity(repositoryClass=BusinessRepository::class)
 */
class Business
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa", type="string", length=200, nullable=false)
     */
    private $empresa;

    /**
     * @var string
     *
     * @ORM\Column(name="dom", type="string", length=200, nullable=false)
     */
    private $dom;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_empresa", type="string", length=150, nullable=false)
     */
    private $telEmpresa;

    /**
     * @var string
     *
     * @ORM\Column(name="rubro", type="string", length=100, nullable=false)
     */
    private $rubro;

    /**
     * @var string
     *
     * @ORM\Column(name="cargo", type="string", length=200, nullable=false)
     */
    private $cargo;

    /**
     * @var string
     *
     * @ORM\Column(name="sueldo", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $sueldo;

    /**
     * @var string
     *
     * @ORM\Column(name="antiguedad", type="string", length=150, nullable=false)
     */
    private $antiguedad;

    /**
     * @var string
     *
     * @ORM\Column(name="first_cont", type="string", length=200, nullable=false)
     */
    private $firstCont;

    /**
     * @var string
     *
     * @ORM\Column(name="last_cont", type="string", length=200, nullable=false)
     */
    private $lastCont;

    /**
     * @var string
     *
     * @ORM\Column(name="domicilio_cont", type="string", length=200, nullable=false)
     */
    private $domicilioCont;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_cont", type="string", length=150, nullable=false)
     */
    private $telCont;

    /**
     * @var string
     *
     * @ORM\Column(name="rel_cont", type="string", length=150, nullable=false)
     */
    private $relCont;

    /**
     * @var \Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idCus", referencedColumnName="id")
     * })
     */
    private $idcus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?string
    {
        return $this->empresa;
    }

    public function setEmpresa(string $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getDom(): ?string
    {
        return $this->dom;
    }

    public function setDom(string $dom): self
    {
        $this->dom = $dom;

        return $this;
    }

    public function getTelEmpresa(): ?string
    {
        return $this->telEmpresa;
    }

    public function setTelEmpresa(string $telEmpresa): self
    {
        $this->telEmpresa = $telEmpresa;

        return $this;
    }

    public function getRubro(): ?string
    {
        return $this->rubro;
    }

    public function setRubro(string $rubro): self
    {
        $this->rubro = $rubro;

        return $this;
    }

    public function getCargo(): ?string
    {
        return $this->cargo;
    }

    public function setCargo(string $cargo): self
    {
        $this->cargo = $cargo;

        return $this;
    }

    public function getSueldo(): ?string
    {
        return $this->sueldo;
    }

    public function setSueldo(string $sueldo): self
    {
        $this->sueldo = $sueldo;

        return $this;
    }

    public function getAntiguedad(): ?string
    {
        return $this->antiguedad;
    }

    public function setAntiguedad(string $antiguedad): self
    {
        $this->antiguedad = $antiguedad;

        return $this;
    }

    public function getFirstCont(): ?string
    {
        return $this->firstCont;
    }

    public function setFirstCont(string $firstCont): self
    {
        $this->firstCont = $firstCont;

        return $this;
    }

    public function getLastCont(): ?string
    {
        return $this->lastCont;
    }

    public function setLastCont(string $lastCont): self
    {
        $this->lastCont = $lastCont;

        return $this;
    }

    public function getDomicilioCont(): ?string
    {
        return $this->domicilioCont;
    }

    public function setDomicilioCont(string $domicilioCont): self
    {
        $this->domicilioCont = $domicilioCont;

        return $this;
    }

    public function getTelCont(): ?string
    {
        return $this->telCont;
    }

    public function setTelCont(string $telCont): self
    {
        $this->telCont = $telCont;

        return $this;
    }

    public function getRelCont(): ?string
    {
        return $this->relCont;
    }

    public function setRelCont(string $relCont): self
    {
        $this->relCont = $relCont;

        return $this;
    }

    public function getIdcus(): ?Customer
    {
        return $this->idcus;
    }

    public function setIdcus(?Customer $idcus): self
    {
        $this->idcus = $idcus;

        return $this;
    }


}
