<?php

namespace App\Repository;

use App\Entity\UsersLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsersLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersLog[]    findAll()
 * @method UsersLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersLog::class);
    }

    // /**
    //  * @return UsersLog[] Returns an array of UsersLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersLog
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
