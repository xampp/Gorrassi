<?php

namespace App\Repository;

use App\Entity\Adv;
use App\Entity\Brands;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Adv|null find($id, $lockMode = null, $lockVersion = null)
 * @method Adv|null findOneBy(array $criteria, array $orderBy = null)
 * @method Adv[]    findAll()
 * @method Adv[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Adv::class);
        $this->manager = $manager;

    }

    public function getAdvToArray()
    {
        return $this
            ->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }
	
	 public function getAdvOffersToArray()
    {   $em =  $this->getEntityManager();
        $RAW_QUERY = 'SELECT a.id, a.file, a.file1,a.file2,a.model,a.file3,a.name as name,a.cuotas, a.description,a.value,a.cantidadCuotas,a.valorCuota,a.valorFinanciado,b.name as brand
		FROM adv a INNER JOIN brands b ON a.brand=b.id where a.offer=1 and a.status=1 order by a.id desc limit 3';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result; 
	}

 public function getAdvSearchToArray($name)
    {   $em =  $this->getEntityManager();
        $RAW_QUERY = "SELECT a.id, a.file, a.file1,a.file2,a.model,a.file3,a.name as name,a.cuotas, a.description,a.value,b.name as brand,a.quantity,
			a.cantidadCuotas,a.valorCuota,a.valorFinanciado
			FROM adv a INNER JOIN brands b ON a.brand=b.id INNER JOIN terms t ON a.conditionPay=t.id WHERE a.status=1 AND (a.name like '%$name%' OR a.description like '%$name%' OR b.name like '%$name%') AND t.visible=1
			ORDER BY a.value asc";
        $statement = $em->getConnection()->prepare($RAW_QUERY);
		//$statement->bindValue('name', $name);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result; 
		
	}



public function getAdvBySubcategory($category,$subcategory)
    {   $em =  $this->getEntityManager();
        
		

if ($category) {

	$Where ="WHERE a.status=1 and a.category = $category "; 

}

if ($subcategory!="0"){

	$Where.="AND a.subcategory = $subcategory";

}
		
		$RAW_QUERY = "SELECT Distinct a.id, a.file,a.model, a.file1,a.file2,a.file3,a.name as name,a.cuotas, a.description,a.value,a.cantidadCuotas,a.valorCuota,a.valorFinanciado,b.name as brand
			FROM adv a INNER JOIN brands b ON a.brand=b.id $Where ORDER BY a.value";
			
        $statement = $em->getConnection()->prepare($RAW_QUERY);
		//$statement->bindValue('name', $name);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result; 
		
	}


    public function getAdvByBrandToArray($brand)
    {
        return $this
            ->createQueryBuilder('a')
            ->where('a.brand = :val' )
            ->setParameter('val', $brand)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getAdvByStatusToArray($status)
    {
        return $this
            ->createQueryBuilder('a')
            ->where('a.status = :val' )
            ->setParameter('val', $status)
            ->orderBy('aid', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getAdvByCategoryToArray($category)
    {
        return $this
            ->createQueryBuilder('a')
            ->where('a.status = :val' )
            ->setParameter('val', $category)
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }



    // /**
    //  * @return Adv[] Returns an array of Adv objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Adv
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
