<?php

namespace App\Repository;

use App\Entity\OrderNext;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderNext|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderNext|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderNext[]    findAll()
 * @method OrderNext[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderNextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderNext::class);
    }

    // /**
    //  * @return OrderNext[] Returns an array of OrderNext objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderNext
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
