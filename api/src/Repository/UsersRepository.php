<?php

namespace App\Repository;

use App\Entity\Adv;
use App\Entity\ApiUsers;
use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Users::class);
        $this->manager = $manager;

    }

    public function saveUsers($usersData)
    {
        $newUsers = new Users();

        $newUsers
            ->setName($usersData['name'])
            ->setUsername($usersData['userName'])
            ->setLastname($usersData['lastName'])
            ->setPassword($usersData['password'])
            ->setCodeactivation($usersData['codeActivation'])
            ->setSysuserstype($usersData['sysUsersType'])
            ->setSha($usersData['sha'])
            ->setPhone($usersData['phone'])
            ->setAddress($usersData['address'])
            ->setIdadmin($usersData['idAdmin']);

        $this->manager->persist($newUsers);
        $this->manager->flush($newUsers);
        return $newUsers;
    }


    // /**
    //  * @return Users[] Returns an array of Users objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Users
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
