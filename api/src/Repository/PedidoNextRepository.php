<?php

namespace App\Repository;

use App\Entity\PedidoNext;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PedidoNext|null find($id, $lockMode = null, $lockVersion = null)
 * @method PedidoNext|null findOneBy(array $criteria, array $orderBy = null)
 * @method PedidoNext[]    findAll()
 * @method PedidoNext[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PedidoNextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PedidoNext::class);
    }

    // /**
    //  * @return PedidoNext[] Returns an array of PedidoNext objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PedidoNext
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
