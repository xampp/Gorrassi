<?php

namespace App\Repository;

use App\Entity\PedidoStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PedidoStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method PedidoStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method PedidoStatus[]    findAll()
 * @method PedidoStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PedidoStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PedidoStatus::class);
    }

    // /**
    //  * @return PedidoStatus[] Returns an array of PedidoStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PedidoStatus
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
