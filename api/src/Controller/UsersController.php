<?php

namespace App\Controller;

use App\Entity\Adv;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UsersController extends AbstractController
{
    private $usersRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->usersRepository = $entityManager->getRepository(Users::class);
    }

    /**
     * @Route("/login", name="login", methods={"POST", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @return JsonResponse
     * @throws \JsonException
     */
    public function login(Request $request, AuthenticationController $authenticationController)
    {
        // Chequeo nivel de autorización mínimo para loguear (sea request desde la app)
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        // Decodeo el request
        $body = json_decode($request->getContent(), true);

        // Busco el usuario en la bd
        $getUser = $this->usersRepository->findBy(['email' => $body['email'], 'password' => $body['password']]);

        if (!count($getUser))
        {
            throw new \JsonException(['error' => 'el usuario ingresado no se encuentra registrado'], 404);
        }

        // Traigo la información de sesión como el uuid o id de sesión o cualquier otro key => value que se haya guardado previamente con las acciones guardas como usuario anónimo
        $uuidSesion = $request->getSession()->get('uuid');

        // TODO: Implementar guardado en carrito de compras con el usuario logueado
        // Manejo las acciones que requiera con esa información se sesión


        return new JsonResponse((['success' => 'usuario logueado correctamente']), Response::HTTP_OK);


    }


    /**
     * @Route("/register", name="register", methods={"POST", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @return JsonResponse
     * @throws \JsonException
     */
    public function register(Request $request, AuthenticationController $authenticationController)
    {
        // Chequeo nivel de autorización mínimo para loguear (sea request desde la app)
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        // Decodeo el request
        $body = json_decode($request->getContent(), true);

        // Guardo el usuario en la bd
        if (!$this->usersRepository->saveUsers($body))
        {
            throw new \JsonException(['error' => 'no se pudo guardar el usuario'], 404);
        }
        // Traigo la información de sesión como el uuid o id de sesión o cualquier otro key => value que se haya guardado previamente con las acciones guardas como usuario anónimo
        $uuidSesion = $request->getSession()->get('uuid');


        // TODO: Implementar guardado en carrito de compras con el usuario registrado
        // Manejo las acciones que requiera con esa información se sesión


        return new JsonResponse((['success' => 'usuario registrado correctamente']), Response::HTTP_OK);

    }

    public function delete()
    {

    }

    public function saveLogUsers()
    {

    }

}
