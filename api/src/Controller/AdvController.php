<?php

namespace App\Controller;

use App\Entity\Adv;
use App\Entity\Brands;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdvController extends AbstractController
{
    private $advRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->advRepository = $entityManager->getRepository(Adv::class);
    }

    /**
     * @Route("/advList", name="advList", methods={"GET", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function advList(Request $request, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $data = $this->advRepository->getAdvToArray();
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ningún producto disponible'], 404);
        }
        return new JsonResponse($data    , Response::HTTP_OK);
    }


/**
     * @Route("/advOffersList", name="advOffersList", methods={"GET", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function advOffersList(Request $request, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $data = $this->advRepository->getAdvOffersToArray();
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ningún producto disponible'], 404);
        }
        return new JsonResponse($data    , Response::HTTP_OK);
    }




    /**
     * @Route("/advByBrand", name="advViewByBrand", methods={"POST", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function advByBrand(Request $request, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $body = json_decode($request->getContent(), true);
        $data = $this->advRepository->getAdvByBrandToArray($body['brand']);
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ningún producto de la marca solicitada disponible'], 404);
        }
        return new JsonResponse(($data), Response::HTTP_OK);
    }

    /**
     * @Route("/advByStatus", name="advViewByStatus", methods={"POST", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function advByStatus(Request $request, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $body = json_decode($request->getContent(), true);
        $data = $this->advRepository->getAdvByStatusToArray($body['status']);
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ningún producto del status solicitado disponible'], 404);
        }
        return new JsonResponse(($data), Response::HTTP_OK);
    }


    /**
     * @Route("/advSearch", name="advSearch", methods={"POST", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function advSearch(Request $request, AuthenticationController $authenticationController)
    { 
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $body = json_decode($request->getContent(), true);
        $data = $this->advRepository->getAdvSearchToArray($body['name']);
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ningún producto del nombre solicitado disponible'], 404);
        }
        return new JsonResponse(($data), Response::HTTP_OK);
    }

    /**
     * @Route("/getAdvByCategory", name="advViewByCategory", methods={"POST", "GET","OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function getAdvByCategory(Request $request, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);
        $body = json_decode($request->getContent(), true);
        $data = $this->advRepository->getAdvByCategoryToArray($body['category']);
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ningún producto la categoría solicitada disponible'], 404);
        }
        return new JsonResponse(($data), Response::HTTP_OK);
    }




  /**
     * @Route("/getAdvBySubcategory", name="getAdvBySubcategory", methods={"POST", "GET","OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function getAdvBySubcategory(Request $request, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);
        $body = json_decode($request->getContent(), true);
        $data = $this->advRepository->getAdvBySubcategory($body['category'],$body['subcategory']);
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ningún producto la categoría solicitada disponible'], 404);
        }
        return new JsonResponse(($data), Response::HTTP_OK);
    }

}
