<?php

namespace App\Controller;

use PascalDeVink\ShortUuid\ShortUuid;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionsHandlerController extends AbstractController
{
    public function checkOrCreateSession($request): bool
    {
        // Chequeo si existe un uuid de sesión generado, adicionalmente se puede utilizar el método getId() de sesión para usuarios aún no autenticados
        if ((!$request->getSession()->get('uuid'))) {
            $request->getSession()->set('uuid' ,$this->generateUid());
        }
        return true;
    }

    function generateUid()
    {
        try {
            $uuid = Uuid::uuid4();
            $shortUuid = new ShortUuid();
            $password = $shortUuid->encode($uuid);
            // Cognito requiere claves alfanumericas, por lo forzamos un 1 al final del string generado cuando no lo es.
            return $password;

        } catch (\Exception $e) {
            throw new NotFoundHttpException('Uuid no se pudo resolver!');
        }
    }

    public function addProductsToCartSession()
    {

    }

}
