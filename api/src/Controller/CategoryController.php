<?php

namespace App\Controller;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    private $categoryRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->categoryRepository = $entityManager->getRepository(Category::class);
    }

    /**
     * @Route("/categoryList", name="categoryList", methods={"GET", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function categoryList(Request $request, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $data = $this->categoryRepository->getCategoryToArray();
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ninguna categoría disponible'], 404);
        }
        return new JsonResponse(($data), Response::HTTP_OK);
    }
	
	 /**
     * @Route("/subcategoryFromCategory", name="subcategoryFromCategory", methods={"POST", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function subcategoryFromCategory(Request $request, AuthenticationController $authenticationController)
    { 
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $body = json_decode($request->getContent(), true);
        $data = $this->categoryRepository->getsubcategoryFromCategory($body['category']);
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ningún producto del nombre solicitado disponible'], 404);
        }
        return new JsonResponse(($data), Response::HTTP_OK);
    }

}
