<?php

namespace App\Controller;

use App\Entity\Brands;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BrandsController extends AbstractController
{

    private $brandsRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->brandsRepository = $entityManager->getRepository(Brands::class);
    }

    /**
     * @Route("/brandList", name="brandList", methods={"GET", "OPTIONS"})
     * @param AuthenticationController $authenticationController
     * @param Request $request
     * @throws \JsonException
     * @return JsonResponse
     */
    public function brandList(Request $request, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $data = $this->brandsRepository->getBrandToArray();
        if (!count($data)){
            throw new \JsonException(['error' => 'No existe ninguna marca disponible'], 404);
        }
        return new JsonResponse(($data), Response::HTTP_OK);
    }


}
