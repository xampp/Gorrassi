<?php

namespace App\Controller;

use phpDocumentor\Reflection\Types\Void_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AuthenticationController extends AbstractController
{
    private $authToken;

    public function __construct($authToken)
    {
        // Nivel mínimo de autenticación con header Authorization para asegurar sólo entren requests desde la app
        $this->authToken = $authToken;
    }

    public function isAuthenticated($token)
    {
        if ($token === $this->authToken) return true;
        throw new AccessDeniedHttpException('Token inválido',null,401);
    }
}

