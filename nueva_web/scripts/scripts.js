/* aparece desaparece boton menu */
$(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 50) {
            $(".bot-up").addClass('_hide');
            $(".bot-up").removeClass("_show");
        } else {
            $(".bot-up").removeClass("_hide");
            $(".bot-up").addClass("_show");
        }
    });
});



$(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 200) {
            $(".bot-dwn").addClass('_show');
            $(".bot-dwn").removeClass("_hide");
        } else {
            $(".bot-dwn").removeClass("_show");
            $(".bot-dwn").addClass("_hide");
        }
    });
});
/* aparece desaparece boton menu */
/* aparece desaparece header flotante */

$(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 200) {
            $("._nav-main").addClass('_nav-float');
        } else {
            $("._nav-main").removeClass("_nav-float");
        }
    });
});
/* aparece desaparece header flotante */

$(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 216) {
            $("._cont-aside").addClass('_sticky _move-top');
        } else {
            $("._cont-aside").removeClass('_sticky _move-top');
        }
    });
});
/* aparece desaparece header flotante */




/* aparece desaparece boton ir arriba*/
$(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll <= 200) {
            $(".bot-arrow").addClass('_hide');
            $(".bot-arrow").removeClass("_show");
        } else {
            $(".bot-arrow").removeClass("_hide");
            $(".bot-arrow").addClass("_show");
        }
    });
});
/* aparece desaparece boton ir arriba*/

jQuery(function($) {
    $(".swipebox").swipebox({
        useCSS: true, // false will force the use of jQuery for animations
        hideBarsDelay: 3000, // 0 to always show caption and action bar
        videoMaxWidth: 1140, // videos max width
        beforeOpen: function() {}, // called before opening
        afterClose: function() {} // called after closing
    });
});


var swiper = new Swiper(".mySwiper", {
    spaceBetween: 30,
    centeredSlides: false,
    autoplay: {
        delay: 2500,
        disableOnInteraction: true
    },
    speed: 3000,
    loop: true,
    slidesPerView: '2',
});


window.addEventListener('load', () => {
    var swiper = new Swiper('.agenda-slide', {
            /* effect: 'fade',
             fadeEffect: {
                 crossFade: true,
             },*/
            autoplay: {
                delay: 3000,
                disableOnInteraction: true,
            },
            speed: 3000,
            preloadImages: true,
            loop: true,
            centeredSlides: true,
            /*pagination: '.swiper-pagination',*/
            slidesPerView: 'auto',
            /*nextButton: '.swiper-button-next',*/
            /*prevButton: '.swiper-button-prev',*/
            centeredSlides: true,
            paginationClickable: true,
        }

    );

}, false);

window.addEventListener('load', () => {
    var swiper = new Swiper(".menuPrincipal", {
        slidesPerView: 'auto',
        spaceBetween: 30,
        slidesOffsetAfter: -1,
        loop: false,
        watchSlidesVisibility: true,
        centeredSlides: false,
        freeMode: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: true,
            stopOnLastSlide: true
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true
        }
    });
}, false);


const loadings = document.querySelectorAll('.loading');
const card_desp = document.querySelectorAll('.card-description');
const p = document.querySelector('p');

function renderCard() {

    for (loading of loadings) {
        loading.classList.remove('loading');
    }

    for (card_des of card_desp) {
        card_des.classList.add('_none');
    }


    for (loading of loadings) {
        p.style.visibility = 'visible';
    }

}

window.onload = () => {
    renderCard();
}

// let spline1 = document.getElementById('iz');
// let spline2 = document.getElementById('der');
// let sierras = document.getElementById('sierras');
// let arboleda = document.getElementById('arboleda');
let pedido = document.getElementById('pedido');
let txt = document.getElementById('texto');

window.addEventListener('scroll', function() {
    let value = window.scrollY;
    // spline1.style.left = value * -0.90 + 'px';
    // spline1.style.transform = "rotate(" + value * 0.12 + "deg) scale(0.30) translateX(-10vw) translateY(-35%)";
    // spline2.style.left = value * -0.8 + 'px';
    // spline2.style.top = value * -0.6 + 'px';
    // spline2.style.filter = 'blur(' + value * 0.02 + 'px)';
    // sierras.style.top = value * -0.3 + 'vh';
    // sierras.style.left = value * -0.3 + 'vw';
    // sierras.style.transform = "rotate(-" + value * 0.3 + "deg) scale(0.30) translateX(30%) translateY(-20vh)";
    // sierras.style.filter = 'blur(' + value * 0.03 + 'px)';
    // arboleda.style.left = value * 0.15 + 'px';
    pedido.style.left = value * 2 + 'px';
    pedido.style.transform = "rotate(-" + value * 0.05 + "deg) translateX(30%) translateY(-10vh) scale(0.60)";
    txt.style.marginRight = value * 3 + 'px';
    txt.style.transform = "rotate(-" + value + "deg)";
    var currScrollPos2 = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (currScrollPos2 > 230) {
        document.getElementById('texto').style.opacity = -currScrollPos2 / 230 + 4;
    }

})




const ball = document.querySelector(".iris");

let mouseX = 0;
let mouseY = 0;

let ballX = 0;
let ballY = 0;

let speed = 0.1;


function animate() {

    let distX = mouseX - ballX;
    let distY = mouseY - ballY;


    ballX = ballX + (distX * speed);
    ballY = ballY + (distY * speed);

    ball.style.left = ballX * 50 / window.innerWidth + "%";
    ball.style.top = ballY * 35 / window.innerHeight + "%";

    requestAnimationFrame(animate);
}
animate();

document.addEventListener("mousemove", function(event) {
    mouseX = event.pageX;
    mouseY = event.pageY;
})